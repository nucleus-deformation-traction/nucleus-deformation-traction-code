
#include "cell_traction_all_v6.h"

//void straincalc_2dsolid(double*** target_shape,double*** deformation_array,double*** strain_array,int nrows);

int main(void)
{
	char sourcename[200];
	char targetname[200];

	FILE* entry_exit_coordinates;

	
	//for before to entering:
	entry_exit_coordinates=fopen("before_to_in_entry_exit_coordinates.txt","r");

	//entry_exit_coordinates=fopen("in_to_exiting_entry_exit_coordinates.txt","r");
	strcpy(sourcename,"average_before_entering_shape.txt");
	strcpy(targetname,"average_entering_shape.txt");

/*
	//for entering to in:
	entry_exit_coordinates=fopen("entering_to_in_entry_exit_coordinates.txt","r");

	//entry_exit_coordinates=fopen("in_to_exiting_entry_exit_coordinates.txt","r");
	strcpy(sourcename,"average_entering_channel_shape.txt");
	strcpy(targetname,"average_in_channel_shape.txt");
	*/

	/*
		// for in to exiting!
	entry_exit_coordinates=fopen("in_to_exiting_entry_exit_coordinates.txt","r");

	//entry_exit_coordinates=fopen("in_to_exiting_entry_exit_coordinates.txt","r");
	strcpy(sourcename,"average_in_channel_shape.txt");
	strcpy(targetname,"average_exiting_shape.txt");
	*/
	/*
		//for exiting to out
	entry_exit_coordinates=fopen("exiting_to_out_entry_exit_coordinates.txt","r");

	//entry_exit_coordinates=fopen("in_to_exiting_entry_exit_coordinates.txt","r");
	strcpy(sourcename,"average_exiting_shape.txt");
	strcpy(targetname,"average_exited_shape.txt");

	*/


	int read_in=1;//0 for a test shape input in code, 1 for reading in from a file.
	//printf("Read in? 1 for yes, other numbers for no\n");
	//scanf("%d",&read_in);
	printf("a test \n");
	if(options_input=='y')//Options to be read in from a keyboard entries/script
	{
		printf("straintype?\n");
		scanf("%c",&straintype);
		printf("volume mode?\n");
		scanf("%c",&volume_mode);
		printf("strain mode?\n");
		scanf("%c",&strainmode);

		if(straintype=='o')
		{
			printf("number of inner shapes (important in solid only)?\n");
			scanf("%d",&num_innershapes);
		}

		printf("number of dimensions (i.e. 2d or 3d data)\n");
		scanf("%d",&dimensions);//2 for 2d or pseudo 3d (analytic approx for third dim), 3 for 3d (set 	of zstacks).

		if(dimensions==2)
		{
			printf("mode for 2d?\n");
			scanf("%c",&mode2d);//c for completely 2d, p for pseudo 3d, doesn't do anything in 3d case. y for cylinder (since c already taken...)
		}
		printf("shape to calculate on\n");
		scanf("%c",&shape_to_calculate_on);//t, shape calculated strain/stress/traction/etc on is the target,u, undeformed shape.
		printf("alignment position\n");
		scanf("%c",&alignment_pos);//r for rear aligned, c for centre aligned.
		printf("out of plane geometry?\n");
		scanf("%c",&out_plane_geom);//used only in 2d or psuedo 3d modes, doesn't do anything in 3d case: defines the geometry in direction not seen in the images
		
		printf("compressible?\n");
		scanf("%c",&incompressible);//if incompressible, sets out of plane strain = the negative of the sum of the other two. Also happens if poissons ratio is close to 0.5

		//Elastic Constants	
		printf("Youngs modulus\n");	
		scanf("%lf",&E); //Elastic Modulus
		
		if(incompressible=='y') sigma=0.50;//poission ratio
		else{printf("poisson's ratio?"); scanf("%lf",&sigma);}

		//image parameters
		printf("xy pixelsize\n");
		scanf("%lf",&xy_pixelsize);//in micro meters
		printf("zstack separation\n");
		scanf("%lf",&zstack_separation);//in micro meters

		//Channel parameters
		scanf("%lf",&Lz);//Length of the channel measured in micrometers along z direction.
		scanf("%lf",&Lzcon);//Length of the constriction of the channel measure in micrometers along z direction

		scanf("%c",&top_surface);
		scanf("%c",&bottom_surface);

		//Simulated annealing parameters
		scanf("%lf",&delta);//the perturbation distance between neighbouring points
		scanf("%lf",&sim_anneal_scale); //the rate at which kt is decreased between loops
		scanf("%lf",&energy_inc_limit);

	}
	FILE* options_used;
	options_used=fopen("options_used.txt","w");

	if(straintype=='o') 	fprintf(options_used,"solid mode\n");
	if(straintype=='h')	fprintf(options_used,"shell mode\n");

	if(volume_mode=='s')	fprintf(options_used,"symmetric shape if 2d data\n");
	if(volume_mode=='f')	fprintf(options_used,"filled channel (only if 2d data)\n");

	if(strainmode=='l') 	fprintf(options_used,"linear strain assumed\n"); //n for non linear strain, l for linearised 
	if(strainmode=='n') 	fprintf(options_used,"non linear strain assumed\n"); //n for non linear strain, l for linearised 

 	fprintf(options_used,"dimensions=%d\n",dimensions);
 	fprintf(options_used,"mode2d=%c\n",mode2d); 
		
	if(alignment_pos=='r') 		fprintf(options_used,"aligned by centre of mass\n"); //n for non linear strain, l for linearised 
	else if(alignment_pos=='c') 	fprintf(options_used,"aligned by rear\n"); //n for non linear strain, l for linearised 		

	if(incompressible=='y') 	fprintf(options_used,"incompressible\n");
	else				fprintf(options_used,"compressible\n"); 

	fprintf(options_used,"xysize=%f\n",xy_pixelsize); 	
	fprintf(options_used,"zstack separation=%f\n",zstack_separation);

	fprintf(options_used,"stating poissons ratio=%f \n Youngs mod=%f\n",sigma,E);

	fprintf(options_used,"channel_size=%f constriction size=%f\n", Lz,Lzcon);

	fprintf(options_used,"if 3d : top surface=%c bottom surface=%c\n",top_surface,bottom_surface);

	fprintf(options_used,"simulated annealing parameters: delta=%f sim_anneal_scale=%f energy_inc_limit=%f\n",delta,sim_anneal_scale,energy_inc_limit);

	//print_options_used(options_used);
	

	int i,j,k;
	//create all the arrays/variables needed pair of 2d arrays, source/targets as normal, with x,y coordinates
	int iterations=0;

	int ncols=3;//one for each coordinate
	int nrows=100;//bit arbitrary at the moment for testing, give each shape this many points.

	//read in the data from a file for entry/exit coordinates on the source and target shapes.
	//the data is read in to the four variables below, seperate variables to allow for microscope to have moved and therefore changed coordinates relatively between images when created from imagej files or from the averaging method over images.
	double entry_source,exit_source,entry_target,exit_target;

	fscanf(entry_exit_coordinates,"%lf %lf %lf %lf",&entry_source,&exit_source,&entry_target,&exit_target);

//	printf("starting pois ratio?\n");
//	scanf("%lf",&sigma);

	//double prev_pois=sigma;//initialise this for checks later.
	//double initial_pois=sigma;//set the initial sigma to whatever is entered. Not used right now...

	double sigma_tolerance=pow(10,-7);//the tolerence to which we should determine the value of poisson ratio.
	double sigma_tolerance_energy=0.01;//the tolerance to which we expect same sigma between energy minimisations

	FILE* poisson_file;
	poisson_file=fopen("poisson_ratio.txt","w");
	fprintf(poisson_file,"#This file contains the output result of the poisson ratio for the input shapes given \n");

	double output_pois;
	double test_weighted_pois;
	double xij,yij,rij,drdx,drdy,dthetadx,dthetady,duxradial,duyradial,dr,duxdr,duydr,rim1,rip1,dtp1,dt,duxdtheta,duydtheta,duxdx,duxdy;
	double duydx,duydy,duxdz,duydz,duzdy,duzdx,duzdz;
	double total_linelength;
	double z_current,z_previous;
	int ip1,im1;
	int nstresscols=6;
	
	double area_before=0,area_after=0,area_change,ai;
	double meanp=0;
	double weighted_meanp=0;
	double*** source_shape;
	double*** target_shape;

	FILE* source_file;
	if((source_file=fopen(sourcename,"r"))==NULL)
		{ printf("warning unable to open input source file, exiting\n"); return 0;}

	FILE* target_file;
	if((target_file=fopen(targetname,"r"))==NULL)
		{ printf("warning unable to open input target file, exiting\n"); return 0;}

	int source_stacks=1;
	int target_stacks=1;
	count_stacks(source_file,dimensions,&source_stacks);
	printf("source_stacks=%d\n",source_stacks);
	count_stacks(target_file,dimensions,&target_stacks);
	printf("target_stacks=%d\n",target_stacks);

	int* targetrows;
	array_alloc_1d_int(&targetrows,target_stacks);
	int* sourcerows;
	array_alloc_1d_int(&sourcerows,source_stacks);

	count_xypoints(source_file,dimensions,sourcerows,source_stacks);
	printf("source memory assigned: sourcerows=%d source_stacks=%d\n",sourcerows[0],source_stacks);
	count_xypoints(target_file,dimensions,targetrows,target_stacks);
	printf("target memory assigned: targetrows=%d target_stacks=%d\n",targetrows[0],target_stacks);

	//now find the biggest stack, so know how much memory to allocate.
	int points_in_biggest_stack=0;
	for(j=0;j<(target_stacks);j++)
	{
		if((targetrows[j])>points_in_biggest_stack) points_in_biggest_stack=(targetrows[j]);		
	}
	for(j=0;j<(source_stacks);j++)
	{
		if((sourcerows[j])>points_in_biggest_stack) points_in_biggest_stack=(sourcerows[j]);		
	}

	array_alloc_3d_dbl(&target_shape,points_in_biggest_stack,target_stacks,3);	
	read_3d(target_file,dimensions,target_shape,targetrows,target_stacks);

	array_alloc_3d_dbl(&source_shape,points_in_biggest_stack,source_stacks,3);	
	read_3d(source_file,dimensions,source_shape,sourcerows,source_stacks);


	//for(j=0;j<target_stacks;j++)	for(i=0;i<targetrows[j];i++) {target_shape[i][j][0]*=3; target_shape[i][j][1]*=3;}
	//for(j=0;j<source_stacks;j++)	for(i=0;i<sourcerows[j];i++) {source_shape[i][j][0]*=2; source_shape[i][j][1]*=2;}
	printf("files read in\n");
	//now remesh the shapes to have the same number of points at each height and in the target and source shapes.
	int refstack=0;
	//mesh_xy(source_shape,target_shape,&sourcerows,&targetrows,&source_stacks,&target_stacks,refstack);
	FILE* outtest_before=fopen("testout_beforemesh3d.txt","w");
	for(j=0;j<source_stacks;j++) for(i=0;i<sourcerows[j];i++) fprintf(outtest_before,"%f %f\n",source_shape[i][j][0],source_shape[i][j][1]);

	printf("beforemesh 3d: targetrows=%d target_stacks=%d\n",targetrows[0],target_stacks);
	mesh_3d_xy(source_shape,target_shape,sourcerows,targetrows,source_stacks,target_stacks,refstack);
	FILE* outtest_after=fopen("testout_aftermesh3d.txt","w");

	for(j=0;j<source_stacks;j++) for(i=0;i<sourcerows[j];i++) fprintf(outtest_after,"%f %f\n",source_shape[i][j][0],source_shape[i][j][1]);

	printf("after mesh3d: sourcerows=%d source_stacks=%d\n",sourcerows[0],source_stacks);
	printf("after mesh3d: targetrows=%d target_stacks=%d\n",targetrows[0],target_stacks);

	//now that all points are meshed to have the same number of rows on both the target and source stacks, equal to targetrows[refstack], allocate memory for the other arrays of this size.	

	double*** deformation_array;
	array_alloc_3d_dbl(&deformation_array,sourcerows[refstack],source_stacks,ncols);
	
	double* area_array; //contains the areas of each segment ai.
	array_alloc_1d_dbl(&area_array,sourcerows[refstack]);

	double* linelength_array;//contains the length of the curve along the shape calculated on between midpoints of each segment. 
	array_alloc_1d_dbl(&linelength_array,sourcerows[refstack]);

	int nstraincols=6;
	double*** strain_array;
	array_alloc_3d_dbl(&strain_array,sourcerows[refstack],source_stacks,nstraincols);

	double*** stress_array;
	array_alloc_3d_dbl(&stress_array,sourcerows[refstack],source_stacks,nstresscols);

	double*** traction_array;
	array_alloc_3d_dbl(&traction_array,sourcerows[refstack],source_stacks,ncols);

	double* pressure_array;
	array_alloc_1d_dbl(&pressure_array,sourcerows[refstack]);

	double*** christoffel_array;
	array_alloc_3d_dbl(&christoffel_array,sourcerows[refstack],source_stacks,18);

	double*** metric;
	array_alloc_3d_dbl(&metric,sourcerows[refstack],source_stacks,6);	

	double*** inverse_metric;
	array_alloc_3d_dbl(&inverse_metric,sourcerows[refstack],source_stacks,6);
	
	double*** curvature_tensor;
	array_alloc_3d_dbl(&curvature_tensor,sourcerows[refstack],source_stacks,6);

	double s1,n,nx,ny,nz,Fx,Fy,Fz;
	double e1x,e1y,e1z,e2x,e2y,e2z,p;
	printf("realigning shape\n");
	//realign the shape if needed
	realign(source_shape,target_shape,targetrows,sourcerows,target_stacks,source_stacks);
	printf("shape realigned\n");
	for(j=0;j<target_stacks;j++)
	{		
		for(i=0;i<targetrows[refstack];i++)
		{	//printf("i=%d j=%d\n",i,j);
			metric_tensor_3d(metric,inverse_metric,i,j,target_shape,refstack,targetrows,target_stacks);
			chr_symbols_3d(christoffel_array,metric,inverse_metric,target_shape,i,j,targetrows[j],target_stacks);
		}
	}

	volume_mode='f';
	printf("finding volume of source, volumemode=%c\n",volume_mode);
	double volume_source=volume_calc(source_shape,sourcerows,source_stacks,volume_mode,entry_source,exit_source);
	printf("source volume=%f\n\n\n\n",volume_source);	
	
	printf("finding volume of target, volumemode=%c entry_target=%f exit_target=%f\n",volume_mode,entry_target,exit_target);
	double volume_target=volume_calc(target_shape,targetrows,target_stacks,volume_mode,entry_target,exit_target);
	printf("target volume=%f\n",volume_target);

	//The volume before and after are given by the shapes entered are stored in volume_source, volume_target. This is used in the calculation of poisson ratio
	double volume_change=volume_target-volume_source;	
	//set the initial deformation between the two shapes, by choosing the lowest energy possible configuration between any possible combination of the pixels on the target/source shapes
	if(volume_change>0) printf("The overall volume change is a expansion by %f\n",volume_change);
	if(volume_change<0) printf("The overall volume change is a compression by %f\n",volume_change);

	//initial_deformation_3d(double*** source_array,double*** target_array,double*** deformation_array,double*** christoffel_array,double***metric,double*** inverse_metric,double*** curvature_tensor,int* source_pointscount,int initialised,int *target_pointcalc,int num_zstacks,int refstack,double entry_x,double exit_x);
	initial_deformation_3d(source_shape,target_shape,deformation_array,christoffel_array,metric,inverse_metric,curvature_tensor,sourcerows,targetrows,target_stacks,refstack,entry_source,exit_source);
	printf("Initial deformation found\n");

	curvature_tensor_3d(curvature_tensor,source_shape,sourcerows[0],source_stacks);
	for(j=0;j<source_stacks;j++)
		for(i=0;i<sourcerows[j];i++)
			{
				strain_calc_3d(christoffel_array,metric,inverse_metric,source_shape,strain_array,deformation_array,curvature_tensor,i,j,sourcerows[refstack],target_stacks,entry_source,exit_source);	
				stress_calc_3d(source_shape,strain_array,stress_array,i,j);
				printf("%f %f curvature = %f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],curvature_tensor[i][j][0],curvature_tensor[i][j][1],curvature_tensor[i][j][2],curvature_tensor[i][j][3]);
			}

	//find length before
	double linelength_before=0;
	for(j=0;j<source_stacks;j++)
	{
		for (i=0;i<sourcerows[j];i++)
		{
			if(i==(sourcerows[j]-1)) {ip1=0; im1=i-1;}
			else if (i==0) {ip1 = i+1; im1=sourcerows[j]-1;}
			else { ip1=i+1; im1=i-1;}

			//length of segments between two neighbouring midpoints, for the mean pressure weighting towards the end of function.
			linelength_before+=1.0/2.0*sqrt((source_shape[ip1][j][0]-source_shape[i][j][0])*(source_shape[ip1][j][0]-source_shape[i][j][0])+(source_shape[ip1][j][1]-source_shape[i][j][1])*(source_shape[ip1][j][1]-source_shape[i][j][1]))+1.0/2.0*sqrt((source_shape[i][j][0]-source_shape[im1][j][0])*(source_shape[i][j][0]-source_shape[im1][j][0])+(source_shape[i][j][1]-source_shape[im1][j][1])*(source_shape[i][j][1]-source_shape[im1][j][1]));
		}
	}

	//find length after.
	double linelength_after=0;
	for(j=0;j<target_stacks;j++)
	{
		for (i=0;i<targetrows[j];i++)
		{
			if(i==(targetrows[j]-1)) {ip1=0; im1=i-1;}
			else if (i==0) {ip1 = i+1; im1=targetrows[j]-1;}
			else { ip1=i+1; im1=i-1;}

			//length of segments between two neighbouring midpoints, for the mean pressure weighting towards the end of function.
			linelength_after+=1.0/2.0*sqrt((target_shape[ip1][j][0]-target_shape[i][j][0])*(target_shape[ip1][j][0]-target_shape[i][j][0])+(target_shape[ip1][j][1]-target_shape[i][j][1])*(target_shape[ip1][j][1]-target_shape[i][j][1]))+1.0/2.0*sqrt((target_shape[i][j][0]-target_shape[im1][j][0])*(target_shape[i][j][0]-target_shape[im1][j][0])+(target_shape[i][j][1]-target_shape[im1][j][1])*(target_shape[i][j][1]-target_shape[im1][j][1]));
		}
	}

	printf("the total length of the target curve is %f\n",total_linelength);
	printf("minimising energy\n");

	FILE* deformation_before_energymin_test;
	deformation_before_energymin_test=fopen("deformation_before_energymin_test.txt","w");
	fprintf(deformation_before_energymin_test,"# File format: x pos (source) y pos (source) x pos (target) y pos (target) ux uy\n");
		for(j=0;j<target_stacks;j++)
			for(i=0;i<targetrows[j];i++)	fprintf(deformation_before_energymin_test,"%f %f %f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],target_shape[i][j][0],target_shape[i][j][1],deformation_array[i][j][0],deformation_array[i][j][1]);
	fclose(deformation_before_energymin_test);

	energy_min_s1(christoffel_array,metric,inverse_metric,source_shape,target_shape,strain_array,stress_array,deformation_array,curvature_tensor,sourcerows,source_stacks,entry_source,exit_source);
	if(target_stacks>1 && source_stacks>1) 
	{	//if 3d data, minimise energy in that direction too.
		energy_min_s2(target_shape,source_shape,strain_array,deformation_array,metric,inverse_metric,christoffel_array,curvature_tensor,stress_array,1,targetrows[0],target_stacks,entry_target,exit_target);
	}
	
	FILE* deformation_after_energymin_test;
	deformation_after_energymin_test=fopen("deformation_after_energymin_test.txt","w");
	fprintf(deformation_after_energymin_test,"# File format: x pos (source) y pos (source) x pos (target) y pos (target) ux uy\n");
		for(j=0;j<target_stacks;j++)
			for(i=0;i<targetrows[j];i++)	fprintf(deformation_after_energymin_test,"%f %f %f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],target_shape[i][j][0],target_shape[i][j][1],deformation_array[i][j][0],deformation_array[i][j][1]);
	fclose(deformation_after_energymin_test);

	//printf("updating poissons ratio\n");
	//iterate_poissons_ratio(target_shape,targetrows,target_stacks,refstack,&output_pois,stress_array,strain_array,traction_array,linelength_before,linelength_after);
	//iterate_poissons_ratio(source_shape,sourcerows,source_stacks,refstack,&output_pois,stress_array,strain_array,traction_array,linelength_before,linelength_after);

	FILE* startshape_file;
	startshape_file=fopen("start_shape.txt","w");
	fprintf(startshape_file,"# File format: x pos y pos \n");
	for(j=0;j<source_stacks;j++)				
		for(i=0;i<sourcerows[j];i++)	fprintf(startshape_file,"%f %f \n",source_shape[i][j][0],source_shape[i][j][1]);
	fclose(startshape_file);

	FILE* finalshape_file;
	finalshape_file=fopen("final_shape.txt","w");
	fprintf(finalshape_file,"# File format: x pos y pos \n");
	for(j=0;j<target_stacks;j++)	
		for(i=0;i<targetrows[j];i++)	fprintf(finalshape_file,"%f %f %f %f\n",target_shape[i][j][0],target_shape[i][j][1],deformation_array[i][j][0],deformation_array[i][j][1]);
	fclose(finalshape_file);

	FILE* deformation_file;
	deformation_file=fopen("deformation.txt","w");
	fprintf(deformation_file,"# File format: (source) x pos y pos z pos (target) x pos y pos z pos (deformations) ux uy uz\n");
		for(j=0;j<target_stacks;j++)
			for(i=0;i<targetrows[j];i++)	fprintf(deformation_file,"%f %f %f %f %f %f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],source_shape[i][j][2],target_shape[i][j][0],target_shape[i][j][1],target_shape[i][j][2],deformation_array[i][j][0],deformation_array[i][j][1],deformation_array[i][j][2]);
	fclose(deformation_file);
	
	FILE* strain_file;
	strain_file=fopen("strain.txt","w");
	fprintf(strain_file,"# File format: x pos y pos z pos uxx uxy uyy uxz uxy uzz\n");
	for(j=0;j<target_stacks;j++)
		for(i=0;i<targetrows[j];i++)	fprintf(strain_file,"%f %f %f %f %f %f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],source_shape[i][j][2],strain_array[i][j][0],strain_array[i][j][1],strain_array[i][j][2],strain_array[i][j][3],strain_array[i][j][4],strain_array[i][j][5]);
	fclose(strain_file);

	FILE* metric_file;
	metric_file=fopen("metric.txt","w");
	for(j=0;j<target_stacks;j++)
		for(i=0;i<targetrows[j];i++)
			fprintf(metric_file,"%f %f %f %f %f %f\n",metric[i][j][0],metric[i][j][1],metric[i][j][2],metric[i][j][3],metric[i][j][4],metric[i][j][5]);
	
	FILE* christoffel_file;
	christoffel_file=fopen("christoffel.txt","w");
	for(j=0;j<target_stacks;j++)
		for(i=0;i<targetrows[j];i++)	
			fprintf(christoffel_file,"%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",christoffel_array[i][j][0],christoffel_array[i][j][1],christoffel_array[i][j][2],christoffel_array[i][j][3],christoffel_array[i][j][4],christoffel_array[i][j][5],christoffel_array[i][j][6],christoffel_array[i][j][7],christoffel_array[i][j][8],christoffel_array[i][j][9],christoffel_array[i][j][10],christoffel_array[i][j][11],christoffel_array[i][j][12],christoffel_array[i][j][13],christoffel_array[i][j][14],christoffel_array[i][j][15],christoffel_array[i][j][16],christoffel_array[i][j][17]);
		
	traction_force(source_shape,sourcerows,source_stacks,stress_array,traction_array);
	FILE* force_file;
	force_file=fopen("force.txt","w");
	fprintf(force_file,"# File format: x pos y pos Tx Ty\n");
	for(j=0;j<source_stacks;j++)
		for(i=0;i<sourcerows[j];i++)
			fprintf(force_file,"%f %f %f %f\n",source_shape[i][j][0],source_shape[i][j][1],traction_array[i][j][0],traction_array[i][j][1]);
	fclose(force_file);

	

	FILE* stress_file;
	stress_file=fopen("stress.txt","w");
	fprintf(stress_file,"# File format: x pos y pos z pos  Sxx Sxy Syy Sxz Syz Szz\n");
	for(j=0;j<target_stacks;j++)
		for(i=0;i<targetrows[j];i++)
			fprintf(stress_file,"%f %f %f %f %f %f %f %f %f \n",source_shape[i][j][0],source_shape[i][j][1],source_shape[i][j][2],stress_array[i][j][0],stress_array[i][j][1],stress_array[i][j][2],stress_array[i][j][3],stress_array[i][j][4],stress_array[i][j][5]);
	fclose(stress_file);

	
//printf("area before=%f area after=%f area_change=%f\n",area_before,area_after,area_change);


	//test to print the strain components in us1ns2 basis.
	

	//calculates the strain at a point i,j on the body, in a form decided by the mode chosen at top of this header file, or in input file.
	//printf("straincalc target_zstacks=%d\n",target_zstacks);

	//printf("strain test i=%d j=%d\n",i,j);
	double us1_i,us2_i,un_i,us1_ip1,us2_ip1,un_ip1,us1_im1,us2_im1,un_im1;
	double us1_jp1,us2_jp1,un_jp1,us1_jm1,us2_jm1,un_jm1;

	double dus1ds1,dus1ds2,dus1dn;
	double dus2ds1,dus2ds2,dus2dn;
	double dunds1,dunds2,dundn;
	
	double us1s1,us1s2,us1n,us2n,us2s2,unn;
	double s2;//the distances between points in the s1,s2 directions. used for the derivatives on the surface which give the unit 				vectors.

	double es1_dot_ds1u,es1_dot_ds2u,es2_dot_ds1u,es2_dot_ds2u,n_dot_ds1u,n_dot_ds2u;//use these to evaluate the strain in either linear or non linear form
	//first convert the values indeformation array from cartesians to s1,s2,n basis.
	double ct,st,cg,sg;
	double dx,dy,dz,normalisation;//used for rotation around s1
	
	int ip2,im2,jp1,jm1,jc;
	double theta,gamma,dtm1;
	double ds2, dus2,dun;			
	double phip1,phim1;
FILE* strain_s1basis;
strain_s1basis=fopen("strain_s1basis.txt","w");

for(j=0;j<source_stacks;j++)
for(i=0;i<sourcerows[j];i++)
{	
	four_nearest_neighbour_points_i(i,&ip1,&im1,&ip2,&im2,sourcerows[0]);
	
	if(dimensions==3)
	{
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,source_stacks);
	}
	distance_between_points_3d_array_i(source_shape,i,j,sourcerows[0],&s1);

	if(dimensions==3)	
	{	
		distance_between_points_3d_array_j(source_shape,i,j,source_stacks,&s2);
	}	
	

	//printf("using straintype h dimensions=%d\n",dimensions);
	//Calcuate the vector elements for derivatives at each point i,j in the (s1,n) [2d] or (s1,s2,n) [psuedo 3d or 3d] basis, 
	//calculate vector elements at the points i,j and those surrounding for numerical calculation of derivatives
	rotation_angles_3d(source_shape,i,j,&theta,&gamma,sourcerows[0],source_stacks);
       	rotate_vector_3d(&us1_i,&un_i,&us2_i,theta,gamma,i,j,sourcerows[0],source_shape,deformation_array,source_stacks);
	//printf("ux=%f uy=%f us1=%f un=%f\n",deformation_array[i][j][0],deformation_array[i][j][1],us1_i,un_i);
	//at points ip1,j
	rotation_angles_3d(source_shape,ip1,j,&theta,&gamma,sourcerows[0],source_stacks);
	rotate_vector_3d(&us1_ip1,&un_ip1,&us2_ip1,theta,gamma,ip1,j,sourcerows[0],source_shape,deformation_array,source_stacks);

	//at point im1,j
	rotation_angles_3d(source_shape,im1,j,&theta,&gamma,sourcerows[0],source_stacks);
	rotate_vector_3d(&us1_im1,&un_im1,&us2_im1,theta,gamma,im1,j,sourcerows[0],source_shape,deformation_array,source_stacks);
		
	if(dimensions==3 && source_stacks>1)
	{	//at point i,jp1, if 3d
		rotation_angles_3d(source_shape,i,jp1,&theta,&gamma,sourcerows[0],source_stacks);
		rotate_vector_3d(&us1_jp1,&un_jp1,&us2_jp1,theta,gamma,i,jp1,sourcerows[0],source_shape,deformation_array,source_stacks);

		//at point i,jm1, if 3d
		rotation_angles_3d(source_shape,i,jm1,&theta,&gamma,sourcerows[0],source_stacks);
		rotate_vector_3d(&us1_jm1,&un_jm1,&us2_jm1,theta,gamma,i,jm1,sourcerows[0],source_shape,deformation_array,source_stacks);
	}


	distance_between_points_3d_array_i(source_shape,i,j,sourcerows[0],&s1);
	//the us1, us2 and un derivatives along s1 can still be calculated numerically from the single stack of data;
	dus1ds1=(us1_ip1-us1_im1)/s1;
	dunds1=(un_ip1-un_im1)/s1;

	//normal derivatives of deformations are 0 because of shell mode 			
	dus1dn=0;
	dundn=0;
	dus2dn=0;

	//the remaining terms are defined by the assumption about the out of plane direction.

	//[0]=sss//[1]=s_sphi=s_phis//[3]=^s_phiphi,
	//[12]=^phi_ss//[13]=^phi_sphi=^phi_phis//[14]=^phi_sn=^phi_ns//[15]=^phi_phiphi,//[16]=^phi_nphi=phin,//[17]=^phi_nn 
	//evaluate strain components, use full 3d expression, but with the christoffel and cuverture tensors values adapted for the current pseudo 3d shape (see those functions).
	us1s1=dus1ds1+christoffel_array[i][j][0]*us1_i+un_i*curvature_tensor[i][j][0]+us2_i*christoffel_array[i][j][1];
	us2s2=dus2ds2+us1_i*christoffel_array[i][j][13]+us2_i*christoffel_array[i][j][15]+un_i*curvature_tensor[i][j][3];
	unn=0;
	us1s2=0.5*(dus1ds2+us2_i*christoffel_array[i][j][3]+us1_i*christoffel_array[i][j][1]+un_i*curvature_tensor[i][j][2]+dus2ds1+us2_i*christoffel_array[i][j][13]+us1_i*christoffel_array[i][j][12]+un_i*curvature_tensor[i][j][1]);
	us1n=0.5*dunds1;
	us2n=0.5*dunds2;
	//printf("pos: %f %f strains:%f %f %f %f %f %f\n",pos_array[i][j][0],pos_array[i][j][1],us1s1,us2s2,unn,us1s2,us1n,us2n);
	//printf("us1s1:%f dusds1=%f us1_i=%f csss=%f us2_i=%f csps=%f un_i=%f curv=%f\n",us1s1,dus1ds1,us1_i, christoffel_array[i][j][0],us2_i,christoffel_array[i][j][1],un_i,curvature_tensor[i][j][0]);
	//printf("us1s1=%f us2s2=%f unn=%f us1s2=%f us1n=%f us2n=%f\n",us1s1,us2s2,unn,us1s2,us1n,us2n);
	//and rotate back to cartesians basis		
	//rotation_angles_3d(source_shape,i,j,&theta,&gamma,target_points,num_zstacks);
	//gamma=0;//again from the symmetry, this is required.
	//theta=-theta;
	rotate_strain_to_cart(source_shape,strain_array,us1s1,us1s2,us1n,us2s2,us2n,unn,i,j,theta,gamma,sourcerows[j],source_stacks);
	//	if(sigma>0.49)us2s2=-(us1s1
	fprintf(strain_s1basis,"%f %f|%f %f|%f|%f %f| %f |%f %f %f %f %f %f|%f %f|%f %f\n",source_shape[i][j][0],source_shape[i][j][1],deformation_array[i][j][0],deformation_array[i][j][1],theta,us1_i,un_i,curvature_tensor[i][j][0],us1s1,us1s2,us1n,us2s2,us2n,unn, strain_array[i][j][0],strain_array[i][j][2],stress_array[i][j][0],stress_array[i][j][2]);
}
	
return 0;
}



