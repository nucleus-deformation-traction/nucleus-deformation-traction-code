#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<unistd.h>
//#include<sys/stat.h>

char options_input='n';//if y, ask for options from user, anything else just uses the below options.

//constants used +options

//Options
char straintype='o';//o for solid, h for shell 
char volume_mode='f';//defines the out of plane direction for shapes where n_zstacks=1;. s = symmetric, f= filled channel (to the sizes given by Lz,Lzcon below)
char iterate_poissons_char='n';//if yes, attempt to calculate the possion ratio from images
char minimise_energy='y';//if yes, minimise energy

char printoutput='o';//a for all points over the body, o for only the outline to text file

char strainmode='n'; //n for non linear strain, l for linearised 
int num_innershapes=100; //Number of shapes the surface is broken in to.

int dimensions=3;//2 for 2d or pseudo 3d (analytic approx for third dim), 3 for 3d (set of zstacks).
char mode2d='p';//c for completely 2d, p for pseudo 3d, doesn't do anything in 3d case. y for cylinder (since c already taken...)

char shape_to_calculate_on='s';//t, shape calculated strain/stress/traction/etc on is the target,s, undeformed shape.
char alignment_pos='c';//r for rear aligned, c for centre aligned,g for a point entered by user.
char out_plane_geom='f';//used only in 2d or psuedo 3d modes, doesn't do anything in 3d case: defines the geometry in direction not seen in the images
char incompressible='n';//if incompressible, sets out of plane strain = the negative of the sum of the other two. Also happens if poissons ratio is close to 0.5
//image parameters

double xy_pixelsize=0.05;//in micro meters
double zstack_separation=0.30;//in micro meters
int z_coordinate=1;//1 if the input file has z in the first column, assumes 3 otherwise
//Elastic Constants
double sigma=0.4;//poission ratio

double E=5000.0; //Elastic Modulus

//Shell parameters
double Lz=5;//Length of the channel measured in micrometers along z direction.
double Lzcon=3.43;//Length of the constriction of the channel measure in micrometers along z direction
double A=1;
double d=0.1;

char top_surface='b';
char bottom_surface='b';
//Simulated annealing parameters
double delta=0.1;//the perturbation distance between neighbouring points
double sim_anneal_scale=0.99; //the rate at which kt is decreased between loops
double energy_inc_limit=1.1;

//function prototypes 
//alignment related functions
void realign(double*** source_array,double*** target_array, int* targetpoints, int* sourcepoints,int target_stacks,int source_stacks);
void cent_masscalc3d(double*** shape_array,int* pointcount_array,int num_stacks);

//memory allocation/freeing function prototypes
void array_alloc_1d_dbl(double **data,int nrows);
void array_alloc_1d_int(int **data,int nrows);
void array_alloc_2d_dbl(double ***data, int nrows, int ncols);
void array_alloc_3d_dbl(double ****data, int nrows, int ncols,int ndepth);
void free_array_1d_dbl(double **data);
void free_array_1d_int(int **data);
void free_array_2d_dbl(double ***data,int nrows);
void free_array_3d_dbl(double ****data, int nrows, int ncols,int ndepth);
void traction_force(double*** shape_array,int* num_points,int num_stacks,double*** stress_array,double*** traction_array);


//energy minimisation functions, deformation, perturbations, strain, stress, traction
double free_energy_calc_3d(double*** strain_array,double*** stress_array,int i,int j);
double free_energy_calc(double** strain_array,double** stress_array,int ndisplace);
void deformation_perturb_s1(double*** source_array,double*** deformation_array,int j,int num_xypoints,int num_zstacks);
void straincalc_2dsolid(double*** target_shape,double*** deformation_array,double*** strain_array,int nrows);
void stress_calc_3d(double*** pos_array,double*** strain_array,double*** stress_array,int i,int j);

void rotate_strain(double** strain_array,double uss,double usn, double usp,double unn,double unp,double upp, double theta,int i,double dxds,double dyds,double r,double y);
void curvature_tensor_3d(double*** curvature_tensor,double*** shape_array,int shape_points,int num_zstacks);
void strain_calc_3d(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** pos_array,double ***strain_array,double ***deformation_array,double*** curvature_tensor,int i,int j,int target_points,int target_zstacks,double entry_x, double exit_x);
void rotate_strain_to_cart(double*** pos_array,double*** strain_array,double us1s1,double us1s2,double us1n,double us2s2,double us2n,double unn,int i,int j,double theta,double gamma,int target_points,int num_zstacks);
double traction_3d(double*** pos_array,double*** stress_array,int i,int j,int nrows,int num_zstacks, double* Fx, double* Fy,double* Fz, double* nx, double* ny,double* nz);
void initial_deformation_3d(double*** source_array,double*** target_array,double*** deformation_array,double*** christoffel_array,double***metric,double*** inverse_metric,double*** curvature_tensor,int* source_pointscount,int *target_pointcalc,int num_zstacks,int refstack,double entry_x,double exit_x);

void energy_min_s2(double*** target_array,double*** source_array,double*** strain_array,double*** deformation_array,double*** metric, double*** inverse_metric,double*** christoffel_array,double*** curvature_tensor,double*** stress_array,int initialised,int num_xypoints,int num_zstacks,double entry_x,double exit_x);
void energy_min_s1(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** source_array,double*** target_array,double ***strain_array,double*** stress_array,double ***deformation_array,double*** curvature_tensor,int* source_pointcalc,int num_zstacks,double entry_x,double exit_x);
int open_output_files(FILE** strainout,char* strainname, FILE** stressout,char* stressname,FILE** meshsource,char* meshsourcename,FILE** meshtarget,char* meshtargetname,FILE** christoffelout,char* christoffelname,FILE** deformationout,char* deformationname,FILE** tracout,char* tractionname);

void iterate_poissons_ratio(double*** source_shape,double*** target_shape,int* shape_rows,int num_zstacks,int refstack,double* output_pois,double*** stress_array,double*** strain_array,double*** traction_array);

//geometry and meshing related functions, mostly used in 3d mode.
void mesh_xy(double*** source_array,double ***target_array,int* sourcerows,int* targetrows,int* source_stacks,int* target_stacks,int refstack);
void mesh_3d_xy(double*** source_array,double ***target_array,int* source_pointcount,int* target_pointcount,int source_stacks,int target_stacks,int refstack);
void mesh_3d_z(double*** source_array,double ***target_array,int* source_pointcount,int* target_pointcount,int source_stacks,int* target_stacks_pointer,int refstack);
void chr_symbols_3d(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** pos_array,int i,int j,int nrows,int num_zstacks);
void rotation_angles_3d(double*** pos_array,int i,int j,double* theta,double* phi,int num_xypoints,int num_zstacks);
void rotate_vector_3d(double* us1,double* un,double* us2,double theta, double gamma,int i, int j, int num_xypoints, double*** pos_array,double*** deformation_array,int num_zstacks);

double first_derivative_array(int ip1,int jc,double*** shape_array);//calculates a first derivative along an array
double second_derivative_array(int ip1,int jc,double*** shape_array);

void two_nearest_neighbour_points_i(int i,int* ip1,int* im1,int nrows);
void four_nearest_neighbour_points_i(int i,int* ip1,int* im1,int* ip2,int* im2,int nrows);
void two_nearest_neighbour_points_j(int j,int* jc,int* jp1,int* jm1,int nstacks);
void four_nearest_neighbour_points_j(int j,int* jc,int* jp1,int* jm1,int* jp2,int* jm2,int nstacks);
void distance_between_points_3d_array_i(double*** shape_array,int i,int j,int nrows,double* s1);
void distance_between_points_3d_array_j(double*** shape_array,int i,int j,int nstacks,double* s2);
void direction_vectors_i(double*** shape_array,double* e1x,double* e1y,double* e1z,int i,int j, int nrows);
void direction_vectors_j(double*** shape_array,double* e2x,double* e2y,double* e2z,int i,int j, int nstacks);

void orientate_curve_after_remeshing(double*** shape_array, int num_zstacks,int points);
void orientate_curve_before_remeshing(double*** shape_array, int num_zstacks,int* pointcount_array);
//input and output related functions
void open_input_file(FILE** input1,char* inputname);
//void read_3d_and_count(FILE* inputfile,int dimensions,double**** input_array,int** input_count,int* num_stacks);
void read_3d(FILE* inputfile,int dimensions,double*** input_array,int* input_count,int num_stacks);
void count_stacks(FILE* inputfile,int dimensions,int* stacks);
void invert_metric(double*** metric,double*** inverse_metric,int dimensions,int i,int j);
void normal_vector(double e1x,double e1y,double e1z,double e2x,double e2y,double e2z,double* nx,double* ny,double* nz,int dimensions,int num_zstacks);

int check_exit(double*** shape,int i,int j,int* numpoints,int entry_x,int exit_x,int first_pixel_out_con,int last_pixel_out_con);
int check_entry(double*** shape,int i,int j,int* numpoints,int entry_x,int exit_x,int first_pixel_in_con,int last_pixel_in_con);
void area_in_out_con(double*** position_array, int first_pixel_in_con,int last_pixel_in_con, int first_pixel_out_con, int last_pixel_out_con,double entry_x,double exit_x,double* area_in,double* area_out, double* area,int* numpoints);
void metric_tensor_3d_shell(double ***metric,double ***inverse_metric,int i, int j,double ***pos_array,int nrows,int num_zstacks);
void metric_tensor_3d(double*** metric,double*** inverse_metric,int i,int j,double*** shape,int refstack,int* shaperows,int z_stacks);

void print_options_used(FILE* options_used)
{

	if(straintype=='o') 	fprintf(options_used,"solid mode\n");
	if(straintype=='h')	fprintf(options_used,"shell mode\n");

	if(volume_mode=='s')	fprintf(options_used,"symmetric shape if 2d data\n");
	if(volume_mode=='f')	fprintf(options_used,"filled channel (only if 2d data)\n");

	if(strainmode=='l') 	fprintf(options_used,"linear strain assumed\n"); //n for non linear strain, l for linearised 
	if(strainmode=='n') 	fprintf(options_used,"non linear strain assumed\n"); //n for non linear strain, l for linearised 

 	fprintf(options_used,"dimensions=%d\n",dimensions);
 	fprintf(options_used,"mode2d=%c\n",mode2d); 
		
	if(alignment_pos=='r') 		fprintf(options_used,"aligned by centre of mass\n"); //n for non linear strain, l for linearised 
	else if(alignment_pos=='c') 	fprintf(options_used,"aligned by rear\n"); //n for non linear strain, l for linearised 		

	if(incompressible=='y') 		fprintf(options_used,"incompressible\n");
	else			 		fprintf(options_used,"compressible\n"); 

	fprintf(options_used,"xysize=%f\n",xy_pixelsize); 	
	fprintf(options_used,"zstack separation=%f\n",zstack_separation);

	fprintf(options_used,"stating poissons ratio=%f \n Youngs mod=%f\n",sigma,E);

	fprintf(options_used,"channel_size=%f constriction size=%f\n", Lz,Lzcon);

	fprintf(options_used,"if 3d : top surface=%c bottom surface=%c\n",top_surface,bottom_surface);

	fprintf(options_used,"simulated annealing parameters: delta=%f sim_anneal_scale=%f energy_inc_limit=%f\n",delta,sim_anneal_scale,energy_inc_limit);
	return;
}

void realign(double*** source_array,double*** target_array, int* targetpoints, int* sourcepoints,int target_stacks,int source_stacks)
{
	double fixed_point=0,nucleus_length=0;
	int i,j;
	j=0;
	if(alignment_pos=='c')
	{
		printf("aligning shapes about centre of mass\n");
		//centre of mass aligned.
		cent_masscalc3d(source_array,sourcepoints,source_stacks);
		cent_masscalc3d(target_array,targetpoints,target_stacks);
		fixed_point=0.0;
	}
	else if(alignment_pos=='r'||alignment_pos=='g')
	{	//set to arbitrary large numbers initially
		double most_negative_x_target=pow(10,10);
		double most_positive_x_target=-pow(10,10);
		double most_negative_x_source=pow(10,10);
		double most_positive_x_source=-pow(10,10);
		printf("finding difference in rear points\n");
		for(j=0;j<target_stacks;j++)
		{
			for(i=0;i<targetpoints[j];i++)
			{
				if(target_array[i][j][0]<most_negative_x_target)
					most_negative_x_target=target_array[i][j][0];
				if(target_array[i][j][0]>most_positive_x_target)
					most_positive_x_target=target_array[i][j][0];
			}
		}

		for(j=0;j<source_stacks;j++)
		{
			for(i=0;i<sourcepoints[j];i++)
			{			
				if(source_array[i][0][0]<most_negative_x_source)
					most_negative_x_source=source_array[i][0][0];
			
				if(source_array[i][0][0]>most_positive_x_source)
					most_positive_x_source=source_array[i][0][0];
			}
		}	

		nucleus_length=most_positive_x_target-most_negative_x_target;
		double change_rear_x=(most_negative_x_target-most_negative_x_source);
		double change_front_x=(most_positive_x_target-most_positive_x_source);
		double ratio_between_back_front_change=-1.0;

		if(alignment_pos=='r') ratio_between_back_front_change=0.0;
		else
		{
			do
			{	
				//printf("Enter fractional alignment (relative alignment from front to back of the nucleus between frames) as a number between -1.0 to 1.0) \n");
				printf("Enter ratio of change in rear/change in front (0=fixed at rear, infinite = fixed at front, only currently accepts values in this range)\n");
				scanf("%lf",&ratio_between_back_front_change);
			}while(ratio_between_back_front_change <0.0);
		}
		//and now shift the target shape so that the rear of the source is at the same x value as the target shape, or by the amount entered
		if(ratio_between_back_front_change>=1.0)
		{	//rear moved more than the front, fixed point is towards the front, move target forwards
			if(change_front_x>0)
				fixed_point= (ratio_between_back_front_change/(1.0+ratio_between_back_front_change)-0.5)*2*(change_front_x);
			else    fixed_point= (ratio_between_back_front_change/(1.0+ratio_between_back_front_change)-0.5)*2*(-change_front_x);
		}
		else
		{	//front moved more than the rear, fixed point is towards the rear
			fixed_point= (ratio_between_back_front_change/(1.0+ratio_between_back_front_change)-0.5)*2*(change_rear_x);
			if(change_rear_x>0)
				fixed_point= (ratio_between_back_front_change/(1.0+ratio_between_back_front_change)-0.5)*2*(-change_rear_x);
			else    fixed_point= (ratio_between_back_front_change/(1.0+ratio_between_back_front_change)-0.5)*2*(+change_rear_x);
		}
		for(j=0;j<target_stacks;j++)								
		{
			for(i=0;i<targetpoints[j];i++)
			{
				target_array[i][j][0]+=fixed_point;//now move the target shape along the x axis so that the fixed point is as defined by fixed point above
			}
		}


	}
	FILE* fixed_point_file;
	fixed_point_file=fopen("fixed_point_coordinates","w");
	fprintf(fixed_point_file,"%f 0.0 \n",fixed_point);
	fclose(fixed_point_file);
	return;
	
}
void stress_calc_3d(double*** pos_array,double*** strain_array,double*** stress_array,int i,int j)
{	//0,1,2,3,4,5 elements of array are respectively xx,xy,yy,xz,yz,zz components in both stress and strain
	//i.e. [0]=xx [1]=xy[2]=yy [3]=xz [4]=yz [5]=zz

	if(dimensions==2 && mode2d=='c' && sigma<=0.49)
	{//completely 2d mode. i.e. no third dimension at all
		stress_array[i][j][0]=E/((1.0+sigma)*(1.0-2.0*sigma)) *((1.0-sigma)*strain_array[i][j][0] + sigma*(strain_array[i][j][2]));
		stress_array[i][j][1]=E/(1.0+sigma)*strain_array[i][j][1];
		stress_array[i][j][2]=E/((1.0+sigma)*(1.0-2.0*sigma)) *((1.0-sigma)*strain_array[i][j][2] + sigma*(strain_array[i][j][0]));
		stress_array[i][j][3]=0;
		stress_array[i][j][4]=0;
		stress_array[i][j][5]=0;
	
		return;
	}
	else if(dimensions==2 && mode2d=='c' && sigma>0.49)
	{//completely 2d mode. i.e. no third dimension at all, for incompressible material (poisson ratio nearly 0.5), assuming that Tr(strain)=0. Used to avoid numerical error in cases where the strains are close to 0 but not, which ends up with (finite value)/(value very close to 0)  -> which gave large results for stress and tractions.
		stress_array[i][j][0]=E/(1.0+sigma)*strain_array[i][j][0];
		stress_array[i][j][1]=E/(1.0+sigma)*strain_array[i][j][1];
		stress_array[i][j][2]=E/(1.0+sigma)*strain_array[i][j][2];
		stress_array[i][j][3]=0;
		stress_array[i][j][4]=0;
		stress_array[i][j][5]=0;
	
		return;
	}
	else if(sigma>0.49)
	{	//3d mode using zstacks, for incompressible material (poisson ratio nearly 0.5), assuming that Tr(strain)=0. Used to avoid numerical error in cases where the strains are close to 0 but not, which ends up with (finite value)/(value very close to 0)  -> which gave large results for stress and tractions.
		stress_array[i][j][0]=E/(1.0+sigma)*strain_array[i][j][0];
		stress_array[i][j][1]=E/(1.0+sigma)*strain_array[i][j][1];
		stress_array[i][j][2]=E/(1.0+sigma)*strain_array[i][j][2];
		stress_array[i][j][3]=E/(1.0+sigma)*strain_array[i][j][3];
		stress_array[i][j][4]=E/(1.0+sigma)*strain_array[i][j][4];
		stress_array[i][j][5]=E/(1.0+sigma)*strain_array[i][j][5];
		
		return;
	}
	else 
	{	//3d mode using zstacks, or defined the strain using a psuedo 3d analytic approach in straincalc.
		stress_array[i][j][0]=E/((1.0+sigma)*(1.0-2.0*sigma)) *((1.0-sigma)*strain_array[i][j][0] + sigma*(strain_array[i][j][2]+strain_array[i][j][5]));
		stress_array[i][j][1]=E/(1.0+sigma)*strain_array[i][j][1];
		stress_array[i][j][2]=E/((1.0+sigma)*(1.0-2.0*sigma)) *((1.0-sigma)*strain_array[i][j][2] + sigma*(strain_array[i][j][0]+strain_array[i][j][5]));
		stress_array[i][j][3]=E/(1.0+sigma)*strain_array[i][j][3];
		stress_array[i][j][4]=E/(1.0+sigma)*strain_array[i][j][4];
		stress_array[i][j][5]=E/((1.0+sigma)*(1.0-2.0*sigma)) *((1.0-sigma)*strain_array[i][j][5] + sigma*(strain_array[i][j][0]+strain_array[i][j][2]));
		return;
	}

	return;
}


void straincalc_2dsolid(double*** target_shape,double*** deformation_array,double*** strain_array,int nrows)
{
/*******************************************************************************************************************************************************/
//	Copy of the solid model strain calc code, because main code body required lots of definitions of things not required here.
/*******************************************************************************************************************************************************/
	
	double xij,yij,rij,drdx,drdy,dthetadx,dthetady,duxradial,duyradial,dr,duxdr,duydr,rim1,rip1,dt,dtp1,duxdtheta,duydtheta;
	double duxdx,duxdy,duxdz,duydx,duydy,duydz,duzdx,duzdy,duzdz;
	int i,ip1,im1;
	int j=0;//since 2d, all arrays only have index j=0;
	for(i=0;i<nrows;i++)
	{
		two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);

		xij=target_shape[i][j][0];
		yij=target_shape[i][j][1];
		rij=sqrt(xij*xij+yij*yij);
							
		drdx=xij/rij;
		drdy=yij/rij;
		dthetadx=-yij/(rij*rij);
		dthetady=xij/(rij*rij);
	
		duxradial=deformation_array[i][j][0]/num_innershapes;
		duyradial=deformation_array[i][j][1]/num_innershapes;
		dr=sqrt(pow(target_shape[i][j][0]/num_innershapes,2)+pow(target_shape[i][j][1]/num_innershapes,2));
		duxdr=duxradial/dr;
		duydr=duyradial/dr;

		rim1=sqrt(pow(target_shape[im1][j][0],2)+pow(target_shape[im1][j][1],2));
		rip1=sqrt(pow(target_shape[ip1][j][0],2)+pow(target_shape[ip1][j][1],2));
		
			dtp1=fabs(atan2(target_shape[ip1][j][1],target_shape[ip1][j][0])-atan2(target_shape[i][j][1],target_shape[i][j][0]));
			dt=fabs(atan2(target_shape[i][j][1],target_shape[i][j][0])-atan2(target_shape[im1][j][1],target_shape[im1][j][0]));		
			if(fabs(dt)>M_PI) dt=2*M_PI-dt; 
			if(fabs(dtp1)>M_PI) dtp1=2*M_PI-dtp1;
		duxdtheta=(deformation_array[ip1][j][0]*dt*dt*rij/rip1-deformation_array[im1][j][0]*dtp1*dtp1*rij/rim1+deformation_array[i][j][0]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
		
		duydtheta=(deformation_array[ip1][j][1]*dt*dt*rij/rip1-deformation_array[im1][j][1]*dtp1*dtp1*rij/rim1+deformation_array[i][j][1]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
	
		duxdx=duxdr*drdx+dthetadx*duxdtheta;
		duxdy=duxdr*drdy+dthetady*duxdtheta;
		duydx=duydr*drdx+dthetadx*duydtheta;
		duydy=duydr*drdy+dthetady*duydtheta;
		duxdz=0.0;
		duydz=0.0;
		duzdy=0.0;
		duzdx=0.0;
		duzdz=0.0;
		if(strainmode=='l')
		{	strain_array[i][j][0]=duxdx;
			strain_array[i][j][1]=1.0/2.0*(duxdy+duydx); 
			strain_array[i][j][2]=duydy;
			strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx); 
			strain_array[i][j][4]=1.0/2.0*(duydz+duzdy); 
			strain_array[i][j][5]=duzdz;
		}	
		if(strainmode=='n')
		{	strain_array[i][j][0]=duxdx+1.0/2.0*(duxdx*duxdx+duydx*duydx+duzdx*duzdx);
			strain_array[i][j][1]=1.0/2.0*(duxdy+duydx+2*(duxdx*duxdy+duydx*duydy+duzdx*duzdy)); 
			strain_array[i][j][2]=duydy+1.0/2.0*(duydy*duydy+duxdy*duxdy+duzdy*duzdy);
			strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx+2*(duxdx*duxdz+duydx*duydz+duzdx*duzdz)); 
			strain_array[i][j][4]=1.0/2.0*(duydz+duzdy+2*(duxdy*duxdz+duydy*duydz+duzdy*duzdz)); 
			strain_array[i][j][5]=duzdz+1.0/2.0*(duxdz*duxdz+duxdy*duxdy+duzdy*duzdy);
		}
	//	printf("Strain xx=%f, strain yy=%f Tr(s)=%f\n ",strain_array[i][j][0],strain_array[i][j][2],strain_array[i][j][0]+strain_array[i][j][2]);
	}
/*******************************************************************************************************************************************************/
//					Solid model strain calc code end 
/*******************************************************************************************************************************************************/
	return;
}

void strain_calc_3d(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** pos_array,double ***strain_array,double ***deformation_array,double*** curvature_tensor,int i,int j,int target_points,int num_zstacks, double entry_x, double exit_x)
{	//calculates the strain at a point i,j on the body, in a form decided by the mode chosen at top of this header file, or in input file.
	//printf("straincalc target_zstacks=%d\n",target_zstacks);

	//printf("strain test i=%d j=%d\n",i,j);
	double us1_i,us2_i,un_i,us1_ip1,us2_ip1,un_ip1,us1_im1,us2_im1,un_im1;
	double us1_jp1,us2_jp1,un_jp1,us1_jm1,us2_jm1,un_jm1;

	double dus1ds1,dus1ds2,dus1dn;
	double dus2ds1,dus2ds2,dus2dn;
	double dunds1,dunds2,dundn;
	
	double us1s1,us1s2,us1n,us2n,us2s2,unn;
	double s1,s2,n;//the distances between points in the s1,s2 directions. used for the derivatives on the surface which give the unit 				vectors.

	double es1_dot_ds1u,es1_dot_ds2u,es2_dot_ds1u,es2_dot_ds2u,n_dot_ds1u,n_dot_ds2u;//use these to evaluate the strain in either linear or non linear form
	//first convert the values indeformation array from cartesians to s1,s2,n basis.
	double ct,st,cg,sg;
	double dx,dy,dz,normalisation;//used for rotation around s1
	
	int ip1,im1,ip2,im2,jp1,jm1,jc,k;
	double theta,gamma,dt,dtp1,dtm1;
	double dr;
	double xij;
	double yij;
	double rij;
	double ds2, dus2,dun;			
	double drdx;
	double drdy;
	double dthetadx;
	double dthetady;
	double phip1,phim1;
	double duxdx,duxdy,duydx,duydy,duxdz,duydz,duzdy,duzdx,duzdz,duxradial,duyradial,duxdr,duxdtheta,duydtheta,duydr;
	double rim1,rip1;
	
	four_nearest_neighbour_points_i(i,&ip1,&im1,&ip2,&im2,target_points);

	if(dimensions==3)
	{
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	}
	distance_between_points_3d_array_i(pos_array,i,j,target_points,&s1);

	if(dimensions==3)	
	{	
		distance_between_points_3d_array_j(pos_array,i,j,num_zstacks,&s2);
	}	
	

	//printf("finding strain using straintype = %c, dimensions=%d, number of stacks=%d\n",straintype,dimensions,num_zstacks);
	//evaluate the derivatives in the appropriate manner for type of material looking at.
	if(straintype=='h')//shell mode
	{
		//printf("using straintype h dimensions=%d\n",dimensions);
		//Calcuate the vector elements for derivatives at each point i,j in the (s1,n) [2d] or (s1,s2,n) [psuedo 3d or 3d] basis, 
		//calculate vector elements at the points i,j and those surrounding for numerical calculation of derivatives
		rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
       		rotate_vector_3d(&us1_i,&un_i,&us2_i,theta,gamma,i,j,target_points,pos_array,deformation_array,num_zstacks);
		//printf("ux=%f uy=%f us1=%f un=%f\n",deformation_array[i][j][0],deformation_array[i][j][1],us1_i,un_i);
		//at points ip1,j
		rotation_angles_3d(pos_array,ip1,j,&theta,&gamma,target_points,num_zstacks);
		rotate_vector_3d(&us1_ip1,&un_ip1,&us2_ip1,theta,gamma,ip1,j,target_points,pos_array,deformation_array,num_zstacks);

		//at point im1,j
		rotation_angles_3d(pos_array,im1,j,&theta,&gamma,target_points,num_zstacks);
		rotate_vector_3d(&us1_im1,&un_im1,&us2_im1,theta,gamma,im1,j,target_points,pos_array,deformation_array,num_zstacks);
		
		if(dimensions==3 && num_zstacks>1)
		{	//at point i,jp1, if 3d
			rotation_angles_3d(pos_array,i,jp1,&theta,&gamma,target_points,num_zstacks);
			rotate_vector_3d(&us1_jp1,&un_jp1,&us2_jp1,theta,gamma,i,jp1,target_points,pos_array,deformation_array,num_zstacks);

			//at point i,jm1, if 3d
			rotation_angles_3d(pos_array,i,jm1,&theta,&gamma,target_points,num_zstacks);
			rotate_vector_3d(&us1_jm1,&un_jm1,&us2_jm1,theta,gamma,i,jm1,target_points,pos_array,deformation_array,num_zstacks);
		}

		if(dimensions==2 && mode2d=='c')//purely 2d shell mode 
		{	
			dus1ds1=(us1_ip1-us1_im1)/s1;
			dunds1=(un_ip1-un_im1)/s1;
			dus1dn=0;//because of shell mode
			dundn=0;//because of shell mode
		
			//following terms set to 0 because no third dimension (s2) included in this version of the model
			dus1ds2=0;	
			dus2ds1=0;
			dus2ds2=0;
			dus2dn=0;
			dunds2=0;

			us2_i=0;//because 3rd dim nonexistant.

			//evaluate strain components.
			es1_dot_ds1u=dus1ds1+christoffel_array[i][j][0]*us1_i+un_i*curvature_tensor[i][j][0];
			n_dot_ds1u=0.5*dunds1;
			
			//in purely 2d, all these components are zero...
			es1_dot_ds2u=0;
			es2_dot_ds1u=0;
			es2_dot_ds2u=0;
			n_dot_ds2u=0;

			us1s1=es1_dot_ds1u;
			us2s2=es2_dot_ds2u;
			unn=0;
			us1s2=0.5*(es2_dot_ds1u+es1_dot_ds2u);
			us1n=0.5*(n_dot_ds1u);
			us2n=0.5*(n_dot_ds2u);
			

			if(strainmode=='n')//non linear strain mode.
			{
				us1s1+=0.5*(es1_dot_ds1u*es1_dot_ds1u);
				us2s2+=0.5*(es2_dot_ds1u*es2_dot_ds1u);
				unn=0;
				us1s2+=0.5*((es1_dot_ds2u*es1_dot_ds1u)+(es2_dot_ds1u*es2_dot_ds2u));
				us1n+=0;
				us2n+=0;
			}

			//and rotate back to cartesians basis		
			rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
			gamma=0;
			rotate_strain_to_cart(pos_array,strain_array,us1s1,us1s2,us1n,us2s2,us2n,unn,i,j,theta,gamma,target_points,num_zstacks);

			return;
		}
		else if(dimensions==3 && num_zstacks>1)//true 3d shell mode, used when data has z stacks of nuclei.
		{	
			dus1ds1=(us1_ip1-us1_im1)/s1;
			dunds1=(un_ip1-un_im1)/s1;
			dus2ds1=(us2_ip1-us2_im1)/s1;
	
			dus1ds2=(us1_jp1-us1_jm1)/s2;
			dus2ds2=(us2_jp1-us2_jm1)/s2;
			dunds2=(un_jp1-un_jm1)/s2;

			dus1dn=0;//because of shell mode
			dundn=0;//because of shell mode
			dus2dn=0;//because of shell mode

			//printf("dus1ds1=%f dunds1=%f dus2ds1=%f dus1ds2=%f dus2ds2=%f dunds2=%f\n",dus1ds1,dunds1,dus2ds1,dus1ds2,dus2ds2,dunds2);
			//(s1,n,s2) strains, for shell models. 
			//evaluate strain components.
			es1_dot_ds1u=(dus1ds1)+christoffel_array[i][j][0]*us1_i+un_i*curvature_tensor[i][j][0]+us2_i*christoffel_array[i][j][1];
			n_dot_ds1u=dunds1;
			
			es1_dot_ds2u=dus1ds2+us2_i*christoffel_array[i][j][3]+us1_i*christoffel_array[i][j][1]+un_i*curvature_tensor[i][j][2];
			es2_dot_ds1u=dus2ds1+us2_i*christoffel_array[i][j][13]+us1_i*christoffel_array[i][j][12]+un_i*curvature_tensor[i][j][1];
			es2_dot_ds2u=dus2ds2+us1_i*christoffel_array[i][j][13]+us2_i*christoffel_array[i][j][15]+un_i*curvature_tensor[i][j][3];
			n_dot_ds2u=0;

			us1s1=es1_dot_ds1u;
			us2s2=es2_dot_ds1u;
			unn=0;
			us1s2=0.5*(es2_dot_ds1u+es1_dot_ds2u);
			us1n=0.5*(n_dot_ds1u);
			us2n=0.5*(n_dot_ds2u);	

			
			us1n=0.5*dunds1;
			us2n=0.5*dunds2;
			//printf(" %f %f %f %f %f %f\n",es1_dot_ds1u,n_dot_ds1u,es1_dot_ds2u,es2_dot_ds1u,es2_dot_ds2u,n_dot_ds2u);
			//printf("%f %f %f %f %f %f %f\n",dus1ds1,christoffel_array[i][j][0],us1_i,un_i,curvature_tensor[i][j][0],us2_i,christoffel_array[i][j][1]);
			//printf("us1s1=%f us2s2=%f unn=%f us1s2=%f us1n=%f us2n=%f\n",us1s1,us2s2,unn,us1s2,us1n,us2n);

			if(strainmode=='n')//non linear strain mode.
			{
				us1s1+=0.5*(es1_dot_ds1u*es1_dot_ds1u);
				us2s2+=0.5*(es2_dot_ds1u*es2_dot_ds1u);
				unn=0;
				us1s2+=0.5*((es1_dot_ds2u*es1_dot_ds1u)+(es2_dot_ds1u*es2_dot_ds2u));
				us1n+=0;
				us2n+=0;
			}
			//printf("us1s1=%f us2s2=%f unn=%f us1s2=%f us1n=%f us2n=%f\n",us1s1,us2s2,unn,us1s2,us1n,us2n);
			rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
			rotate_strain_to_cart(pos_array,strain_array,us1s1,us1s2,us1n,us2s2,us2n,unn,i,j,theta,gamma,target_points,num_zstacks);

			return;
		}	
		else if(dimensions==3 && num_zstacks==1) //3d shell, 2d data with a assumption about the third dimension
		{
			distance_between_points_3d_array_i(pos_array,i,j,target_points,&s1);
			//the us1, us2 and un derivatives along s1 can still be calculated numerically from the single stack of data;
			dus1ds1=(us1_ip1-us1_im1)/s1;
			dunds1=(un_ip1-un_im1)/s1;

			//normal derivatives of deformations are 0 because of shell mode 			
			dus1dn=0;
			dundn=0;
			dus2dn=0;


			//the remaining terms are defined by the assumption about the out of plane direction.

			//[0]=sss//[1]=s_sphi=s_phis//[3]=^s_phiphi,
			//[12]=^phi_ss//[13]=^phi_sphi=^phi_phis//[14]=^phi_sn=^phi_ns//[15]=^phi_phiphi,//[16]=^phi_nphi=phin,//[17]=^phi_nn 

			//evaluate strain components, use full 3d expression, but with the christoffel and cuverture tensors values adapted for the current pseudo 3d shape (see those functions).
			us1s1=dus1ds1+christoffel_array[i][j][0]*us1_i+un_i*curvature_tensor[i][j][0]+us2_i*christoffel_array[i][j][1];
			us2s2=dus2ds2+us1_i*christoffel_array[i][j][13]+us2_i*christoffel_array[i][j][15]+un_i*curvature_tensor[i][j][3];
			unn=0;
			us1s2=0.5*(dus1ds2+us2_i*christoffel_array[i][j][3]+us1_i*christoffel_array[i][j][1]+un_i*curvature_tensor[i][j][2]+dus2ds1+us2_i*christoffel_array[i][j][13]+us1_i*christoffel_array[i][j][12]+un_i*curvature_tensor[i][j][1]);
			us1n=0.5*dunds1;
			us2n=0.5*dunds2;

			//printf("pos: %f %f strains:%f %f %f %f %f %f\n",pos_array[i][j][0],pos_array[i][j][1],us1s1,us2s2,unn,us1s2,us1n,us2n);
			//printf("us1s1:%f dusds1=%f us1_i=%f csss=%f us2_i=%f csps=%f un_i=%f curv=%f\n",us1s1,dus1ds1,us1_i, christoffel_array[i][j][0],us2_i,christoffel_array[i][j][1],un_i,curvature_tensor[i][j][0]);
			//printf("us1s1=%f us2s2=%f unn=%f us1s2=%f us1n=%f us2n=%f\n",us1s1,us2s2,unn,us1s2,us1n,us2n);
			//and rotate back to cartesians basis		
			rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
			gamma=0;//again from the symmetry, this is required.
			rotate_strain_to_cart(pos_array,strain_array,us1s1,us1s2,us1n,us2s2,us2n,unn,i,j,theta,gamma,target_points,num_zstacks);
	
			return;
		}		
	}
	else if(straintype=='o')//solid mode
	{	//in both solid cases, we presume the solid deformation to be the same for any radial point as that of the boundary, but reduced by the fraction of the distance from the centre.
		if(dimensions==2 && mode2d=='c')//purely 2d solid mode 
		{ 
			xij=pos_array[i][j][0];
			yij=pos_array[i][j][1];
			rij=sqrt(xij*xij+yij*yij);
						
			drdx=xij/rij;
			drdy=yij/rij;
			dthetadx=-yij/(rij*rij);
			dthetady=xij/(rij*rij);
	
			duxradial=deformation_array[i][j][0]/num_innershapes;
			duyradial=deformation_array[i][j][1]/num_innershapes;
			dr=sqrt(pow(pos_array[i][j][0]/num_innershapes,2)+pow(pos_array[i][j][1]/num_innershapes,2));
			duxdr=duxradial/dr;
			duydr=duyradial/dr;

			rim1=sqrt(pow(pos_array[im1][j][0],2)+pow(pos_array[im1][j][1],2));
			rip1=sqrt(pow(pos_array[ip1][j][0],2)+pow(pos_array[ip1][j][1],2));
			
			dtp1=fabs(atan2(pos_array[ip1][j][1],pos_array[ip1][j][0])-atan2(pos_array[i][j][1],pos_array[i][j][0]));
			dt=fabs(atan2(pos_array[i][j][1],pos_array[i][j][0])-atan2(pos_array[im1][j][1],pos_array[im1][j][0]));		
			if(fabs(dt)>M_PI) dt=2*M_PI-dt; 
			if(fabs(dtp1)>M_PI) dtp1=2*M_PI-dtp1;
		
			duxdtheta=(deformation_array[ip1][j][0]*dt*dt*rij/rip1-deformation_array[im1][j][0]*dtp1*dtp1*rij/rim1+deformation_array[i][j][0]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			
			duydtheta=(deformation_array[ip1][j][1]*dt*dt*rij/rip1-deformation_array[im1][j][1]*dtp1*dtp1*rij/rim1+deformation_array[i][j][1]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
	
			duxdx=duxdr*drdx+dthetadx*duxdtheta;
			duxdy=duxdr*drdy+dthetady*duxdtheta;
			duydx=duydr*drdx+dthetadx*duydtheta;
			duydy=duydr*drdy+dthetady*duydtheta;
			duxdz=0.0;
			duydz=0.0;
			duzdy=0.0;
			duzdx=0.0;
			duzdz=0;
			if(strainmode=='l')
			{	strain_array[i][j][0]=duxdx;
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx); 
				strain_array[i][j][2]=duydy;
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy); 
				strain_array[i][j][5]=duzdz;
			}
			
			if(strainmode=='n')
			{	strain_array[i][j][0]=duxdx+1.0/2.0*(duxdx*duxdx+duydx*duydx+duzdx*duzdx);
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx+2*(duxdx*duxdy+duydx*duydy+duzdx*duzdy)); 
				strain_array[i][j][2]=duydy+1.0/2.0*(duydy*duydy+duxdy*duxdy+duzdy*duzdy);
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx+2*(duxdx*duxdz+duydx*duydz+duzdx*duzdz)); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy+2*(duxdy*duxdz+duydy*duydz+duzdy*duzdz)); 
				strain_array[i][j][5]=duzdz+1.0/2.0*(duxdz*duxdz+duxdy*duxdy+duzdy*duzdy);
			}
			
//double duxdx,duxdy,duydx,duydy,duxdz,duydz,duzdy,duzdy,duzdx,duzdz,duxradial,duyradial,duxdr,drdx,dthetadx,duxdtheta,duydr,duydtheta,drdy;
			return;
		}
		else if(dimensions==3 && num_zstacks==1)//true 3d solid mode with radial deformations inside the body around centre of mass of each stack.  use three coordinates, s2, r and theta where r and theta are in the 2d xy plane of each stack.
		{

			
			xij=pos_array[i][j][0];
			yij=pos_array[i][j][1];
			
			rij=sqrt(xij*xij+yij*yij);
							
			drdx=xij/rij;
			drdy=yij/rij;
			dthetadx=-yij/(rij*rij);
			dthetady=xij/(rij*rij);
	
			duxradial=deformation_array[i][j][0]/num_innershapes;
			duyradial=deformation_array[i][j][1]/num_innershapes;
			dr=sqrt(pow(pos_array[i][j][0]/num_innershapes,2)+pow(pos_array[i][j][1]/num_innershapes,2));
			duxdr=duxradial/dr;
			duydr=duyradial/dr;

			rim1=sqrt(pow(pos_array[im1][j][0],2)+pow(pos_array[im1][j][1],2));
			rip1=sqrt(pow(pos_array[ip1][j][0],2)+pow(pos_array[ip1][j][1],2));
			
			dtp1=fabs(atan2(pos_array[ip1][j][1],pos_array[ip1][j][0])-atan2(pos_array[i][j][1],pos_array[i][j][0]));
			dt=fabs(atan2(pos_array[i][j][1],pos_array[i][j][0])-atan2(pos_array[im1][j][1],pos_array[im1][j][0]));	
			if(fabs(dt)>M_PI) dt=2*M_PI-dt; 
			if(fabs(dtp1)>M_PI) dtp1=2*M_PI-dtp1;
		
			duxdtheta=(deformation_array[ip1][j][0]*dt*dt*rij/rip1-deformation_array[im1][j][0]*dtp1*dtp1*rij/rim1+deformation_array[i][j][0]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			
			duydtheta=(deformation_array[ip1][j][1]*dt*dt*rij/rip1-deformation_array[im1][j][1]*dtp1*dtp1*rij/rim1+deformation_array[i][j][1]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
	
			duxdx=duxdr*drdx+dthetadx*duxdtheta;
			duxdy=duxdr*drdy+dthetady*duxdtheta;
			duydx=duydr*drdx+dthetadx*duydtheta;
			duydy=duydr*drdy+dthetady*duydtheta;
			duxdz=0.0;
			duydz=0.0;
			duzdy=0.0;
			duzdx=0.0;
			duzdz=0.0;
			if(strainmode=='l')
			{	strain_array[i][j][0]=duxdx;
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx); 
				strain_array[i][j][2]=duydy;
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy); 
				strain_array[i][j][5]=duzdz;
			}
			
			if(strainmode=='n')
			{	strain_array[i][j][0]=duxdx+1.0/2.0*(duxdx*duxdx+duydx*duydx+duzdx*duzdx);
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx+2*(duxdx*duxdy+duydx*duydy+duzdx*duzdy)); 
				strain_array[i][j][2]=duydy+1.0/2.0*(duydy*duydy+duxdy*duxdy+duzdy*duzdy);
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx+2*(duxdx*duxdz+duydx*duydz+duzdx*duzdz)); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy+2*(duxdy*duxdz+duydy*duydz+duzdy*duzdz)); 
				strain_array[i][j][5]=duzdz+1.0/2.0*(duxdz*duxdz+duxdy*duxdy+duzdy*duzdy);
			}
			
			if(volume_mode=='s') 
			{
				//symmetric deformation, defined by having the same strain in the along channel (x) and out of plane direction (z)
				strain_array[i][j][5]=strain_array[i][j][0];
				if(incompressible=='y' || fabs(sigma-0.5)<0.01) 
				{	printf("warning, incompressible when supposed to be biaxial strain\n");
					strain_array[i][j][2]=-strain_array[i][j][0]-strain_array[i][j][5];
				}
			}
			if(volume_mode=='f') 
			{
				//filled channel deformation, defined by having the same strain in the along channel (x) and out of plane direction (z)
				//if in the channel, strain is non-zero, if out of the constriction, strain is zero.
				strain_array[i][j][5]=Lz-Lzcon;
				if(incompressible=='y' || fabs(sigma-0.5)<0.01) {strain_array[i][j][5]=-strain_array[i][j][0]-strain_array[i][j][2];}
			}
			return;
		}	
		else if(dimensions==3 && num_zstacks>1)//true 3d solid mode with radial deformations inside the body around centre of mass of each stack.  use three coordinates, s2, r and theta where r and theta are in the 2d xy plane of each stack.
		{
			
			double duxds2,duyds2,duzds2;
			double ds2dx,ds2dy,ds2dz;
			double drdz=0;//0 because radial direction and z are perpendicular, but included for completeness
			double dthetadz=0;//0 because xy angle plane direction and z are perpendicular, but included for completeness
			double duzdr=0;//assuming that entire plane has the same z deformation
			double duzdtheta=0;//assumes entire plane has same z deformation
			double s2jp1=0,s2j=0,s2jm1=0;

			for(k=1;k<=jm1;k++)
			{
				s2jm1+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			for(k=1;k<=j;k++)
			{
				s2j+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			for(k=1;k<=jp1;k++)
			{
				s2jp1+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			xij=pos_array[i][j][0];
			yij=pos_array[i][j][1];
			
			rij=sqrt(xij*xij+yij*yij);
							
			drdx=xij/rij;
			drdy=yij/rij;
			dthetadx=-yij/(rij*rij);
			dthetady=xij/(rij*rij);
	
			duxradial=deformation_array[i][j][0]/num_innershapes;
			duyradial=deformation_array[i][j][1]/num_innershapes;
			dr=sqrt(pow(pos_array[i][j][0]/num_innershapes,2)+pow(pos_array[i][j][1]/num_innershapes,2));
			duxdr=duxradial/dr;
			duydr=duyradial/dr;

			rim1=sqrt(pow(pos_array[im1][j][0],2)+pow(pos_array[im1][j][1],2));
			rip1=sqrt(pow(pos_array[ip1][j][0],2)+pow(pos_array[ip1][j][1],2));
			
			dtp1=fabs(atan2(pos_array[ip1][j][1],pos_array[ip1][j][0])-atan2(pos_array[i][j][1],pos_array[i][j][0]));
			dt=  fabs(atan2(pos_array[i][j][1],pos_array[i][j][0])-atan2(pos_array[im1][j][1],pos_array[im1][j][0]));	
			if(fabs(dt)>M_PI) dt=2*M_PI-dt; 
			if(fabs(dtp1)>M_PI) dtp1=2*M_PI-dtp1;
		
			duxdtheta=(deformation_array[ip1][j][0]*dt*dt*rij/rip1-deformation_array[im1][j][0]*dtp1*dtp1*rij/rim1+deformation_array[i][j][0]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			
			duydtheta=(deformation_array[ip1][j][1]*dt*dt*rij/rip1-deformation_array[im1][j][1]*dtp1*dtp1*rij/rim1+deformation_array[i][j][1]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			//s2 is at an angle gamma from the z axis 
			
			//if(j==0){printf("jp1=%d jm1=%d s2jp1=%f s2 jm1=%f\n",jp1,jm1,s2jp1,s2jm1);}

			duxds2=(deformation_array[i][jp1][0]-deformation_array[i][jm1][0])/(s2jp1-s2jm1);
			duyds2=(deformation_array[i][jp1][1]-deformation_array[i][jm1][1])/(s2jp1-s2jm1);
			duzds2=(deformation_array[i][jp1][2]-deformation_array[i][jm1][2])/(s2jp1-s2jm1);
			ds2dx=s2j/num_innershapes*drdx;//dsdr * drdx
			ds2dy=s2j/num_innershapes*drdy;//dsdr * drdy, assume s2 drops linearly to proprtional to z at r=0.

			double gammajp1,gammajm1;
			rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
			//because of the way rotation angles calculates gamma, actually want gamma-M_PI/2.0;
			//gamma=gamma-M_PI/2.0;
			rotation_angles_3d(pos_array,i,jp1,&theta,&gammajp1,target_points,num_zstacks);
			//gammajp1=gammajp1-M_PI/2.0;;
			rotation_angles_3d(pos_array,i,jm1,&theta,&gammajm1,target_points,num_zstacks);
			//gammajm1=gammajm1-M_PI/2.0;;
			
			ds2dz=1/cos(gamma);//*(1+(pos_array[i][jp1][2]-pos_array[i][jm1][2])*(gammajp1-gammajm1)*tan(gamma));//z=s2 cos(gamma) -> ds2dz=1/cos(gamma) *(1+change in z * dgamma/dz tan(gamma) 
		
			
			//printf("gamma=%f duxds2=%f duyds2=%f duzds2=%f ds2dz=%f\n",gamma,duxds2,duyds2,duzds2,ds2dz);
			duxdx=duxdr*drdx+dthetadx*duxdtheta+duxds2*ds2dx;
			duxdy=duxdr*drdy+dthetady*duxdtheta+duxds2*ds2dy;
			duydx=duydr*drdx+dthetadx*duydtheta+duyds2*ds2dx;
			duydy=duydr*drdy+dthetady*duydtheta+duyds2*ds2dy;

			duxdz=duxdr*drdz+dthetadz*duxdtheta+duxds2*ds2dz;
			duydz=duydr*drdz+dthetadz*duydtheta+duyds2*ds2dz;
			duzdy=duzdr*drdy+dthetady*duzdtheta+duzds2*ds2dy;
			duzdx=duzdr*drdx+dthetadx*duzdtheta+duzds2*ds2dx;
			duzdz=duzdr*drdz+dthetadz*duzdtheta+duzds2*ds2dz;
		/*if(i==0 && j<3)
		{
			printf("i=%d j=%d duzdr=%f drdz=%f dthetadz=%f duzdtheta=%f duzds2=%f ds2dz=%f\n",i,j,duzdr,drdz,dthetadz,duzdtheta,duzds2,ds2dz);
			printf("ux=%f duxdr=%f drdx=%f dthetadx=%f dthetady=%f duxdtheta=%f\n ",deformation_array[i][j][0],duxdr,drdx,dthetadx,dthetady,duxdtheta);

		}*/
			if(strainmode=='l')
			{	strain_array[i][j][0]=duxdx;
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx); 
				strain_array[i][j][2]=duydy;
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy); 
				strain_array[i][j][5]=duzdz;
			}
			
			if(strainmode=='n')
			{	strain_array[i][j][0]=duxdx+1.0/2.0*(duxdx*duxdx+duydx*duydx+duzdx*duzdx);
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx+2*(duxdx*duxdy+duydx*duydy+duzdx*duzdy)); 
				strain_array[i][j][2]=duydy+1.0/2.0*(duydy*duydy+duxdy*duxdy+duzdy*duzdy);
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx+2*(duxdx*duxdz+duydx*duydz+duzdx*duzdz)); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy+2*(duxdy*duxdz+duydy*duydz+duzdy*duzdz)); 
				strain_array[i][j][5]=duzdz+1.0/2.0*(duxdz*duxdz+duxdy*duxdy+duzdy*duzdy);
			}
			return;
		}
	}
/*
			
			double duxds2,duyds2,duzds2;
			double ds2dx,ds2dy,ds2dz;
			double drdz=0;//0 because radial direction and z are perpendicular, but included for completeness
			double dthetadz=0;//0 because xy angle plane direction and z are perpendicular, but included for completeness
			double duzdr=0;//assuming that entire plane has the same z deformation
			double duzdtheta=0;//assumes entire plane has same z deformation
			double s2jp1=0,s2j=0,s2jm1=0;

			for(k=1;k<jm1;k++)
			{
				s2jm1+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			for(k=1;k<j;k++)
			{
				s2j+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			for(k=1;k<jp1;k++)
			{
				s2jp1+=sqrt(pow(pos_array[i][k][0]-pos_array[i][k-1][0],2)+pow(pos_array[i][k][1]-pos_array[i][k-1][1],2)+pow(pos_array[i][k][2]-pos_array[i][k-1][2],2));
			}
			xij=pos_array[i][j][0];
			yij=pos_array[i][j][1];
			
			rij=sqrt(xij*xij+yij*yij);
							
			drdx=xij/rij;
			drdy=yij/rij;
			dthetadx=-yij/(rij*rij);
			dthetady=xij/(rij*rij);
	
			duxradial=deformation_array[i][j][0]/num_innershapes;
			duyradial=deformation_array[i][j][1]/num_innershapes;
			dr=sqrt(pow(pos_array[i][j][0]/num_innershapes,2)+pow(pos_array[i][j][1]/num_innershapes,2));
			duxdr=duxradial/dr;
			duydr=duyradial/dr;

			rim1=sqrt(pow(pos_array[im1][j][0],2)+pow(pos_array[im1][j][1],2));
			rip1=sqrt(pow(pos_array[ip1][j][0],2)+pow(pos_array[ip1][j][1],2));
			
			dtp1=fabs(atan2(pos_array[ip1][j][1],pos_array[ip1][j][0])-atan2(pos_array[i][j][1],pos_array[i][j][0]));
			dt=fabs(atan2(pos_array[i][j][1],pos_array[i][j][0])-atan2(pos_array[im1][j][1],pos_array[im1][j][0]));	
			if(fabs(dt)>M_PI) dt=2*M_PI-dt; 
			if(fabs(dtp1)>M_PI) dtp1=2*M_PI-dtp1;
		
			duxdtheta=(deformation_array[ip1][j][0]*dt*dt*rij/rip1-deformation_array[im1][j][0]*dtp1*dtp1*rij/rim1+deformation_array[i][j][0]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			
			duydtheta=(deformation_array[ip1][j][1]*dt*dt*rij/rip1-deformation_array[im1][j][1]*dtp1*dtp1*rij/rim1+deformation_array[i][j][1]*(dtp1*dtp1-dt*dt))/(dt*dtp1*(dt+dtp1));
			//s2 is at an angle gamma from the z axis 
			
			
			duxds2=(deformation_array[i][jp1][0]-deformation_array[i][jm1][0])/(s2jp1-s2jm1);
			duyds2=(deformation_array[i][jp1][1]-deformation_array[i][jm1][1])/(s2jp1-s2jm1);
			duzds2=(deformation_array[i][jp1][2]-deformation_array[i][jm1][2])/(s2jp1-s2jm1);
			ds2dx=s2j/num_innershapes*drdx;//dsdr * drdx
			ds2dy=s2j/num_innershapes*drdy;//dsdr * drdy, assume s2 drops linearly to proprtional to z at r=0.

			double gammajp1,gammajm1;
			rotation_angles_3d(pos_array,i,j,&theta,&gamma,target_points,num_zstacks);
			//because of hte way rotation angles calculates gamma, actually want M_PI/2.0-gamma;
			gamma=M_PI/2.0-gamma;
			rotation_angles_3d(pos_array,i,jp1,&theta,&gammajp1,target_points,num_zstacks);
			gammajp1=M_PI/2.0-gammajp1;
			rotation_angles_3d(pos_array,i,jm1,&theta,&gammajm1,target_points,num_zstacks);
			gammajm1=M_PI/2.0-gammajm1;
			ds2dz=1/cos(gamma);//*(1+(pos_array[i][jp1][2]-pos_array[i][jm1][2])*(gammajp1-gammajm1)*tan(gamma));//z=s2 cos(gamma) -> ds2dz=1/cos(gamma) *(1+change in z * dgamma/dz tan(gamma) 

			//printf("gamma=%f duxds2=%f duyds2=%f duzds2=%f ds2dz=%f\n",gamma,duxds2,duyds2,duzds2,ds2dz);
			duxdx=duxdr*drdx+dthetadx*duxdtheta+duxds2*ds2dx;
			duxdy=duxdr*drdy+dthetady*duxdtheta+duxds2*ds2dy;
			duydx=duydr*drdx+dthetadx*duydtheta+duyds2*ds2dx;
			duydy=duydr*drdy+dthetady*duydtheta+duyds2*ds2dy;

			duxdz=duxdr*drdz+dthetadz*duxdtheta+duxds2*ds2dz;
			duydz=duydr*drdz+dthetadz*duydtheta+duyds2*ds2dz;
			duzdy=duzdr*drdy+dthetady*duzdtheta+duzds2*ds2dy;
			duzdx=duzdr*drdx+dthetadx*duzdtheta+duzds2*ds2dx;
			duzdz=duzdr*drdz+dthetadz*duzdtheta+duzds2*ds2dz;

			if(strainmode=='l')
			{	strain_array[i][j][0]=duxdx;
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx); 
				strain_array[i][j][2]=duydy;
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy); 
				strain_array[i][j][5]=duzdz;
			}
			
			if(strainmode=='n')
			{	strain_array[i][j][0]=duxdx+1.0/2.0*(duxdx*duxdx+duydx*duydx+duzdx*duzdx);
				strain_array[i][j][1]=1.0/2.0*(duxdy+duydx+2*(duxdx*duxdy+duydx*duydy+duzdx*duzdy)); 
				strain_array[i][j][2]=duydy+1.0/2.0*(duydy*duydy+duxdy*duxdy+duzdy*duzdy);
				strain_array[i][j][3]=1.0/2.0*(duxdz+duzdx+2*(duxdx*duxdz+duydx*duydz+duzdx*duzdz)); 
				strain_array[i][j][4]=1.0/2.0*(duydz+duzdy+2*(duxdy*duxdz+duydy*duydz+duzdy*duzdz)); 
				strain_array[i][j][5]=duzdz+1.0/2.0*(duxdz*duxdz+duxdy*duxdy+duzdy*duzdy);
			}
			return;
		}	
	}
*/	
	else{
		printf("warning: strain mode undefined or incorrect options input, straintype=%c, mode2d=%c dimensions=%d\n",straintype,mode2d,dimensions);
		return;
	}

	
return;
} 

void array_alloc_1d_int(int **data,int nrows)
{
	*data = malloc(sizeof(int)*nrows);
	int i;
return;
}

void array_alloc_2d_dbl(double ***data, int nrows, int ncols)
{
	int i,j;

	*data = malloc( sizeof(double *) * nrows);
	for (i=0;i<nrows;i++)
	{
  	(*data)[i] = malloc(sizeof(double)*ncols);
        }
	

    return;
}

void array_alloc_3d_dbl(double ****data, int nrows, int ncols,int ndepth)
{
	int i,j,k;

	*data = malloc(sizeof(double**)*nrows);

	for (i=0;i<nrows;i++)
	{
	  	(*data)[i] = malloc(sizeof(double*)*ncols);
        }

	for(i=0;i<nrows;i++)
	{	for(j=0;j<ncols;j++)
		{	(*data)[i][j]=malloc(sizeof(double)*ndepth);
		}
	}


	return;
}

void free_array_1d_dbl(double **data)
{
	free(*data);
	return;
}

void free_array_1d_int(int **data)
{
	free(*data);
	return;
}

void free_array_2d_dbl(double ***data,int nrows)
{
	int i;
	for (i=0;i<nrows;i++)free((*data)[i]); 
	free(*data);
	return;
}

void free_array_3d_dbl(double ****data, int nrows, int ncols,int ndepth)
{
	int i,j,k;

	for(i=0;i<nrows;i++)
	{	for(j=0;j<ncols;j++)
		{	free((*data)[i][j]);
		}
	}

	for (i=0;i<nrows;i++)
	{
	  	free((*data)[i]);
        }
	free(*data);
	return;
}



double free_energy_calc_3d(double*** strain_array,double*** stress_array,int i,int j)
{	//calculates the free energy density at a point [i][j]
	double freeenergy=0.5*(strain_array[i][j][0]*stress_array[i][j][0]+strain_array[i][j][2]*stress_array[i][j][2]+2*strain_array[i][j][1]*stress_array[i][j][1]+strain_array[i][j][5]*stress_array[i][j][5]+2*strain_array[i][j][3]*strain_array[i][j][3]+2*strain_array[i][j][4]*strain_array[i][j][4]);

return 0.5*freeenergy;
}


void mesh_xy(double*** source_array,double ***target_array,int* sourcerows,int* targetrows,int* source_stacks,int* target_stacks,int refstack)
{
//input array is read in as a list of data points(x,y,z) sorted by angle around the centre of mass. The data in input array looks like for point i, [i][0]=0.5,[i][1]=0.5,[i][2]= 1.0 as an example of point i with [0]=x coordinate [1]=y , [2]=z
	int i,im1,ip1;
	int k,km1,kp1;
	int check=0;
	int backup_k;
	int points_assigned;
	double theta,thetap1,a,xi,yi,xip1,yip1,xim1,yim1;
	double zmintarget,zminsource;
	//pointcount is some array containing the number of points at each zstack.
	//the value of refstack passed to the function is the stack to use as a reference.
	
	//first redraw both the source and target mesh to have the same number of xy points at each height, given by number in pointcount[refstack]. Don't worry about matching to angles yet, just add/delete points depending on the ratio of the number of points at height j compared to the number in pointcount
	
	int running_tot=0;
	int j;

	//find largest stack on target or source.
	int largeststack=0;
	for(j=0;j<*target_stacks;j++)
	{
		if(*targetrows>largeststack) largeststack=*targetrows;
	}
	for(j=0;j<*source_stacks;j++)
	{
		if(*sourcerows>largeststack) largeststack=*sourcerows;
	}

	double** dummy_array;
	array_alloc_2d_dbl(&dummy_array,largeststack,3);
	
	//sort both target and source by smallest angle to biggest, while maintaining order of points from the data file.

	orientate_curve_after_remeshing(target_array,*target_stacks,*targetrows);
	orientate_curve_after_remeshing(source_array,*source_stacks,*sourcerows);

	//first spread points on stack j evenly along the s1 (constant second index), using the ratio of the points at that height.
	int floor_ratio,floor_ratio_p1;	
	double target_ratio,source_ratio;
	//printf("*sourcerows=%d\n", *sourcerows);
	source_ratio=(double)*targetrows/(double)*sourcerows;
	target_ratio=1;//(double)*targetrows/(double)*targetrows[j];

	double frac_ratio,gradientx,gradienty,finalposx,finalposy;

	zmintarget=target_array[0][0][2];
	zminsource=source_array[0][0][2];

	//first rescale the points to actual measured sizes rather than pixel sizes on both source and target arrays, and reduce each zstack on both source and target shape to have the same number of points at same angles

	printf("remeshing target\n");
	for(j=0;j<*target_stacks;j++)
	{	
		target_ratio=1;//(double)target_pointcount[refstack]/(double)target_pointcount[j];
		if(j==refstack)
		{
			for(i=0;i<*targetrows;i++)
			{
			dummy_array[i][0]=target_array[i][j][0];
			dummy_array[i][1]=target_array[i][j][1]; //leave initial point unchanged, only converts to same size 
			dummy_array[i][2]=target_array[i][j][2];
				
			}
		}

		else for(i=0;i<*targetrows;i++)
			{//else loop over the number of points in that stack, and increase/reduce the number of points to match the ref stack.						
			if(i==0){
				 dummy_array[i][0]=target_array[0][j][0];
				 dummy_array[i][1]=target_array[0][j][1];
				 dummy_array[i][2]=target_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points 				
					floor_ratio=(int)(floor(i/target_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(*targetrows-1)) floor_ratio_p1=0;
			
					frac_ratio=(i)/target_ratio-floor_ratio;
					gradientx=(target_array[floor_ratio_p1][j][0]-target_array[floor_ratio][j][0]);
					gradienty=(target_array[floor_ratio_p1][j][1]-target_array[floor_ratio][j][1]);
					dummy_array[i][0]=target_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=target_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=target_array[floor_ratio][j][2];
					
				}
			}
		for(i=0;i<*targetrows;i++)
		{	
		//convert to same size scale in both xy plane and z directions
			target_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
			target_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
			target_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
	}

	printf("remeshed target, remeshing source\n");

	for(j=0;j<*source_stacks;j++)
	{

		source_ratio=(double)*targetrows/(double)*sourcerows;
		//and do the same for the source shape!
		for(i=0;i<*targetrows;i++)
		{
			if(i==0){dummy_array[i][0]=source_array[i][j][0];
				dummy_array[i][1]=source_array[i][j][1];
				dummy_array[i][2]=source_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points		
					floor_ratio=(int)(floor(i/source_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(*sourcerows-1)) {floor_ratio_p1=0;}
					frac_ratio=(i)/source_ratio-floor_ratio;
					gradientx=(source_array[floor_ratio_p1][j][0]-source_array[floor_ratio][j][0]);
					gradienty=(source_array[floor_ratio_p1][j][1]-source_array[floor_ratio][j][1]);

					dummy_array[i][0]=source_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=source_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=source_array[floor_ratio][j][2];

				}
			
		}
	
		for(i=0;i<*targetrows;i++)
		{
	
			source_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
			source_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
			source_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
		*sourcerows=*targetrows;
	}
	printf("remeshed source\n");


//now rerun angle orientation just in case the remeshing has changed which point is at largest negative angle.
	orientate_curve_after_remeshing(target_array,*target_stacks,*targetrows);
	orientate_curve_after_remeshing(source_array,*source_stacks,*sourcerows);
	
	return;
}

void cent_masscalc3d(double*** shape_array,int* pointcount_array,int num_stacks)
{   //calculates CoM for the list of points given.
	double sumx=0,sumy=0;
	double comx=0.0,comy=0.0;
	int i=0,j=0,k=0,framenum=0;
	double area=0.0,totarea=0.0,ai=0.0;
	double xtmp = 0, ytmp = 0;
   	double z;
	
	double areasum=0;
	double areacomx=0,areacomy=0;

	comx=0.0; comy=0.0; area=0.0; xtmp=0.0; ytmp=0.0; ai=0.0;	
	for(k=0;k<num_stacks;k++)
        {              	
	   	for (i = 0;i<pointcount_array[k]; i++)
		{
			if(i==(pointcount_array[k]-1)) j=0;
			else j=i+1;
		  	ai=shape_array[i][k][0] *shape_array[j][k][1] - shape_array[j][k][0] * shape_array[i][k][1];
	                area+= ai;
	            	xtmp += (shape_array[j][k][0] + shape_array[i][k][0]) * ai;
		  	ytmp += (shape_array[j][k][1] + shape_array[i][k][1]) * ai;
	  	}

		//align shapes by the centre of mass of the average com of all stacks
		comx+=xtmp / (3.0*area);
		comy+=ytmp / (3.0*area);
		
		areasum+=area;
		areacomx+=comx*area; areacomy+=comy*area;
	}   
		comx/=num_stacks;
		comy/=num_stacks;
	//shift all points to be around centre of mass.
	for(k=0;k<num_stacks;k++)
	{  
		for(i=0;i<pointcount_array[k];i++)
		{
			shape_array[i][k][0]=shape_array[i][k][0]-comx;
			shape_array[i][k][1]=shape_array[i][k][1]-comy;
		}
	}
     
	printf("com calculated, points recentered \n");
	return;
}

double free_energy_calc(double** strain_array,double** stress_array,int ndisplace)
{
	int i;
	double freeenergy=0;
	for(i=0;i<ndisplace;i++) {freeenergy=freeenergy+strain_array[i][0]*stress_array[i][0]+strain_array[i][2]*stress_array[i][2]+2*strain_array[i][1]*stress_array[i][1]+strain_array[i][5]*stress_array[i][5]+2*strain_array[i][3]*strain_array[i][3]+2*strain_array[i][4]*strain_array[i][4];}

	return 0.5*freeenergy;
}

void array_alloc_1d_dbl(double **data,int nrows)
{
	*data = malloc(sizeof(double)*nrows);
	return;
}

void open_input_file(FILE** input1,char* inputname)
{	
	while((*input1=fopen(inputname,"r"))==NULL)
        {
		printf("Enter name of the input txt file (don't include extensions!)\n");
		scanf("%s",inputname);
		strcat(inputname,".txt");
	} 
	return;
}

void rotate_strain(double** strain_array,double uss,double usn, double usp,double unn,double unp,double upp, double theta,int i,double dxds,double dyds,double r,double y)
{
	//rotates strain array from elements uss,usz,uzz back to the xyz system by an anticlockwise rotation about the xy plane by angle theta
	//if(dx<0)//if dx<0 then the rotation is clockwise theta->-theta
	double phi;
	if(straintype=='h')//to rotate the strain back to cartesian basis from the (s,n,phi) basis as defined in guilliames paper.
	{
	//if y>0, phi =0 as on the "top" part of the rotation, phi=0. else if y<0, on the bottom and phi =+-Pi
	if(y>0) phi=0; else phi=M_PI;
		strain_array[i][0]=pow(sin(theta), 0.2e1) * uss + 0.2e1 * sin(theta) * cos(theta) * usn + pow(cos(theta), 0.2e1) * unn;

		strain_array[i][1]=  cos(theta) * cos(phi) * sin(theta) * uss + pow(cos(theta), 0.2e1) * cos(phi) * usn - pow(sin(theta), 0.2e1) * cos(phi) * usn - sin(theta) * cos(phi) * cos(theta) * unn - 0.1e1 / r * sin(phi) * sin(theta) * usp - 0.1e1 / r * sin(phi) * cos(theta) * unp;

		strain_array[i][2]=  pow(cos(theta), 0.2e1) * pow(cos(phi), 0.2e1) * uss - 0.2e1 * cos(theta) * pow(cos(phi), 0.2e1) * sin(theta) * usn - cos(theta) * cos(phi) * r * sin(phi) * usp + pow(sin(theta), 0.2e1) * pow(cos(phi), 0.2e1) * unn + sin(theta) * cos(phi) * r * sin(phi) * unp - 0.1e1 / r * sin(phi) * cos(theta) * cos(phi) * usp + 0.1e1 / r * sin(phi) * sin(theta) * cos(phi) * unp + pow(sin(phi), 0.2e1) * upp;

		strain_array[i][3]= cos(theta) * sin(phi) * sin(theta) * uss + pow(cos(theta), 0.2e1) * sin(phi) * usn - pow(sin(theta), 0.2e1) * sin(phi) * usn - sin(theta) * sin(phi) * cos(theta) * unn + 0.1e1 / r * cos(phi) * sin(theta) * usp + 0.1e1 / r * cos(phi) * cos(theta) * unp;

		strain_array[i][4]=pow(cos(theta), 0.2e1) * sin(phi) * cos(phi) * uss - 0.2e1 * cos(theta) * sin(phi) * sin(theta) * cos(phi) * usn - cos(theta) * pow(sin(phi), 0.2e1) * r * usp + pow(sin(theta), 0.2e1) * sin(phi) * cos(phi) * unn + sin(theta) * pow(sin(phi), 0.2e1) * r * unp + 0.1e1 / r * pow(cos(phi), 0.2e1) * cos(theta) * usp - 0.1e1 / r * pow(cos(phi), 0.2e1) * sin(theta) * unp - cos(phi) * sin(phi) * upp;


	}
	else if(straintype=='c'||straintype=='t')//curved shell
	{	phi=0;//again this is already substituted in to below equations.
		
		strain_array[i][0]=pow(dxds, 0.2e1) * uss / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) + 0.2e1 * dxds * dyds * usn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) + pow(dyds, 0.2e1) * unn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1));
		strain_array[i][1]=-pow(dxds, 0.2e1) * usn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) - dxds * dyds * unn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) + dxds * dyds * uss / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) + pow(dyds, 0.2e1) * usn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1));
		
		strain_array[i][2]= pow(dxds, 0.2e1) * unn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) - 0.2e1 * dxds * dyds * usn / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1)) + pow(dyds, 0.2e1) * uss / (pow(dxds, 0.2e1) + pow(dyds, 0.2e1));
		
		strain_array[i][3]=dxds * usp * pow(pow(dxds, 0.2e1) + pow(dyds, 0.2e1), -0.1e1 / 0.2e1) + dyds * unp * pow(pow(dxds, 0.2e1) + pow(dyds, 0.2e1), -0.1e1 / 0.2e1);
		strain_array[i][4]=dyds * usp * pow(pow(dxds, 0.2e1) + pow(dyds, 0.2e1), -0.1e1 / 0.2e1) - dxds * unp * pow(pow(dxds, 0.2e1) + pow(dyds, 0.2e1), -0.1e1 / 0.2e1);
		strain_array[i][5]=upp;

		//strain_array[i][0]=(dxds * Us(s, phi) * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1) + dyds * Un(s, phi) * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1)) * dxds * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1) + (dyds * Us(s, phi) * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1) - dxds * Un(s, phi) * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1)) * dyds * pow(dxds * dxds + dyds * dyds, -0.1e1 / 0.2e1);

		

	}
	else{
	strain_array[i][0]=uss*sin(theta)*sin(theta);
	strain_array[i][1]=uss*sin(theta)*cos(theta);
	strain_array[i][2]=uss*cos(theta)*cos(theta);
	strain_array[i][3]=r*usp*sin(theta);
	strain_array[i][4]=r*usp*cos(theta);
	strain_array[i][5]=r*upp;
	}	

	return;
}

void mesh_3d_xy(double*** source_array,double ***target_array,int* source_pointcount,int* target_pointcount,int source_stacks,int target_stacks,int refstack)
{//this function fills pos_array with the input data from input_array.
//input array is read in as a list of data points(x,y,z) sorted by angle around the centre of mass. The data in input array looks like for point i, [i][0]=0.5,[i][1]=0.5,[i][2]= 1.0 as an example of point i with [0]=x coordinate [1]=y , [2]=z
	int i,im1,ip1;
	int k,km1,kp1;
	int check=0;
	int backup_k;
	int points_assigned;
	double theta,thetap1,a,xi,yi,xip1,yip1,xim1,yim1;
	double zmintarget,zminsource;
	//pointcount is some array containing the number of points at each zstack.
	//the value of refstack passed to the function is the stack to use as a reference.
	
	//first redraw both the source and target mesh to have the same number of xy points at each height, given in pointcount[refstack]. Don't worry about matching to angles yet, just add/delete points depending on the ratio of the number of points at height j compared to the number in pointcount
	
	int running_tot=0;
	int j;
	//want to scale the number of points down from number at height j to match number at height refstack

	//rather crude approach as in 2d: bruteforce through the stack and just evenly space them along s1 initially, than afterwards remesh along s1 to get mesh to have symmetry in s2
	
	//find largest stack on target or source.
	int largeststack=0;
	for(j=0;j<target_stacks;j++)
	{
		if(target_pointcount[j]>largeststack) largeststack=target_pointcount[j];
	}
	for(j=0;j<source_stacks;j++)
	{
		if(source_pointcount[j]>largeststack) largeststack=source_pointcount[j];
	}
	double** dummy_array;
	array_alloc_2d_dbl(&dummy_array,largeststack,3);
	
	
	//sort both target and source by smallest angle to biggest, while maintaining order of points from the data file.
	orientate_curve_before_remeshing(target_array,target_stacks,target_pointcount);
	orientate_curve_before_remeshing(source_array,source_stacks,source_pointcount);

	//first spread points on stack j evenly along the s1 (constant second index), using the ratio of the points at that height.
	int floor_ratio,floor_ratio_p1;	
	double target_ratio,source_ratio;
	//printf("source_pointcount[j]=%d\n", source_pointcount[j]);
	source_ratio=(double)target_pointcount[refstack]/(double)source_pointcount[j];
	target_ratio=(double)target_pointcount[refstack]/(double)target_pointcount[j];

	double frac_ratio,gradientx,gradienty,finalposx,finalposy;

	zmintarget=target_array[0][0][2];
	zminsource=source_array[0][0][2];

	//first rescale the points to actual measured sizes rather than pixel sizes on both source and target arrays, and reduce each zstack on both source and target shape to have the same number of points at same angles

	for(j=0;j<target_stacks;j++)
	{	
		target_ratio=(double)target_pointcount[refstack]/(double)target_pointcount[j];
		if(j==refstack)
		{
			for(i=0;i<target_pointcount[refstack];i++)
			{
			dummy_array[i][0]=target_array[i][j][0];
			dummy_array[i][1]=target_array[i][j][1]; //leave initial point unchanged, only converts to same size 
			dummy_array[i][2]=target_array[i][j][2];
				
			}
		}

		else for(i=0;i<target_pointcount[refstack];i++)
			{//else loop over the number of points in that stack, and increase/reduce the number of points to match the ref stack.						
			if(i==0){
				 dummy_array[i][0]=target_array[0][j][0];
				 dummy_array[i][1]=target_array[0][j][1];
				 dummy_array[i][2]=target_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points 				
					floor_ratio=(int)(floor(i/target_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(target_pointcount[j]-1)) floor_ratio_p1=0;
			
					frac_ratio=(i)/target_ratio-floor_ratio;
					gradientx=(target_array[floor_ratio_p1][j][0]-target_array[floor_ratio][j][0]);
					gradienty=(target_array[floor_ratio_p1][j][1]-target_array[floor_ratio][j][1]);
					dummy_array[i][0]=target_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=target_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=target_array[floor_ratio][j][2];
					
				}
			}


	for(i=0;i<target_pointcount[refstack];i++)
		{	
	//convert to same size scale in both xy plane and z directions
		target_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
		target_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
		target_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
	
	target_pointcount[j]=target_pointcount[refstack];
	}

	//printf("remeshed target, remeshing source\n");

	for(j=0;j<source_stacks;j++)
	{

	source_ratio=(double)target_pointcount[refstack]/(double)source_pointcount[j];
	//and do the same for the source shape!
	for(i=0;i<target_pointcount[refstack];i++)
		{
			if(i==0){dummy_array[i][0]=source_array[i][j][0];
				dummy_array[i][1]=source_array[i][j][1];
				dummy_array[i][2]=source_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points
					
					floor_ratio=(int)(floor(i/source_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(source_pointcount[j]-1)) {floor_ratio_p1=0;}
					frac_ratio=(i)/source_ratio-floor_ratio;
					gradientx=(source_array[floor_ratio_p1][j][0]-source_array[floor_ratio][j][0]);
					gradienty=(source_array[floor_ratio_p1][j][1]-source_array[floor_ratio][j][1]);

					dummy_array[i][0]=source_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=source_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=source_array[floor_ratio][j][2];
				}			
		}

		for(i=0;i<target_pointcount[refstack];i++)
		{	
			source_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
			source_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
			source_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
		source_pointcount[j]=target_pointcount[refstack];
	}
	//now rerun angle orientation just in case the remeshing has changed which point is at largest negative angle.
	orientate_curve_before_remeshing(target_array,target_stacks,target_pointcount);
	orientate_curve_before_remeshing(source_array,source_stacks,source_pointcount);

	//now every layer of both source and target has same number of points at every zstack. now arrange them to be at the same angles in the xy plane at different heights. e.g. target_array[i][0][x,y] should be at the same angle as target_array[i][j>0][x,y]

	//now remesh each zstack of the target shape to have each [i] represent the same angle in x-y plane at different heights, [j].
	
	double* ref_angles;
	ref_angles=malloc(sizeof(double)*target_pointcount[refstack]);	
	for(i=0;i<target_pointcount[refstack];i++)
	{	
		ref_angles[i]=atan2(target_array[i][refstack][1],target_array[i][refstack][0]);	
	}
	//int used_index=0;

	for(j=0;j<target_stacks;j++)
	{	
		double* compare_angles_target;
		compare_angles_target=malloc(sizeof(double)*target_pointcount[j]);
	
		//now have the list of angles we want the entire body to be aligned to.
	
		int compare_points=target_pointcount[refstack];
		for(i=0;i<target_pointcount[refstack];i++)
		{	
			two_nearest_neighbour_points_i(i,&ip1,&im1,target_pointcount[refstack]);
			compare_angles_target[i]=atan2(target_array[i][j][1],target_array[i][j][0]);
		}

				
		for(i=0;i<target_pointcount[refstack];i++)//loop over every point in the reference stack!
		{
			if(j==refstack)
			{
				dummy_array[i][0]=target_array[i][j][0];
				dummy_array[i][1]=target_array[i][j][1];//do nothing!
			}
			else
			{			
				
				for(k=0;k<target_pointcount[refstack];k++)
				{
					two_nearest_neighbour_points_i(k,&kp1,&km1,target_pointcount[refstack]);	
			
					if(compare_angles_target[k]<= ref_angles[i]&&compare_angles_target[kp1]>= ref_angles[i])
					{
						xi=target_array[k][j][0]; xip1=target_array[kp1][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[k][j][1]; yip1=target_array[kp1][j][1]; yim1=target_array[km1][j][1];
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
	
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						k=target_pointcount[j];//to move on to the next i point immediately
					
					}	
					else if((ref_angles[i]>=-M_PI)&&(compare_angles_target[0]>ref_angles[i]))
					{	//printf("i=%d j=%d ref_angles[i]=%f compare_angle[i]=%f\n",i,j,ref_angles[i],compare_angles_target[0]);
					
						xi=target_array[target_pointcount[j]-1][j][0]; xip1=target_array[0][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[target_pointcount[j]-1][j][1]; yip1=target_array[0][j][1]; yim1=target_array[km1][j][1];				
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
						if((fabs(ref_angles[i])-fabs(M_PI))<0.01||fabs(ref_angles[i]<0.01))
						{//to stop 1/tan(theta) giving infinite numerical results, do the simplification by hand
							a=0; //tan(theta)=0 actually implies the result is equal to (xi,yi)
						
						}
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						//printf("i=%d j=%d k=%d xi=%f xip1=%f yi=%f yip1=%f a=%f xt=%f yt=%f %f %f\n",i,j,k,xi,xip1,yi,yip1,a,dummy_array[i][0],dummy_array[i][1],compare_angles_target[0],ref_angles[i]);
						k=target_pointcount[j];
					}
					else if(ref_angles[i]>=compare_angles_target[target_pointcount[refstack]-1])
					{	
	
						xi=target_array[target_pointcount[j]-1][j][0]; xip1=target_array[0][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[target_pointcount[j]-1][j][1]; yip1=target_array[0][j][1]; yim1=target_array[km1][j][1];				
						
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
						if((fabs(ref_angles[i])-fabs(M_PI))<0.01)
						{//to stop 1/tan(theta) giving infinite numerical results, do the simplification by hand
							a=0;//tan(theta)=0 actually implies the result is equal to (xi,yi)
						
						}	
						a=0;			
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						k=target_pointcount[j];
					}				
					
				}
					
			}
		}

		for(i=0;i<target_pointcount[refstack];i++)
		{	
			target_array[i][j][0]=dummy_array[i][0];
			target_array[i][j][1]=dummy_array[i][1];		
		}
				
		free(compare_angles_target);
	}
	
	free(ref_angles);
	

	return;
}


/*void mesh_3d_xy(double*** source_array,double ***target_array,int* source_pointcount,int* target_pointcount,int source_stacks,int target_stacks,int refstack)
{//this function fills pos_array with the input data from input_array.
//input array is read in as a list of data points(x,y,z) sorted by angle around the centre of mass. The data in input array looks like for point i, [i][0]=0.5,[i][1]=0.5,[i][2]= 1.0 as an example of point i with [0]=x coordinate [1]=y , [2]=z
	int i,im1,ip1;
	int k,km1,kp1;
	int check=0;
	int backup_k;
	int points_assigned;
	double theta,thetap1,a,xi,yi,xip1,yip1,xim1,yim1;
	double zmintarget,zminsource;
	//pointcount is some array containing the number of points at each zstack.
	//the value of refstack passed to the function is the stack to use as a reference.
	
	//first redraw both the source and target mesh to have the same number of xy points at each height, given in pointcount[refstack]. Don't worry about matching to angles yet, just add/delete points depending on the ratio of the number of points at height j compared to the number in pointcount
	
	int running_tot=0;
	int j;
	//want to scale the number of points down from number at height j to match number at height refstack

	//rather crude approach as in 2d: bruteforce through the stack and just evenly space them along s1 initially, than afterwards remesh along s1 to get mesh to have symmetry in s2
	
	//find largest stack on target or source.
	int largeststack=0;
	for(j=0;j<target_stacks;j++)
	{
		if(target_pointcount[j]>largeststack) largeststack=target_pointcount[j];
	}
	for(j=0;j<source_stacks;j++)
	{
		if(source_pointcount[j]>largeststack) largeststack=source_pointcount[j];
	}
	double** dummy_array;
	array_alloc_2d_dbl(&dummy_array,largeststack,3);
	
	
	//sort both target and source by smallest angle to biggest, while maintaining order of points from the data file.
	orientate_curve_before_remeshing(target_array,target_stacks,target_pointcount);
	orientate_curve_before_remeshing(source_array,source_stacks,source_pointcount);

	//first spread points on stack j evenly along the s1 (constant second index), using the ratio of the points at that height.
	int floor_ratio,floor_ratio_p1;	
	double target_ratio,source_ratio;
	//printf("source_pointcount[j]=%d\n", source_pointcount[j]);
	source_ratio=(double)target_pointcount[refstack]/(double)source_pointcount[j];
	target_ratio=(double)target_pointcount[refstack]/(double)target_pointcount[j];

	double frac_ratio,gradientx,gradienty,finalposx,finalposy;

	zmintarget=target_array[0][0][2];
	zminsource=source_array[0][0][2];

	for(j=0;j<target_stacks;j++)
	{	
		target_ratio=(double)target_pointcount[refstack]/(double)target_pointcount[j];
		if(j==refstack)
		{
			for(i=0;i<target_pointcount[refstack];i++)
			{
			dummy_array[i][0]=target_array[i][j][0];
			dummy_array[i][1]=target_array[i][j][1]; //leave initial point unchanged, only converts to same size 
			dummy_array[i][2]=target_array[i][j][2];
				
			}
		}

		else for(i=0;i<target_pointcount[refstack];i++)
			{//else loop over the number of points in that stack, and increase/reduce the number of points to match the ref stack.						
			if(i==0){
				 dummy_array[i][0]=target_array[0][j][0];
				 dummy_array[i][1]=target_array[0][j][1];
				 dummy_array[i][2]=target_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points 				
					floor_ratio=(int)(floor(i/target_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(target_pointcount[j]-1)) floor_ratio_p1=0;
			
					frac_ratio=(i)/target_ratio-floor_ratio;
					gradientx=(target_array[floor_ratio_p1][j][0]-target_array[floor_ratio][j][0]);
					gradienty=(target_array[floor_ratio_p1][j][1]-target_array[floor_ratio][j][1]);
					dummy_array[i][0]=target_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=target_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=target_array[floor_ratio][j][2];
					
				}
			}


	for(i=0;i<target_pointcount[refstack];i++)
		{	
	//convert to same size scale in both xy plane and z directions
		target_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
		target_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
		target_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
	
	target_pointcount[j]=target_pointcount[refstack];
	}

	//printf("remeshed target, remeshing source\n");

	for(j=0;j<source_stacks;j++)
	{

	source_ratio=(double)target_pointcount[refstack]/(double)source_pointcount[j];
	//and do the same for the source shape!
	for(i=0;i<target_pointcount[refstack];i++)
		{
			if(i==0){dummy_array[i][0]=source_array[i][j][0];
				dummy_array[i][1]=source_array[i][j][1];
				dummy_array[i][2]=source_array[0][j][2];
				} //leave initial point unchanged
			else 
				{//remaining points defined using the ratio of the number of points
					
					floor_ratio=(int)(floor(i/source_ratio));
					floor_ratio_p1=floor_ratio+1;
					if(floor_ratio==(source_pointcount[j]-1)) {floor_ratio_p1=0;}
					frac_ratio=(i)/source_ratio-floor_ratio;
					gradientx=(source_array[floor_ratio_p1][j][0]-source_array[floor_ratio][j][0]);
					gradienty=(source_array[floor_ratio_p1][j][1]-source_array[floor_ratio][j][1]);

					dummy_array[i][0]=source_array[floor_ratio][j][0]+gradientx*frac_ratio;
					dummy_array[i][1]=source_array[floor_ratio][j][1]+gradienty*frac_ratio;
					dummy_array[i][2]=source_array[floor_ratio][j][2];
				}			
		}

		for(i=0;i<target_pointcount[refstack];i++)
		{	
			source_array[i][j][0]=dummy_array[i][0]*xy_pixelsize;
			source_array[i][j][1]=dummy_array[i][1]*xy_pixelsize;
			source_array[i][j][2]=dummy_array[i][2]*zstack_separation;	
		}
		source_pointcount[j]=target_pointcount[refstack];
	}
	//now rerun angle orientation just in case the remeshing has changed which point is at largest negative angle.
	orientate_curve_before_remeshing(target_array,target_stacks,target_pointcount);
	orientate_curve_before_remeshing(source_array,source_stacks,source_pointcount);

	//now every layer of both source and target has same number of points at every zstack. now arrange them to be at the same angles in the xy plane at different heights. e.g. target_array[i][0][x,y] should be at the same angle as target_array[i][j>0][x,y]

	//now remesh each zstack of the target shape to have each [i] represent the same angle in x-y plane at different heights, [j].
	
	double* ref_angles;
	ref_angles=malloc(sizeof(double)*target_pointcount[refstack]);	
	for(i=0;i<target_pointcount[refstack];i++)
	{	
		ref_angles[i]=atan2(target_array[i][refstack][1],target_array[i][refstack][0]);	
	}
	//int used_index=0;

	for(j=0;j<target_stacks;j++)
	{	
		double* compare_angles_target;
		compare_angles_target=malloc(sizeof(double)*target_pointcount[j]);
	
		//now have the list of angles we want the entire body to be aligned to.
	
		int compare_points=target_pointcount[refstack];
		for(i=0;i<target_pointcount[refstack];i++)
		{	
			two_nearest_neighbour_points_i(i,&ip1,&im1,target_pointcount[refstack]);
			compare_angles_target[i]=atan2(target_array[i][j][1],target_array[i][j][0]);
		}

				
		for(i=0;i<target_pointcount[refstack];i++)//loop over every point in the reference stack!
		{
			if(j==refstack)
			{
				dummy_array[i][0]=target_array[i][j][0];
				dummy_array[i][1]=target_array[i][j][1];//do nothing!
			}
			else
			{			
				
				for(k=0;k<target_pointcount[refstack];k++)
				{
					two_nearest_neighbour_points_i(k,&kp1,&km1,target_pointcount[refstack]);	
			
					if(compare_angles_target[k]<= ref_angles[i]&&compare_angles_target[kp1]>= ref_angles[i])
					{
						xi=target_array[k][j][0]; xip1=target_array[kp1][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[k][j][1]; yip1=target_array[kp1][j][1]; yim1=target_array[km1][j][1];
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
	
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						k=target_pointcount[j];//to move on to the next i point immediately
					
					}	
					else if((ref_angles[i]>=-M_PI)&&(compare_angles_target[0]>ref_angles[i]))
					{	//printf("i=%d j=%d ref_angles[i]=%f compare_angle[i]=%f\n",i,j,ref_angles[i],compare_angles_target[0]);
					
						xi=target_array[target_pointcount[j]-1][j][0]; xip1=target_array[0][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[target_pointcount[j]-1][j][1]; yip1=target_array[0][j][1]; yim1=target_array[km1][j][1];				
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
						if((fabs(ref_angles[i])-fabs(M_PI))<0.01||fabs(ref_angles[i]<0.01))
						{//to stop 1/tan(theta) giving infinite numerical results, do the simplification by hand
							a=0; //tan(theta)=0 actually implies the result is equal to (xi,yi)
						
						}
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						//printf("i=%d j=%d k=%d xi=%f xip1=%f yi=%f yip1=%f a=%f xt=%f yt=%f %f %f\n",i,j,k,xi,xip1,yi,yip1,a,dummy_array[i][0],dummy_array[i][1],compare_angles_target[0],ref_angles[i]);
						k=target_pointcount[j];
					}
					else if(ref_angles[i]>=compare_angles_target[target_pointcount[refstack]-1])
					{	
	
						xi=target_array[target_pointcount[j]-1][j][0]; xip1=target_array[0][j][0]; xim1=target_array[km1][j][0];
						yi=target_array[target_pointcount[j]-1][j][1]; yip1=target_array[0][j][1]; yim1=target_array[km1][j][1];				
						
						a=(yi-xi*tan(ref_angles[i]))/((xip1-xi)*tan(ref_angles[i])-(yip1-yi));
						if((fabs(ref_angles[i])-fabs(M_PI))<0.01)
						{//to stop 1/tan(theta) giving infinite numerical results, do the simplification by hand
							a=0;//tan(theta)=0 actually implies the result is equal to (xi,yi)
						
						}	
						a=0;			
						dummy_array[i][0]=xi+a*(xip1-xi);
						dummy_array[i][1]=yi+a*(yip1-yi);
						k=target_pointcount[j];
					}				
					
				}
					
			}
		}

		for(i=0;i<target_pointcount[refstack];i++)
		{	
			target_array[i][j][0]=dummy_array[i][0];
			target_array[i][j][1]=dummy_array[i][1];		
		}
				
		free(compare_angles_target);
	}
	
	free(ref_angles);
	

	return;
}*/

void mesh_3d_z(double*** source_array,double ***target_array,int* source_pointcount,int* target_pointcount,int source_stacks,int* target_stacks,int refstack)
{
	printf("changing mesh in out of plane direction\n");
	//Now create a mesh that has the same number of stacks on the target and source images. the source should be unchanged
	double ratio;

	ratio=((double)(*target_stacks)-1.0)/((double)source_stacks-1.0);//this ratio tells you the spacing between the 'new' mesh for the target.


	int i,j;       
   	int floor_ratio,floor_ratio_p1;
	double gradientx,gradienty;
	double frac_ratio;
	//create a dummy mesh to store the new mesh, and copy it back on to target_array afterwards.
	double*** dummy_array;

	
	if(source_stacks>*target_stacks){	array_alloc_3d_dbl(&dummy_array,target_pointcount[refstack],source_stacks,3); printf("dummy array has %d j columns\n",source_stacks);}
	else if(source_stacks<=*target_stacks){	array_alloc_3d_dbl(&dummy_array,target_pointcount[refstack],*target_stacks,3);printf("dummy array has %d j columns\n",*target_stacks);}

	for(j=0;j<source_stacks;j++)
		{		printf("ratio=%f, j*ratio=%f\n",ratio,j*ratio);
			for(i=0;i<target_pointcount[refstack];i++)
			{	
				if(j==0)
					{ //keep the bottom segment of the array fixed
						dummy_array[i][j][0]=target_array[i][j][0];
						dummy_array[i][j][1]=target_array[i][j][1];
						dummy_array[i][j][2]=target_array[i][j][2];
					}
				else if(floor(j*ratio)==(*target_stacks-1))
					{//keep top fixed too	
						//printf("keeping top fixed\n");
						dummy_array[i][j][0]=target_array[i][*target_stacks-1][0];
						dummy_array[i][j][1]=target_array[i][*target_stacks-1][1];
						dummy_array[i][j][2]=target_array[i][*target_stacks-1][2];
					}
				else 
				{
					//otherwise the stacks are positioned at heights of equal seperation, with a z seperation of ratio (in units of the distance between zstacks)
					floor_ratio=(int)(floor(j*ratio));
					floor_ratio_p1=floor_ratio+1.0;
					//if(floor_ratio_p1==target_pointcount[j]) floor_ratio_p1=0;
					frac_ratio=(j)*ratio-floor_ratio;
					//printf("i=%d j=%d frac_ratio=%f x=%f y=%f gradx=%f grady=%f\n",i,j,frac_ratio,source_array[i][floor_ratio][0],source_array[i][floor_ratio][1],gradientx,gradienty);
					gradientx=(target_array[i][floor_ratio_p1][0]-target_array[i][floor_ratio][0]);
					gradienty=(target_array[i][floor_ratio_p1][1]-target_array[i][floor_ratio][1]);

					dummy_array[i][j][0]=target_array[i][floor_ratio][0]+gradientx*frac_ratio;
					dummy_array[i][j][1]=target_array[i][floor_ratio][1]+gradienty*frac_ratio;
					dummy_array[i][j][2]=target_array[i][floor_ratio][2]+(target_array[i][floor_ratio_p1][2]-target_array[i][floor_ratio][2])*frac_ratio;
					//printf("i=%d j=%d thetabefore=%f theta after=%f\n",i,j,atan2(target_array[i][j][1],target_array[i][j][0]),atan2(dummy_array[i][j][1],dummy_array[i][j][0]));
	//if(i==13)printf("%d %d p1 =%f p2= %f fraction=%f final=%f \n",floor_ratio_p1,floor_ratio,source_array[i][floor_ratio_p1][1],source_array[i][floor_ratio][1],gradienty*frac_ratio,dummy_array[i][j][1]);
				}


			}
	}

	printf("now applying change to target mesh\n");
	for(j=0;j<source_stacks;j++)
	{
			for(i=0;i<target_pointcount[refstack];i++)
			{ 	// printf("i=%d j=%d dummy_array[i][j][0]=%f dummy_array[i][j][1]=%f dummy_array[i][j][2]=%f \n",i,j,dummy_array[i][j][0],dummy_array[i][j][1],dummy_array[i][j][2]);
				target_array[i][j][0]=dummy_array[i][j][0];
				target_array[i][j][1]=dummy_array[i][j][1];
				target_array[i][j][2]=dummy_array[i][j][2];
			}
		target_pointcount[j]=target_pointcount[refstack];//
	}
	for(j=source_stacks;j<*target_stacks;j++)
	{	//set all other points to zero
		//printf("setting stack j=%d to zero\n",j);
		for(i=0;i<target_pointcount[refstack];i++)
			{
				target_array[i][j][0]=0;
				target_array[i][j][1]=0;
				target_array[i][j][2]=0;
			}
		
	}
	*target_stacks=source_stacks;
	// also move both shapes so that they have z=0 on the lowest shape!
	double zmin_target=target_array[0][0][2];
	double zmin_source=source_array[0][0][2];
	for(j=0;j<source_stacks;j++)
	{	//do in one loop since they now have the same amount of stacks....
		for(i=0;i<target_pointcount[refstack];i++)
			{
				target_array[i][j][2]-=zmin_target;
				source_array[i][j][2]-=zmin_source;
			}
	}



	printf("z remesh finished\n");
	return;
}

void metric_tensor_3d(double*** metric,double*** inverse_metric,int i,int j,double*** shape,int refstack,int* shaperows,int z_stacks)
{
	if(straintype=='h') metric_tensor_3d_shell(metric,inverse_metric,i,j,shape,shaperows[j],z_stacks);

	return;
}


void metric_tensor_3d_shell(double ***metric,double ***inverse_metric,int i, int j,double ***pos_array,int nrows,int num_zstacks)
{
	double e1x,e1y,e1z,e2x,e2y,e2z,nx,ny,nz;
	double det;
	double r,theta;
	int ip1,ip2,im1,im2,jp1,jp2,jm1,jm2,jc;

	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);
	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	
	double s1,s2;//the length changes along surface in the s1,s2,n directions.
	double n;
	double rx,ry,rz,tx,ty,tz,t,s2x,s2y,s2z;
	double Nx,Ny,Nz;

	double dxds,dyds,phi,d2xds2,d2yds2,x,y;

	if(num_zstacks>1)
	{//3d shell basis (s1,n,s2) and no assumption about out of plane direction
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		distance_between_points_3d_array_j(pos_array,i,j,num_zstacks,&s2);

		//shell model metric, i.e. (s1,n,s2) basis
		direction_vectors_i(pos_array,&e1x,&e1y,&e1z,i,j,nrows);	
		direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,j,num_zstacks);
		normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);

		//now these can be used to get the metric tensor components
		//recall that metric is symmetric, so only 6 terms not 9.
 
		metric[i][j][0]=e1x*e1x+e1y*e1y+e1z*e1z;//es1.es1
		metric[i][j][1]=e1x*e2x+e1y*e2y+e1z*e2z;//es1.es2
		metric[i][j][2]=e1x*nx+e1y*ny+e1z*nz;//es1.n should come out as zero.
		metric[i][j][3]=e2x*e2x+e2y*e2y+e2z*e2z;//es2.es2 
		metric[i][j][4]=e2x*nx+e2y*ny+e2z*nz;//es2.n. Should come out as zero.
		metric[i][j][5]=nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
	}
	else if(num_zstacks==1)
	{
		//in plane directions desgined the same way, but make an assumption abou the out of plane direction shape.
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		distance_between_points_3d_array_j(pos_array,i,j,num_zstacks,&s2);

		//shell model metric, i.e. (s1,n,s2) basis
		direction_vectors_i(pos_array,&e1x,&e1y,&e1z,i,j,nrows);	
		direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,j,num_zstacks);
		normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);

		//now these can be used to get the metric tensor components
		//recall that metric is symmetric, so only 6 terms not 9.
 
		metric[i][j][0]=e1x*e1x+e1y*e1y+e1z*e1z;//es1.es1
		metric[i][j][1]=e1x*e2x+e1y*e2y+e1z*e2z;//es1.es2
		metric[i][j][2]=e1x*nx+e1y*ny+e1z*nz;//es1.n should come out as zero.
		metric[i][j][3]=e2x*e2x+e2y*e2y+e2z*e2z;//es2.es2 
		metric[i][j][4]=e2x*nx+e2y*ny+e2z*nz;//es2.n. Should come out as zero.
		metric[i][j][5]=nx*nx+ny*ny+nz*nz;//n.n. //should be 1!		

	}
	
	invert_metric(metric,inverse_metric,dimensions,i,j);	
	return;
}
/*
void metric_tensor_3d_solid(double ***metric,double ***inverse_metric,int i, int j,double ***pos_array,int nrows,int num_zstacks)
{
	//solid model metric. i.e. (r,theta,s2) basis. define metric components as [0]=rr,[1]=rtheta,[2]=rs2,[3]=thetatheta,[4]=thetas2,[5]=s2s2 in the fully 3d data case
	//here, rx,y,z are the components of the radial vector (in xy plane)  t are theta directions, same notation
	double x=pos_array[i][j][0];
	double y=pos_array[i][j][1];			
	double r=sqrt(x*x+y*y);	
	double rx=x/r;
	double ry=y/r;
	double rz=0; //nb this should always be zero, since z is held constant over varied i.

	double tx=-pos_array[i][j][1]/(r*r);
	double ty=pos_array[i][j][0]/(r*r);
	double tz=0;

	direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,j,num_zstacks);
	//now these can be used to get the metric tensor components
	//recall that metric is symmetric. 
	metric[i][j][0]=rx*rx+ry*ry+rz*rz;//r.r
	metric[i][j][1]=rx*tx+ry*ty+rz*tz;//r.theta should be zero
	metric[i][j][2]=rx*e2x+ry*e2y+rz*e2z;//r.e2 should come out as zero?
	metric[i][j][3]=tx*tx+ty*ty+tz*tz;//t.t , should be 1/r^2 
	metric[i][j][4]=e2x*tx+e2y*ty+e2z*tz;//e2.t. Should come out as zero.
	metric[i][j][5]=e2x*e2x+e2y*e2y+e2z*e2z;//e2.e2 determined by 3d shape
		
	invert_metric(metric,inverse_metric,dimensions,i,j);

	return;
}

void metric_tensor_2d_shell_fill_channel(double ***metric,double ***inverse_metric,int i, int j,double ***pos_array,int nrows,int num_zstacks)
{//	else if(mode2d=='p'&& straintype=='h' && dimensions==2)
	
	//psuedo 2d mode where the metric tensor is given by an analytical expression, representing the channel being entirely filled.
	distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);

		x=pos_array[i][j][0];
		y=pos_array[i][j][1];
		dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
		dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;
		d2xds2=(pos_array[ip1][j][0]-2*pos_array[i][j][0]+pos_array[im1][j][0])/(s1*s1);
		d2xds2=(pos_array[ip1][j][1]-2*pos_array[i][j][1]+pos_array[im1][j][1])/(s1*s1);

		phi=0;//since we only see in data the central line where phi =0...
		if(straintype=='h')
		{			
			n=sqrt(Nx*Nx+Ny*Ny+Nz*Nz);
			//the normal vector is normalised.

			normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);
			//recall that metric is symmetric. 
			metric[i][j][0]=dxds * dxds + dyds * dyds * pow(pow(cos(phi), d), 0.2e1);//es1.es1
			metric[i][j][1]=-dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi);//es1.ephi
			metric[i][j][2]=0;//es1.n
			metric[i][j][3]= y * y * pow(pow(cos(phi), (double) (d - 1)),2) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi),2) + pow(Lz,.2) * pow(cos(phi),2);//ephi.ephi 
			metric[i][j][4]=0;//ephi.n
			metric[i][j][5]=1;//nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}	
		else if(straintype=='o')
		{
			//in solid model, we use the coordinate system (r,theta,phi) where phi is the angle as in the shell model.
			distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
			x=pos_array[i][j][0];
			y=pos_array[i][j][1];
			r=sqrt(x*x+y*y);
			theta=atan2(y,x);
			dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
			dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;

			phi=0;//since we only look at the central line where phi =0...

			metric[i][j][0]=pow(cos(theta), 0.2e1) + pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//er.er
			metric[i][j][1]=-cos(theta) * r * sin(theta) + sin(theta) * pow(pow(cos(phi), d), 2) * r * cos(theta);//er.etheta
			metric[i][j][2]=-pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * r * d * sin(phi) / cos(phi);//er.ephi
			metric[i][j][3]= pow(r, 0.2e1) * pow(sin(theta), 0.2e1) + pow(r, 0.2e1) * pow(cos(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//etheta.etheta 
			metric[i][j][4]=-pow(r, 0.2e1) * cos(theta) * pow(pow(cos(phi), d), 0.2e1) * sin(theta) * d * sin(phi) / cos(phi);//etheta.ephi
			metric[i][j][5]=pow(r, 0.2e1) * pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * pow(cos(phi), -0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1);//ephi.ephi
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}
	return;
}


	}
	else if(mode2d=='y'&& straintype=='h'&& dimensions==2)
	{//psuedo 2d mode where the  shape is 3d, but with constant shape in z (i.e. the cross section given by (x,y) is the shape of the surface for all z in the constriction.)
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
		x=pos_array[i][j][0];
		y=pos_array[i][j][1];
		dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
		dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;
		d2xds2=(pos_array[ip1][j][0]-2*pos_array[i][j][0]+pos_array[im1][j][0])/(s1*s1);
		d2xds2=(pos_array[ip1][j][1]-2*pos_array[i][j][1]+pos_array[im1][j][1])/(s1*s1);
		//phi=0;//since we only see in data the central line where phi =0...
		if(straintype=='h')
		{
			n=sqrt(Nx*Nx+Ny*Ny+Nz*Nz);
			//the normal vector is normalised.
		
			nx=Nx/n;
			ny=Ny/n;
			nz=Nz/n;
			//printf("%f %f %f %f\n",nx,ny,nz,n);
			//recall that metric is symmetric. 
			metric[i][j][0]=dxds * dxds + dyds * dyds;//es1.es1, will be 1 because of the normalised relation between s and x,y
			metric[i][j][1]=0;//es1.ez
			metric[i][j][2]=0;//es1.n
			metric[i][j][3]=1;//ez.ez 
			metric[i][j][4]=0;//ez.n
			metric[i][j][5]=1;//nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
		
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}	
		else if(straintype=='o')
		{
			//in solid model, we use the coordinate system (r,theta,phi) where phi is the angle as in the shell model.
			distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
			x=pos_array[i][j][0];
			y=pos_array[i][j][1];
			r=sqrt(x*x+y*y);
			theta=atan2(y,x);
			dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
			dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;

			phi=0;//since we only look at the central line where phi =0...

			metric[i][j][0]=pow(cos(theta), 0.2e1) + pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//er.er
			metric[i][j][1]=-cos(theta) * r * sin(theta) + sin(theta) * pow(pow(cos(phi), d), 2) * r * cos(theta);//er.etheta
			metric[i][j][2]=-pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * r * d * sin(phi) / cos(phi);//er.ephi
			metric[i][j][3]= pow(r, 0.2e1) * pow(sin(theta), 0.2e1) + pow(r, 0.2e1) * pow(cos(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//etheta.etheta 
			metric[i][j][4]=-pow(r, 0.2e1) * cos(theta) * pow(pow(cos(phi), d), 0.2e1) * sin(theta) * d * sin(phi) / cos(phi);//etheta.ephi
			metric[i][j][5]=pow(r, 0.2e1) * pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * pow(cos(phi), -0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1);//ephi.ephi
		
		
			/*inverse_metric[i][j][0]=(y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1)) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1)); //es1.es1;


			inverse_metric[i][j][1]=dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));//es1.ephi


			inverse_metric[i][j][2]=0; //e1.en
			inverse_metric[i][j][3]=dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));

; //ephi.ephi
			inverse_metric[i][j][4]=0;//ephi.en
			inverse_metric[i][j][5]=1;//n.n
		

			invert_metric(metric,inverse_metric,dimensions,i,j);
			}
	return;
	}
return;
}
		     (double*** metric,double*** inverse_metric,int i,int j,double*** shape,int refstack,int* shaperows,int z_stacks)
void metric_tensor_3d(double*** metric,double*** inverse_metric,int i,int j,double*** target_shape,int* targetrows[j],int* target_stacks);
{
if(dimensions==3||mode2d=='c')//completely 2d or 3d mode.
	{	//printf("i=%d ip1=%d im1=%d pos_array[im1][j][0]=%f\n",i,ip1,im1,pos_array[im1][j][0]);
		
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		if(dimensions==3) distance_between_points_3d_array_j(pos_array,i,j,num_zstacks,&s2);

		if(straintype=='h')
		{	
			//shell model metric, i.e. (s1,n,s2) basis
			direction_vectors_i(pos_array,&e1x,&e1y,&e1z,i,j,nrows);	
			direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,j,num_zstacks);
			normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);

			//now these can be used to get the metric tensor components
			//recall that metric is symmetric. 
			metric[i][j][0]=e1x*e1x+e1y*e1y+e1z*e1z;//es1.es1
			metric[i][j][1]=e1x*e2x+e1y*e2y+e1z*e2z;//es1.es2
			metric[i][j][2]=e1x*nx+e1y*ny+e1z*nz;//es1.n should come out as zero.
			metric[i][j][3]=e2x*e2x+e2y*e2y+e2z*e2z;//es2.es2 
			metric[i][j][4]=e2x*nx+e2y*ny+e2z*nz;//es2.n. Should come out as zero.
			metric[i][j][5]=nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
	
			invert_metric(metric,inverse_metric,dimensions,i,j);	
		return;
		}
		else if(straintype=='o')
		{	//solid model metric. i.e. (r,theta,s2) basis. define metric components as [0]=rr,[1]=rtheta,[2]=rs2,[3]=thetatheta,[4]=thetas2,[5]=s2s2 in the fully 3d data case
			//here, rx,y,z are the components of the radial vector (in xy plane)  t are theta directions, same notation
			x=pos_array[i][j][0];
			y=pos_array[i][j][1];			
			r=sqrt(x*x+y*y);	
			rx=x/r;
			ry=y/r;
			rz=0; //nb this should always be zero, since z is held constant over varied i.

			tx=-pos_array[i][j][1]/(r*r);
			ty=pos_array[i][j][0]/(r*r);
			tz=0;

			if(dimensions==3)
			{ 	//also have the s2 components
				direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,j,num_zstacks);
			}
			else
			{
				e2x=0;
				e2y=0;
				e2z=0;
			}
	
			//now these can be used to get the metric tensor components
			//recall that metric is symmetric. 
			metric[i][j][0]=rx*rx+ry*ry+rz*rz;//r.r
			metric[i][j][1]=rx*tx+ry*ty+rz*tz;//r.theta should be zero
			metric[i][j][2]=rx*e2x+ry*e2y+rz*e2z;//r.e2 should come out as zero?
			metric[i][j][3]=tx*tx+ty*ty+tz*tz;//t.t , should be 1/r^2 
			metric[i][j][4]=e2x*tx+e2y*ty+e2z*tz;//e2.t. Should come out as zero.
			metric[i][j][5]=e2x*e2x+e2y*e2y+e2z*e2z;//e2.e2 determined by 3d shape
		
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}
		return;
	}
	else if(mode2d=='p'&& straintype=='h' && dimensions==2)
	{
		//psuedo 2d mode where the metric tensor is given by some analytical expression rather than purely numerically.
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
		x=pos_array[i][j][0];
		y=pos_array[i][j][1];
		dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
		dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;
		d2xds2=(pos_array[ip1][j][0]-2*pos_array[i][j][0]+pos_array[im1][j][0])/(s1*s1);
		d2xds2=(pos_array[ip1][j][1]-2*pos_array[i][j][1]+pos_array[im1][j][1])/(s1*s1);

		phi=0;//since we only see in data the central line where phi =0...
		if(straintype=='h')
		{			
			n=sqrt(Nx*Nx+Ny*Ny+Nz*Nz);
			//the normal vector is normalised.

			normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);
			//recall that metric is symmetric. 
			metric[i][j][0]=dxds * dxds + dyds * dyds * pow(pow(cos(phi), d), 0.2e1);//es1.es1
			metric[i][j][1]=-dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi);//es1.ephi
			metric[i][j][2]=0;//es1.n
			metric[i][j][3]= y * y * pow(pow(cos(phi), (double) (d - 1)),2) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi),2) + pow(Lz,.2) * pow(cos(phi),2);//ephi.ephi 
			metric[i][j][4]=0;//ephi.n
			metric[i][j][5]=1;//nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}	
		else if(straintype=='o')
		{
			//in solid model, we use the coordinate system (r,theta,phi) where phi is the angle as in the shell model.
			distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
			x=pos_array[i][j][0];
			y=pos_array[i][j][1];
			r=sqrt(x*x+y*y);
			theta=atan2(y,x);
			dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
			dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;

			phi=0;//since we only look at the central line where phi =0...

			metric[i][j][0]=pow(cos(theta), 0.2e1) + pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//er.er
			metric[i][j][1]=-cos(theta) * r * sin(theta) + sin(theta) * pow(pow(cos(phi), d), 2) * r * cos(theta);//er.etheta
			metric[i][j][2]=-pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * r * d * sin(phi) / cos(phi);//er.ephi
			metric[i][j][3]= pow(r, 0.2e1) * pow(sin(theta), 0.2e1) + pow(r, 0.2e1) * pow(cos(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//etheta.etheta 
			metric[i][j][4]=-pow(r, 0.2e1) * cos(theta) * pow(pow(cos(phi), d), 0.2e1) * sin(theta) * d * sin(phi) / cos(phi);//etheta.ephi
			metric[i][j][5]=pow(r, 0.2e1) * pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * pow(cos(phi), -0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1);//ephi.ephi
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}
	return;
	}
	else if(mode2d=='y'&& straintype=='h'&& dimensions==2)
	{//psuedo 2d mode where the  shape is 3d, but with constant shape in z (i.e. the cross section given by (x,y) is the shape of the surface for all z in the constriction.)
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
		x=pos_array[i][j][0];
		y=pos_array[i][j][1];
		dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
		dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;
		d2xds2=(pos_array[ip1][j][0]-2*pos_array[i][j][0]+pos_array[im1][j][0])/(s1*s1);
		d2xds2=(pos_array[ip1][j][1]-2*pos_array[i][j][1]+pos_array[im1][j][1])/(s1*s1);
		//phi=0;//since we only see in data the central line where phi =0...
		if(straintype=='h')
		{
			n=sqrt(Nx*Nx+Ny*Ny+Nz*Nz);
			//the normal vector is normalised.
		
			nx=Nx/n;
			ny=Ny/n;
			nz=Nz/n;
			//printf("%f %f %f %f\n",nx,ny,nz,n);
			//recall that metric is symmetric. 
			metric[i][j][0]=dxds * dxds + dyds * dyds;//es1.es1, will be 1 because of the normalised relation between s and x,y
			metric[i][j][1]=0;//es1.ez
			metric[i][j][2]=0;//es1.n
			metric[i][j][3]=1;//ez.ez 
			metric[i][j][4]=0;//ez.n
			metric[i][j][5]=1;//nx*nx+ny*ny+nz*nz;//n.n. //should be 1!
		
			invert_metric(metric,inverse_metric,dimensions,i,j);
		}	
		else if(straintype=='o')
		{
			//in solid model, we use the coordinate system (r,theta,phi) where phi is the angle as in the shell model.
			distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		
			x=pos_array[i][j][0];
			y=pos_array[i][j][1];
			r=sqrt(x*x+y*y);
			theta=atan2(y,x);
			dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
			dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;

			phi=0;//since we only look at the central line where phi =0...

			metric[i][j][0]=pow(cos(theta), 0.2e1) + pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//er.er
			metric[i][j][1]=-cos(theta) * r * sin(theta) + sin(theta) * pow(pow(cos(phi), d), 2) * r * cos(theta);//er.etheta
			metric[i][j][2]=-pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * r * d * sin(phi) / cos(phi);//er.ephi
			metric[i][j][3]= pow(r, 0.2e1) * pow(sin(theta), 0.2e1) + pow(r, 0.2e1) * pow(cos(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1);//etheta.etheta 
			metric[i][j][4]=-pow(r, 0.2e1) * cos(theta) * pow(pow(cos(phi), d), 0.2e1) * sin(theta) * d * sin(phi) / cos(phi);//etheta.ephi
			metric[i][j][5]=pow(r, 0.2e1) * pow(sin(theta), 0.2e1) * pow(pow(cos(phi), d), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * pow(cos(phi), -0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1);//ephi.ephi
		
		
			inverse_metric[i][j][0]=(y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1)) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1)); //es1.es1;


			inverse_metric[i][j][1]=dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));//es1.ephi


			inverse_metric[i][j][2]=0; //e1.en
			inverse_metric[i][j][3]=dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));

; //ephi.ephi
			inverse_metric[i][j][4]=0;//ephi.en
			inverse_metric[i][j][5]=1;//n.n
		

			invert_metric(metric,inverse_metric,dimensions,i,j);
			}
	return;
	}


}*/

void invert_metric(double*** metric,double*** inverse_metric,int dimensions,int i,int j)
{
	double det;
	int k;
	if(dimensions==2 && mode2d=='c')
	{
		det=metric[i][j][0]*metric[i][j][3]-metric[i][j][1]*metric[i][j][1];//ae-bd
		if(det!=0)
		{
			inverse_metric[i][j][0]=1/det*metric[i][j][3];
			inverse_metric[i][j][1]=-1/det*metric[i][j][1];
			inverse_metric[i][j][2]=0;
			inverse_metric[i][j][3]=1/det*metric[i][j][0];
			inverse_metric[i][j][4]=0;
			inverse_metric[i][j][5]=0;
		}
		else
		{
			//printf("warning det=0 for point (i=%d j=%d)\n",i,j);
			for(k=0;k<6;k++) inverse_metric[i][j][k]=0;
		}
		
	}
	else
	{
		det=metric[i][j][0]*metric[i][j][3]*metric[i][j][5]-metric[i][j][0]*metric[i][j][4]*metric[i][j][4]
		   -metric[i][j][1]*metric[i][j][1]*metric[i][j][5]+metric[i][j][1]*metric[i][j][4]*metric[i][j][2]
		   +metric[i][j][2]*metric[i][j][1]*metric[i][j][4]-metric[i][j][2]*metric[i][j][3]*metric[i][j][2];
		if(det!=0)
		{
			inverse_metric[i][j][0]=1/det*(metric[i][j][3]*metric[i][j][5]-metric[i][j][4]*metric[i][j][4]) ;//ei-fh
			inverse_metric[i][j][1]=1/det*(metric[i][j][1]*metric[i][j][5]-metric[i][j][2]*metric[i][j][1]);//bi-cd
			inverse_metric[i][j][2]=1/det*(metric[i][j][1]*metric[i][j][4]-metric[i][j][2]*metric[i][j][3]);//bf-ce
			inverse_metric[i][j][3]=1/det*(metric[i][j][0]*metric[i][j][5]-metric[i][j][2]*metric[i][j][2]);//ai-cg
			inverse_metric[i][j][4]=1/det*(metric[i][j][0]*metric[i][j][4]-metric[i][j][2]*metric[i][j][1]);//af-cd
			inverse_metric[i][j][5]=1/det*(metric[i][j][0]*metric[i][j][3]-metric[i][j][1]*metric[i][j][1]);//ae-bd
		}
		else
		{
			//printf("warning det=0 for point (i=%d j=%d)\n",i,j);
			for(k=0;k<6;k++) inverse_metric[i][j][k]=0;
		}
	}

	return;
}

void normal_vector(double e1x,double e1y,double e1z,double e2x,double e2y,double e2z,double* nx,double* ny,double* nz,int dimensions,int num_zstacks)
{
	double Nx,Ny,Nz,n;
	if(dimensions==3 && num_zstacks>1)
	{
		Nx=(e1y*e2z-e2y*e1z);
		Ny=(-e1x*e2z+e1z*e2x);
		Nz=(e1x*e2y-e1y*e2x);
	}
	else
	{			
		Nx=e1y;
		Ny=-e1x;
		Nz=0;
	}
	n=sqrt(Nx*Nx+Ny*Ny+Nz*Nz);
	//the normal vector is normalised.
				
	*nx=Nx/n;
	*ny=Ny/n;
	*nz=Nz/n;

	return;
}


void chr_symbols_3d(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** pos_array,int i,int j,int nrows,int num_zstacks)
{	
	//printf("finding chr symbols for i=%d j=%d\n",i,j);
	int ip1,ip2,im1,im2,jp1,jp2,jm1,jm2,jc;

	double dxds,dyds,d2xds2,d2yds2,y,x;
	double gs1s1,gs1n,gs1s2,gnn,gs2n,gs2s2;
	double ginv_s1s1,ginv_s1n,ginv_s1s2,ginv_nn,ginv_s2n,ginv_s2s2;
	double dgs1s1ds1,dgs1nds1,dgs1s2ds1,dgs2s2ds1,dgs2nds1,dgnnds1;
	double dgs1s1ds2,dgs1nds2,dgs1s2ds2,dgs2s2ds2,dgs2nds2,dgnnds2;
	double dgs1s1dn,dgs1ndn,dgs1s2dn,dgs2s2dn,dgs2ndn,dgnndn;
	double r;
	double phi=0;
	gs1s1=metric[i][j][0];//s1s1=11
	gs1s2=metric[i][j][1];//s1s2=12
	gs1n=metric[i][j][2];//s1n=13
	gs2s2=metric[i][j][3];//s2s2
	gs2n=metric[i][j][4];//s2n=23
	gnn=metric[i][j][5];//nn=33
	
	ginv_s1s1=inverse_metric[i][j][0];
	ginv_s1s2=inverse_metric[i][j][1];
	ginv_s1n=inverse_metric[i][j][2];
	ginv_s2s2=inverse_metric[i][j][3];
	ginv_s2n=inverse_metric[i][j][4];
	ginv_nn=inverse_metric[i][j][5];
	
	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);

	if(dimensions==3 && num_zstacks>1) two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
//	printf("dimensions=%d straintype=%c\n",dimensions,straintype);
	double s1,s2,n;//the distances between points in the s1,s2 directions. used for the derivatives on the surface which give the unit vectors.
//printf("pos:[i][j] %f %f [ip1][j] %f %f [im1][j] %f %f [i][jp1] %f %f [i][jm1] %f %f \n	metric: %f %f %f %f %f %f\n",pos_array[i][j][0],pos_array[i][j][1],pos_array[ip1][j][0],pos_array[ip1][j][1],pos_array[im1][j][0],pos_array[im1][j][1],pos_array[i][jp1][0],pos_array[i][jp1][1],pos_array[i][jm1][0],pos_array[i][jm1][1],metric[i][j][0],metric[i][j][1],metric[i][j][2],metric[i][j][3],metric[i][j][4],metric[i][j][5]);
	if(dimensions==3 && straintype=='h')
	{ 	//3d shell.
		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		distance_between_points_3d_array_j(pos_array,i,j,num_zstacks,&s2);
	//	printf("s1=%f s2=%f\n",s1,s2);
		//derivatives along the mesh at constant s2
		dgs1s1ds1=(metric[ip1][j][0]-metric[im1][j][0])/s1;
		dgs1s2ds1=(metric[ip1][j][1]-metric[im1][j][1])/s1;
		dgs1nds1=(metric[ip1][j][2]-metric[im1][j][2])/s1;
		dgs2s2ds1=(metric[ip1][j][3]-metric[im1][j][3])/s1;
		dgs2nds1=(metric[ip1][j][4]-metric[im1][j][4])/s1;
		dgnnds1=(metric[ip1][j][5]-metric[im1][j][5])/s1;

		//derivatives along the mesh at constant angle (in xy plane)
		if(num_zstacks>1)
		{
			dgs1s1ds2=(metric[i][jp1][0]-metric[i][jm1][0])/s2;
			dgs1s2ds2=(metric[i][jp1][1]-metric[i][jm1][1])/s2;
			dgs1nds2=(metric[i][jp1][2]-metric[i][jm1][2])/s2;
			dgs2s2ds2=(metric[i][jp1][3]-metric[i][jm1][3])/s2;
			dgs2nds2=(metric[i][jp1][4]-metric[i][jm1][4])/s2;
			dgnnds2=(metric[i][jp1][5]-metric[i][jm1][5])/s2;
		}
		else 
		{
			dgs1s1ds2=0;
			dgs1s2ds2=0;
			dgs1nds2=0;
			dgs2s2ds2=0;
			dgs2nds2=0;
			dgnnds2=0;
		}
		dgs1s1dn=0;
		dgs1ndn=0;
		dgs1s2dn=0;
		dgs2s2dn=0;
		dgs2ndn=0;
		dgnndn=0;

		//use the purely numerical expressions for the (s1,s2,n) basis.
		//in shell model let the components be as follows

		//[0]=^s1_s1s1//[1]=^s1_s1s2=^s1_s2s1//[2]=^s1_s1n=^s1_ns1//[3]=^s1_s2s2,//[4]=^s1_ns2=s2n,//[5]=^s1_nn 
		//[6]=^n_s1s1,[7]=^n_s1s2=^n_s2s1,[8]=^n_s1n=^n_ns1, [9]=^n_s2s2,[10]=^n_s2n,[11]=^n_nn
		//[12]=^s2_s1s1//[13]=^s2_s1s2=^s2_s2s1//[14]=^s2_s1n=^s2_ns1//[15]=^s2_s2s2,//[16]=^s2_ns2=s2n,//[17]=^s2_nn 

		//NOTE the terms involving the normals here aren't used, hence why set to zero, but left in in case decide to use them later...
		christoffel_array[i][j][0]=0.5*ginv_s1s1*(dgs1s1ds1)+0.5*ginv_s1n*(2*dgs1nds1-dgs1s1dn);
		christoffel_array[i][j][1]=0.5*ginv_s1s1*(dgs1s1ds2)+0.5*ginv_s1n*(dgs2nds1+dgs2nds2-dgs1s2dn)+0.5*ginv_s1s2*(dgs2s2ds1);
		christoffel_array[i][j][2]=0.5*ginv_s1s1*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_s1n*(dgnnds1);
		christoffel_array[i][j][3]=0.5*ginv_s1s1*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_s1n*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s1s2*(dgs2s2ds2);
		christoffel_array[i][j][4]=0.5*ginv_s1s1*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_s1n*(dgnnds2)+0.5*ginv_s1s2*(dgs2s2dn);	
		christoffel_array[i][j][5]=0.5*ginv_s1s1*(2*dgs1ndn-dgnnds1)+0.5*ginv_s1n*(dgnndn);
		christoffel_array[i][j][6]=0.5*ginv_s1n*(dgs1s1ds1)+0.5*ginv_nn*(2*dgs1nds1-dgs1s1dn);
		christoffel_array[i][j][7]=0.5*ginv_s1n*(dgs1s1ds2)+0.5*ginv_nn*(dgs2nds1+dgs1nds2-dgs1s2dn)+0.5*ginv_s2n*(dgs2s2ds1);
		christoffel_array[i][j][8]=0.5*ginv_s1n*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_nn*(dgnnds1);
		christoffel_array[i][j][9]=0.5*ginv_s1n*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_nn*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s2n*(dgs2s2ds2);
		christoffel_array[i][j][10]=0.5*ginv_s1n*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_nn*(dgnnds2)+0.5*ginv_s2n*(dgs2s2dn);
		christoffel_array[i][j][11]=0.5*ginv_s1n*(2*dgs1ndn-dgnnds1)+0.5*ginv_nn*(dgnndn);
		christoffel_array[i][j][12]=0.5*ginv_s1s2*(dgs1s1ds1)+0.5*ginv_s2n*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_s2s2*(2*dgs1s2ds1-dgs1s1ds2);
		christoffel_array[i][j][13]=0.5*ginv_s1s2*(dgs1s1ds2)+0.5*ginv_s2n*(dgs2nds1+dgs1nds2-dgs1s2dn)+0.5*ginv_s2s2*(dgs2s2ds1);
		christoffel_array[i][j][14]=0.5*ginv_s1s2*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_s2n*(dgnnds1)+0.5*ginv_s2s2*(dgs2nds1+dgs1s2dn-dgs1nds2);
		christoffel_array[i][j][15]=0.5*ginv_s1s2*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_s2n*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s2s2*(dgs2s2ds2);
		christoffel_array[i][j][16]=0.5*ginv_s1s2*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_s2n*(dgnnds2)+0.5*ginv_s2s2*(dgs2s2dn);
		christoffel_array[i][j][17]=0.5*ginv_s1s2*(2*dgs1ndn-dgnnds1)+0.5*ginv_s2n*(dgnndn)+0.5*ginv_s2s2*(2*dgs2ndn-dgnnds2);

		return;	
	}
	else if(dimensions==2 && mode2d=='c')
	{
		//completely 2d shell.
		//in completely 2d case, there is no "s2" direction, all terms involving s2 are set to 0
		//also (see metric function) the [1],[3],[4] terms of metric are 0 everywhere.

		//[0]=^s1_s1s1//[1]=^s1_s1s2=^s1_s2s1//[2]=^s1_s1n=^s1_ns1//[3]=^s1_s2s2,//[4]=^s1_ns2=s2n,//[5]=^s1_nn 
		//[6]=^n_s1s1,[7]=^n_s1s2=^n_s2s1,[8]=^n_s1n=^n_ns1, [9]=^n_s2s2,[10]=^n_s2n,[11]=^n_nn
		//[12]=^s2_s1s1//[13]=^s2_s1s2=^s2_s2s1//[14]=^s2_s1n=^s2_ns1//[15]=^s2_s2s2,//[16]=^s2_ns2=s2n,//[17]=^s2_nn 

		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
			
		s2=0;	

		//derivatives along the mesh at constant s2 are only non zeros- all s2 terms set to zero
		dgs1s1ds1=(metric[ip1][j][0]-metric[im1][j][0])/s1;
		dgs1s2ds1=0;
		dgs1nds1=(metric[ip1][j][2]-metric[im1][j][2])/s1;
		dgs2s2ds1=0;
		dgs2nds1=0;
		dgnnds1=(metric[ip1][j][5]-metric[im1][j][5])/s1;

	
		dgs1s1ds2=0;
		dgs1s2ds2=0;//(metric[i][jp1][1]-metric[i][jm1][1])/s2;
		dgs1nds2=0;//(metric[i][jp1][2]-metric[i][jm1][2])/s2;
		dgs2s2ds2=0;//(metric[i][jp1][3]-metric[i][jm1][3])/s2;
		dgs2nds2=0;//(metric[i][jp1][4]-metric[i][jm1][4])/s2;
		dgnnds2=0;//(metric[i][jp1][5]-metric[i][jm1][5])/s2;
		
		dgs1s1dn=0;
		dgs1ndn=0;
		dgs1s2dn=0;
		dgs2s2dn=0;
		dgs2ndn=0;
		dgnndn=0;
		//in completely 1d shell case, the metric is a 1x1 matrix and there is only one cijk.
		christoffel_array[i][j][0]=0.5*ginv_s1s1*(dgs1s1ds1);
		christoffel_array[i][j][2]=0;
		christoffel_array[i][j][5]=0;

		christoffel_array[i][j][6]=0;
		christoffel_array[i][j][8]=0;
		christoffel_array[i][j][11]=0;

		//in purely 2d case, all the s2 involved symbols should be zero
		christoffel_array[i][j][1]=0;
		christoffel_array[i][j][3]=0;
		christoffel_array[i][j][4]=0;		
		christoffel_array[i][j][7]=0;
		christoffel_array[i][j][9]=0;
		christoffel_array[i][j][10]=0;
		christoffel_array[i][j][12]=0;
		christoffel_array[i][j][13]=0;
		christoffel_array[i][j][14]=0;
		christoffel_array[i][j][15]=0;
		christoffel_array[i][j][16]=0;
		christoffel_array[i][j][17]=0;
	
		return;
	}
	else if(dimensions==2 && mode2d=='p')
	{
		//psuedo 2d shell.
		//in psuedo 2d case, all christoffel terms are calculated using an analytical approx to the shape.

		//[0]=^s1_s1s1//[1]=^s1_s1s2=^s1_s2s1//[2]=^s1_s1n=^s1_ns1//[3]=^s1_s2s2,//[4]=^s1_ns2=s2n,//[5]=^s1_nn 
		//[6]=^n_s1s1,[7]=^n_s1s2=^n_s2s1,[8]=^n_s1n=^n_ns1, [9]=^n_s2s2,[10]=^n_s2n,[11]=^n_nn
		//[12]=^s2_s1s1//[13]=^s2_s1s2=^s2_s2s1//[14]=^s2_s1n=^s2_ns1//[15]=^s2_s2s2,//[16]=^s2_ns2=s2n,//[17]=^s2_nn 
		
		four_nearest_neighbour_points_i(i,&ip1,&im1,&ip2,&im2,nrows);

		distance_between_points_3d_array_i(pos_array,i,j,nrows,&s1);
		x=pos_array[i][j][0];
		y=pos_array[i][j][1];
		dxds=(pos_array[ip1][j][0]-pos_array[im1][j][0])/s1;
		dyds=(pos_array[ip1][j][1]-pos_array[im1][j][1])/s1;
		d2xds2=(pos_array[ip1][j][0]-2*pos_array[i][j][0]+pos_array[im1][j][0])/(s1*s1);
		d2xds2=(pos_array[ip1][j][1]-2*pos_array[i][j][1]+pos_array[im1][j][1])/(s1*s1);
		//in pseudo 3d shell case, the metric is a 2x2 matrix, the cijk which would have "n" terms are zero and the rest potentially non zero
		s2=0;	
		//[0]=sss
		christoffel_array[i][j][0]=(-0.5000000000e0 * pow(cos(phi), 0.3e1 + 0.4e1 * d) * d2yds2 * dyds * y * y * pow(d, 0.2e1) + 0.5000000000e0 * pow(cos(phi), 0.4e1 * d + 0.1e1) * d2yds2 * dyds * y * y * pow(d, 0.2e1) - pow(cos(phi), 0.2e1 * d + 0.3e1) * d2xds2 * dxds * y * y * pow(d, 0.2e1) + pow(cos(phi), 0.2e1 * d + 0.1e1) * d2xds2 * dxds * y * y * pow(d, 0.2e1) + pow(cos(phi), 0.5e1 + 0.2e1 * d) * pow(Lz, 0.2e1) * d2yds2 * dyds + pow(cos(phi), 0.5e1) * pow(Lz, 0.2e1) * d2xds2 * dxds + pow(cos(phi), 0.3e1 + 0.4e1 * d) * dyds * pow(y, 0.3e1) * pow(d, 0.4e1) - 0.2e1 * pow(cos(phi), 0.4e1 * d + 0.1e1) * dyds * pow(y, 0.3e1) * pow(d, 0.4e1) + pow(cos(phi), 0.4e1 * d - 0.1e1) * dyds * pow(y, 0.3e1) * pow(d, 0.4e1) - pow(cos(phi), 0.2e1 + 0.4e1 * d) * dyds * dyds * y * y * sin(phi) * pow(d, 0.3e1) + pow(cos(phi), 0.4e1 * d) * dyds * dyds * y * y * sin(phi) * pow(d, 0.3e1) + pow(cos(phi), 0.4e1 * d + 0.1e1) * dyds * pow(y, 0.3e1) * pow(d, 0.3e1) - pow(cos(phi), 0.4e1 * d - 0.1e1) * dyds * pow(y, 0.3e1) * pow(d, 0.3e1) + 0.5000000000e0 * pow(cos(phi), 0.3e1 + 0.4e1 * d) * pow(dyds, 0.3e1) * y * pow(d, 0.2e1) - 0.5000000000e0 * pow(cos(phi), 0.4e1 * d + 0.1e1) * pow(dyds, 0.3e1) * y * pow(d, 0.2e1) - pow(cos(phi), 0.5e1 + 0.2e1 * d) * pow(Lz, 0.2e1) * dyds * y * d + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1 * d + 0.3e1) * dyds * y * d) / (-pow(cos(phi), 0.2e1 * d + 0.2e1) * dxds * dxds * y * y * pow(d, 0.2e1) + dyds * dyds * pow(cos(phi), 0.2e1 * d + 0.4e1) * pow(Lz, 0.2e1) + pow(cos(phi), 0.2e1 * d) * dxds * dxds * y * y * pow(d, 0.2e1) + pow(Lz, 0.2e1) * dxds * dxds * pow(cos(phi), 0.4e1)) / cos(phi);

		//[1]=s_sphi=s_phis
		christoffel_array[i][j][1]=-0.10e1 * (y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1)) * dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * (double) d * sin(phi) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1)) / cos(phi) + 0.5e0 * dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) d * sin(phi) * (0.2e1 * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) * dyds + dyds * pow(cos(phi), (double) d) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) * y * pow(cos(phi), (double) (d - 1)) / cos(phi) + dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) (d - 1) * pow(sin(phi), 0.2e1) * (double) d / cos(phi) - dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) d * cos(phi) + 0.2e1 * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.3e1) * (double) (d - 1) / cos(phi) - 0.2e1 * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * sin(phi) * cos(phi) + 0.2e1 * pow(Lz, 0.2e1) * cos(phi) * sin(phi)) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1));

		//[2]=^s_sn=^s_ns
		christoffel_array[i][j][2]=0;
		//[3]=^s_phiphi,
		christoffel_array[i][j][3]=0.5e0 * (y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) + pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1)) * (0.2e1 * dyds * pow(cos(phi), (double) d) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) * y * pow(cos(phi), (double) (d - 1)) / cos(phi) + 0.2e1 * dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) (d - 1) * pow(sin(phi), 0.2e1) * (double) d / cos(phi) - 0.2e1 * dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) d * cos(phi) - 0.2e1 * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1) * dyds) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1)) + 0.5e0 * dyds * pow(cos(phi), (double) d) * y * pow(cos(phi), (double) (d - 1)) * (double) d * sin(phi) * (-0.2e1 * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.3e1) * (double) (d - 1) / cos(phi) + 0.2e1 * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * sin(phi) * cos(phi) - 0.2e1 * pow(Lz, 0.2e1) * cos(phi) * sin(phi)) / (dyds * dyds * pow(pow(cos(phi), (double) d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), (double) (d - 1)), 0.2e1) * (double) (int) pow((double) d, (double) 2) * pow(sin(phi), 0.2e1));

		 //[4]=^s1_ns2=s2n,//[5]=^s1_nn //[6]=^n_s1s1,[7]=^n_s1s2=^n_s2s1,[8]=^n_s1n=^n_ns1, [9]=^n_s2s2,[10]=^n_s2n,[11]=^n_nn
		christoffel_array[i][j][4]=0;
		christoffel_array[i][j][5]=0;
		christoffel_array[i][j][6]=0;
		christoffel_array[i][j][7]=0;
		christoffel_array[i][j][8]=0;
		christoffel_array[i][j][9]=0;
		christoffel_array[i][j][10]=0;
		christoffel_array[i][j][11]=0;

		//[0]=sss//[1]=s_sphi=s_phis//[3]=^s_phiphi,
		christoffel_array[i][j][12]= (0.5000000000e0 * pow(cos(phi), 0.2e1 + 0.4e1 * d) * d2yds2 * dyds * dyds * y * d + pow(cos(phi), 0.2e1 * d + 0.2e1) * d2xds2 * dxds * dyds * y * d - pow(cos(phi), 0.2e1 + 0.4e1 * d) * dyds * dyds * y * y * pow(d, 0.3e1) + pow(cos(phi), 0.4e1 * d) * dyds * dyds * y * y * pow(d, 0.3e1) + pow(cos(phi), 0.4e1 * d + 0.1e1) * pow(dyds, 0.3e1) * y * sin(phi) * pow(d, 0.2e1) - dyds * dyds * y * y * pow(cos(phi), 0.4e1 * d) * pow(d, 0.2e1) - pow(cos(phi), 0.2e1 * d + 0.2e1) * dxds * dxds * y * y * pow(d, 0.3e1) + dxds * dxds * pow(cos(phi), 0.2e1 * d) * y * y * pow(d, 0.3e1) + pow(cos(phi), 0.2e1 * d + 0.1e1) * dxds * dxds * dyds * y * sin(phi) * pow(d, 0.2e1) - pow(cos(phi), 0.2e1 * d) * dxds * dxds * y * y * pow(d, 0.2e1) - 0.5000000000e0 * pow(cos(phi), 0.2e1 + 0.4e1 * d) * pow(dyds, 0.4e1) * d + dyds * dyds * pow(cos(phi), 0.2e1 * d + 0.4e1) * pow(Lz, 0.2e1) - 0.5000000000e0 * pow(cos(phi), 0.2e1 * d + 0.2e1) * d2yds2 * dxds * dxds * y * d - 0.5000000000e0 * pow(cos(phi), 0.2e1 * d + 0.2e1) * dxds * dxds * dyds * dyds * d + pow(Lz, 0.2e1) * dxds * dxds * pow(cos(phi), 0.4e1)) * sin(phi) / cos(phi) / (-pow(cos(phi), 0.2e1 * d + 0.2e1) * dxds * dxds * y * y * pow(d, 0.2e1) + dyds * dyds * pow(cos(phi), 0.2e1 * d + 0.4e1) * pow(Lz, 0.2e1) + pow(cos(phi), 0.2e1 * d) * dxds * dxds * y * y * pow(d, 0.2e1) + pow(Lz, 0.2e1) * dxds * dxds * pow(cos(phi), 0.4e1));
;
		christoffel_array[i][j][13]=-0.10e1 * pow(dyds, 0.3e1) * pow(pow(cos(phi), d), 0.3e1) * y * pow(cos(phi), d - 0.1e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1)) / cos(phi) + 0.5e0 * (dxds * dxds + dyds * dyds * pow(pow(cos(phi), d), 0.2e1)) * (0.2e1 * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * dyds + dyds * pow(cos(phi), d) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * y * pow(cos(phi), d - 0.1e1) / cos(phi) + dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * (d - 0.1e1) * pow(sin(phi), 0.2e1) * d / cos(phi) - dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * cos(phi) + 0.2e1 * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.3e1) * (d - 0.1e1) / cos(phi) - 0.2e1 * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * sin(phi) * cos(phi) + 0.2e1 * pow(Lz, 0.2e1) * cos(phi) * sin(phi)) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));
;
		christoffel_array[i][j][14]=0;
		christoffel_array[i][j][15]=0.5e0 * dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * sin(phi) * (0.2e1 * dyds * pow(cos(phi), d) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * y * pow(cos(phi), d - 0.1e1) / cos(phi) + 0.2e1 * dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * (d - 0.1e1) * pow(sin(phi), 0.2e1) * d / cos(phi) - 0.2e1 * dyds * pow(cos(phi), d) * y * pow(cos(phi), d - 0.1e1) * d * cos(phi) - 0.2e1 * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1) * dyds) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1)) + 0.5e0 * (dxds * dxds + dyds * dyds * pow(pow(cos(phi), d), 0.2e1)) * (-0.2e1 * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.3e1) * (d - 0.1e1) / cos(phi) + 0.2e1 * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * sin(phi) * cos(phi) - 0.2e1 * pow(Lz, 0.2e1) * cos(phi) * sin(phi)) / (dyds * dyds * pow(pow(cos(phi), d), 0.2e1) * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * pow(Lz, 0.2e1) * pow(cos(phi), 0.2e1) + dxds * dxds * y * y * pow(pow(cos(phi), d - 0.1e1), 0.2e1) * pow(d, 0.2e1) * pow(sin(phi), 0.2e1));

		christoffel_array[i][j][16]=0;
		christoffel_array[i][j][17]=0;

		//printf("metric: %f %f %f %f %f %f inverse:%f %f %f %f %f %f\n",metric[i][j][0],metric[i][j][1],metric[i][j][2],metric[i][j][3],metric[i][j][4],metric[i][j][5],inverse_metric[i][j][0],inverse_metric[i][j][1],inverse_metric[i][j][2],inverse_metric[i][j][3],inverse_metric[i][j][4],inverse_metric[i][j][5]);	
		//printf("cijk: %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\n",christoffel_array[i][j][0],christoffel_array[i][j][1],christoffel_array[i][j][2],christoffel_array[i][j][3],christoffel_array[i][j][4],christoffel_array[i][j][5],christoffel_array[i][j][6],christoffel_array[i][j][7],christoffel_array[i][j][8],christoffel_array[i][j][9],christoffel_array[i][j][10],christoffel_array[i][j][11],christoffel_array[i][j][12],christoffel_array[i][j][13],christoffel_array[i][j][14],christoffel_array[i][j][15],christoffel_array[i][j][16],christoffel_array[i][j][17]);
		return;
	}
	else if(dimensions==3 && straintype=='o')
	{
		//solid mode, r,theta,s2 rather than s1,n,s2
		//since coordinates are known in xy plane to be 2d polar (r,theta), can just write those directly.
		//s2 is still found numerically.

		//[0]=^r_rr//[1]=^r_rs2=^r_s2r//[2]=^r_rt=^r_tr//[3]=^r_s2s2,//[4]=^r_ts2=s2t,//[5]=^r_tt 
		//[6]=^t_rr,[7]=^t_rs2=^t_s2r,[8]=^t_rt=^t_tr, [9]=^t_s2s2,[10]=^t_s2t,[11]=^t_tt
		//[12]=^s2_rr//[13]=^s2_rs2=^s2_s2r//[14]=^s2_rt=^s2_tr//[15]=^s2_s2s2,//[16]=^s2_ts2=s2t,//[17]=^s2_tt
 
		//to save redefining a bunch of variables, 'n' means theta direction, s1 means radial xy plane, s2 means s2, unchanged from shell case

		//for the r,t only terms, only ^t_rt=^t_tr= 1/r and ^r_tt = -r are non zero
	
		r=sqrt(pos_array[i][j][0]*pos_array[i][j][0]+pos_array[i][j][1]*pos_array[i][j][1]);
		//these terms not involving the s2 direction have the same expression in both (r,t) and (r,t,s2) basis.
		christoffel_array[i][j][0]=0;
		christoffel_array[i][j][2]=0;
		christoffel_array[i][j][5]=-r;

		christoffel_array[i][j][6]=0;
		christoffel_array[i][j][8]=1/r;
		christoffel_array[i][j][11]=0;
		//ds1=dr
		
		//reminder : g_ij=(1,0,grs2),(0,r^2,gthetas2),(grs2,gthetas2,gs2s2) in r,theta, s2 basis.
		//dgrr dr=0
	//printf("test 1\n");
		dgs1s1ds1=0;
		dgs1s2ds1=(metric[ip1][j][1]-metric[im1][j][1])/s1;
		dgs1nds1=0;
		dgs2s2ds1=(metric[ip1][j][3]-metric[im1][j][3])/s1;
		dgs2nds1=(metric[ip1][j][4]-metric[im1][j][4])/s1;
		dgnnds1=(metric[ip1][j][5]-metric[im1][j][5])/s1;
//	printf("test 2\n");
		if(num_zstacks>1)
		{
			dgs1s1ds2=(metric[i][jp1][0]-metric[i][jm1][0])/s2;
			dgs1s2ds2=(metric[i][jp1][1]-metric[i][jm1][1])/s2;
			dgs1nds2=(metric[i][jp1][2]-metric[i][jm1][2])/s2;
			dgs2s2ds2=(metric[i][jp1][3]-metric[i][jm1][3])/s2;
			dgs2nds2=(metric[i][jp1][4]-metric[i][jm1][4])/s2;
			dgnnds2=(metric[i][jp1][5]-metric[i][jm1][5])/s2;
		}
		else
		{
			dgs1s1ds2=0;
			dgs1s2ds2=0;
			dgs1nds2=0;
			dgs2s2ds2=0;
			dgs2nds2=0;
			dgnnds2=0;
		}
	//printf("test 3\n");
		//and now convert these to polar values rather than the (s1,s2,n)
		if(dimensions==3)
		{	//these terms involve the s2 direction. Find them analytically.	
			christoffel_array[i][j][1]=0.5*ginv_s1s1*(dgs1s1ds2)+0.5*ginv_s1n*(dgs2nds1+dgs2nds2-dgs1s2dn)+0.5*ginv_s1s2*(dgs2s2ds1);
			christoffel_array[i][j][3]=0.5*ginv_s1s1*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_s1n*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s1s2*(dgs2s2ds2);
			christoffel_array[i][j][4]=0.5*ginv_s1s1*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_s1n*(dgnnds2)+0.5*ginv_s1s2*(dgs2s2dn);		
			christoffel_array[i][j][7]=0.5*ginv_s1n*(dgs1s1ds2)+0.5*ginv_nn*(dgs2nds1+dgs1nds2-dgs1s2dn)+0.5*ginv_s2n*(dgs2s2ds1);
			christoffel_array[i][j][9]=0.5*ginv_s1n*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_nn*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s2n*(dgs2s2ds2);
			christoffel_array[i][j][10]=0.5*ginv_s1n*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_nn*(dgnnds2)+0.5*ginv_s2n*(dgs2s2dn);
			christoffel_array[i][j][12]=0.5*ginv_s1s2*(dgs1s1ds1)+0.5*ginv_s2n*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_s2s2*(2*dgs1s2ds1-dgs1s1ds2);
			christoffel_array[i][j][13]=0.5*ginv_s1s2*(dgs1s1ds2)+0.5*ginv_s2n*(dgs2nds1+dgs1nds2-dgs1s2dn)+0.5*ginv_s2s2*(dgs2s2ds1);
			christoffel_array[i][j][14]=0.5*ginv_s1s2*(2*dgs1nds1-dgs1s1dn)+0.5*ginv_s2n*(dgnnds1)+0.5*ginv_s2s2*(dgs2nds1+dgs1s2dn-dgs1nds2);
			christoffel_array[i][j][15]=0.5*ginv_s1s2*(2*dgs1s2ds2-dgs2s2dn)+0.5*ginv_s2n*(2*dgs2nds2-dgs2s2dn)+0.5*ginv_s2s2*(dgs2s2ds2);
			christoffel_array[i][j][16]=0.5*ginv_s1s2*(dgs1nds2+dgs1ndn-dgs2nds1)+0.5*ginv_s2n*(dgnnds2)+0.5*ginv_s2s2*(dgs2s2dn);
			christoffel_array[i][j][17]=0.5*ginv_s1s2*(2*dgs1ndn-dgnnds1)+0.5*ginv_s2n*(dgnndn)+0.5*ginv_s2s2*(2*dgs2ndn-dgnnds2);
		}
		else
		{//in 2d case, all the s2 involved symbols should be zero
			christoffel_array[i][j][1]=0;
			christoffel_array[i][j][3]=0;
			christoffel_array[i][j][4]=0;		
			christoffel_array[i][j][7]=0;
			christoffel_array[i][j][9]=0;
			christoffel_array[i][j][10]=0;
			christoffel_array[i][j][12]=0;
			christoffel_array[i][j][13]=0;
			christoffel_array[i][j][14]=0;
			christoffel_array[i][j][15]=0;
			christoffel_array[i][j][16]=0;
			christoffel_array[i][j][17]=0;
		}
		return;
	}
	else if(mode2d=='y' &&dimensions==2)
	{
		//christoffel symbols are all trivially zero, as the metric of this surface is constant everywhere.
		christoffel_array[i][j][0]=0;	
		christoffel_array[i][j][1]=0;
		christoffel_array[i][j][2]=0;
		christoffel_array[i][j][3]=0;
		christoffel_array[i][j][4]=0;		
		christoffel_array[i][j][5]=0;
		christoffel_array[i][j][6]=0;
		christoffel_array[i][j][7]=0;
		christoffel_array[i][j][8]=0;
		christoffel_array[i][j][9]=0;
		christoffel_array[i][j][10]=0;
		christoffel_array[i][j][11]=0;
		christoffel_array[i][j][12]=0;
		christoffel_array[i][j][13]=0;
		christoffel_array[i][j][14]=0;
		christoffel_array[i][j][15]=0;
		christoffel_array[i][j][16]=0;
		christoffel_array[i][j][17]=0;
	}	
	//printf("found chr symbols for i=%d j=%d\n",i,j);
	return;
}

void curvature_tensor_3d(double*** curvature_tensor,double*** shape_array,int shape_points,int num_zstacks)
{	
	//the curvature tensor is a measure of the rate of change of the normal vector on the surface.
	//let [0]=s1s1,[1]=s1s2,[2]=s2s1,[3]=s2s2
	//determine the curavture tensor elements in this function

	double e1x,e2x,e1y,e2y,e1z,e2z,s1,s2,nx,ny,nz,n;
	double e1xip1,e2xip1,e1yip1,e2yip1,e1zip1,e2zip1,nxip1,nyip1,nzip1,nip1;
	double e1xim1,e2xim1,e1yim1,e2yim1,e1zim1,e2zim1,nxim1,nyim1,nzim1,nim1;
	double e1xjp1,e2xjp1,e1yjp1,e2yjp1,e1zjp1,e2zjp1,nxjp1,nyjp1,nzjp1,njp1;
	double e1xjm1,e2xjm1,e1yjm1,e2yjm1,e1zjm1,e2zjm1,nxjm1,nyjm1,nzjm1,njm1;		

	double dnds1x,dnds2x,dnds1y,dnds2y,dnds1z,dnds2z;
	int i,ip1,im1,ip2,im2,j,jp1,jm1,jp2,jm2,jc;
	double dyds,dxds,d2yds2,d2xds2,x,y;
	double phi=0;
	double theta=0,gamma=0;	
	s1=1;
	s2=1;
	//printf("dimensions=%d num stacks =%d volume_mode=%c\n",dimensions,num_zstacks,volume_mode);
	for(j=0;j<num_zstacks;j++)
	{	
		for(i=0;i<shape_points;i++)
		{
			//printf("i=%d j=%d\n",i,j);
			four_nearest_neighbour_points_i(i,&ip1,&im1,&ip2,&im2,shape_points);
			//printf("ip1=%d im1=%d ip2=%d im2=%d\n",ip1,im1,ip2,im2);			
			four_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,&jp2,&jm2,num_zstacks);
			//printf("jp1=%d jm1=%d jp2=%d jm2=%d\n",jp1,jm1,jp2,jm2);
			
			if(dimensions==2)
			{	//for completely 2d mode...
				jc=0;
	
				distance_between_points_3d_array_i(shape_array,i,jc,shape_points,&s1);				
				direction_vectors_i(shape_array,&e1x,&e1y,&e1z,i,jc,shape_points);
				normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);
	
				distance_between_points_3d_array_i(shape_array,ip1,jc,shape_points,&s1);
				direction_vectors_i(shape_array,&e1xip1,&e1yip1,&e1zip1,ip1,jc,shape_points);
				normal_vector(e1xip1,e1yip1,e1zip1,e2xip1,e2yip1,e2zip1,&nx,&ny,&nz,dimensions,num_zstacks);
				nxip1=(e1yip1);
				nyip1=(-e1xip1);
				nzip1=0;
	
				nip1=sqrt(nxip1*nxip1+nyip1*nyip1+nzip1*nzip1);
	
				nxip1= nxip1/nip1;
				nyip1= nyip1/nip1;
				nzip1= nzip1/nip1;

				direction_vectors_i(shape_array,&e1xim1,&e1yim1,&e1zim1,im1,jc,shape_points);		

				nxim1=(e1yim1);
				nyim1=(-e1xim1);
				nzim1=0;
	
				nim1=sqrt(nxim1*nxim1+nyim1*nyim1+nzim1*nzim1);
	
				nxim1= nxim1/nim1;
				nyim1= nyim1/nim1;
				nzim1= nzim1/nim1;
		
				distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
		
				dnds1x=(nxip1-nxim1)/s1;
				dnds1y=(nyip1-nyim1)/s1;
				dnds1z=0;//in 2d mode, always zero...(nzip1-nzim1)/s1;

				curvature_tensor[i][j][0]=dnds1x*e1x+dnds1y*e1y; 
				//printf("i:%d x:%f y:%f curvature=%f\n",i,shape_array[i][jc][0],shape_array[i][jc][1],curvature_tensor[i][j][0]);
				curvature_tensor[i][j][1]=0;//curvature tensor is only 1x1 (i.e. scalar) in 2d case, instead of 2x2 as in 3d or psuedo 3d.
				curvature_tensor[i][j][2]=0;
				curvature_tensor[i][j][3]=0;
			}
			else if(dimensions==3 && num_zstacks==1)
			{
				//[0]=Css [1]=Csz [2]=Czs [3]=Czz
				if(volume_mode=='f')
				{	//fills the channel
					//because s1 and z are perpendicular to each other, n is in the xy plane, hence has no z component and is the same the purely 2d case, except at the constriction sides#

					distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
	//	printf("e1x %f e1y %f e1z %f i=%d jc=%d shapepoints=%d\n",e1x,e1y,e1z,i,jc,shape_points);
					direction_vectors_i(shape_array,&e1x,&e1y,&e1z,i,jc,shape_points);

					nx=e1y;
					ny=-e1x;
					nz=0;
					n=sqrt(nx*nx+ny*ny+nz*nz);
					nx= nx/n;
					ny= ny/n;
					nz= nz/n;

					direction_vectors_i(shape_array,&e1xip1,&e1yip1,&e1zip1,ip1,jc,shape_points);
					nxip1=(e1yip1);
					nyip1=(-e1xip1);
					nzip1=0;

					nip1=sqrt(nxip1*nxip1+nyip1*nyip1+nzip1*nzip1);
					nxip1= nxip1/nip1;
					nyip1= nyip1/nip1;
					nzip1= nzip1/nip1;

					direction_vectors_i(shape_array,&e1xim1,&e1yim1,&e1zim1,im1,jc,shape_points);
					e1zim1=0;//in 3d mode 
					nxim1=(e1yim1);
					nyim1=(-e1xim1);
					nzim1=0;
	
					nim1=sqrt(nxim1*nxim1+nyim1*nyim1+nzim1*nzim1);		
					nxim1= nxim1/nim1;
					nyim1= nyim1/nim1;
					nzim1= nzim1/nim1;

					distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
					dnds1x=(nxip1-nxim1)/s1;
					dnds1y=(nyip1-nyim1)/s1;
					dnds1z=0;//in constant x-y cross section, the z component of the normal vector doesn't change with s1, is always 0.
					
					rotation_angles_3d(shape_array,i,j,&theta,&gamma,shape_points,num_zstacks);

					//printf("costheta=%f sintheta=%f e1x=%f e1y=%f\n",cos(theta),sin(theta),e1x,e1y);
					//printf("nxi=%f s1=%f dn/ds1=(%f,%f) curv 1: %f curv 2:%f  sum=%f radius=%f\n",nx,s1,dnds1x,dnds1y,dnds1x*sin(theta),dnds1y*cos(theta),dnds1x*sin(theta)+dnds1y*cos(theta), sqrt(pow(shape_array[i][j][0],2)+pow(shape_array[i][j][1],2)));
					curvature_tensor[i][j][0]=dnds1x*e1x+dnds1y*e1y; //(dn/ds1 dot prod e_s1)
					curvature_tensor[i][j][1]=0;	//due to symm considerations.					
					curvature_tensor[i][j][2]=0;
					curvature_tensor[i][j][3]=0;
					
				}
				if(volume_mode=='s')
				{
					//symmetric shape in x (along channel) and z (out of plane) directions	

				
				distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
				direction_vectors_i(shape_array,&e1x,&e1y,&e1z,i,jc,shape_points);
//because s1 and z are perpendicular to each other, n is in the xy plane, hence has no z component and is the same the purely 2d case.
				nx=e1y;
				ny=-e1x;
				nz=0;
				n=sqrt(nx*nx+ny*ny+nz*nz);
				nx= nx/n;
				ny= ny/n;
				nz= nz/n;

				direction_vectors_i(shape_array,&e1xip1,&e1yip1,&e1zip1,ip1,jc,shape_points);

				nxip1=(e1yip1);
				nyip1=(-e1xip1);
				nzip1=0;

				nip1=sqrt(nxip1*nxip1+nyip1*nyip1+nzip1*nzip1);

				nxip1= nxip1/nip1;
				nyip1= nyip1/nip1;
				nzip1= nzip1/nip1;
	
				direction_vectors_i(shape_array,&e1xim1,&e1yim1,&e1zim1,im1,jc,shape_points);
				e1zim1=0;//in 2d mode, always zero...(shape_array[i][j][2]-shape_array[im2][j][2])/s1; //nb this should always be zero, since z is held constant over varied i.
				nxim1=(e1yim1);
				nyim1=(-e1xim1);
				nzim1=0;

				nim1=sqrt(nxim1*nxim1+nyim1*nyim1+nzim1*nzim1);
	
				nxim1= nxim1/nim1;
				nyim1= nyim1/nim1;
				nzim1= nzim1/nim1;

				distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
		
				dnds1x=(nxip1-nxim1)/s1;
				dnds1y=(nyip1-nyim1)/s1;
				dnds1z=0;//in constant x-y cross section, the z component of the normal vector doesn't change with s1, is always 0.


				curvature_tensor[i][j][0]=dnds1x*e1x+dnds1y*e1y;
				curvature_tensor[i][j][1]=0;			
				curvature_tensor[i][j][2]=0;
				curvature_tensor[i][j][3]=0;	//due to symm considerations.
				}				
			}
			else if(dimensions==3 && num_zstacks>1)
			{	
				double s1ip1,s1im1,s1jp1,s1jm1;
				double s2ip1,s2im1,s2jp1,s2jm1;
				//find normal vector at point i,j
				distance_between_points_3d_array_i(shape_array,i,jc,shape_points,&s1);
				distance_between_points_3d_array_j(shape_array,i,jc,num_zstacks,&s2);
				direction_vectors_i(shape_array,&e1x,&e1y,&e1z,i,jc,shape_points);
				direction_vectors_j(shape_array,&e2x,&e2y,&e2z,i,jc,num_zstacks);

				nx=(e1y*e2z-e2y*e1z);
				ny=(-e1x*e2z+e1z*e2x);
				nz=(e1x*e2y-e1y*e2x);
				n=sqrt(nx*nx+ny*ny+nz*nz);
				nx= nx/n;
				ny= ny/n;
				nz= nz/n;

				//find normal vector at point ip1,j
				distance_between_points_3d_array_i(shape_array,ip1,jc,shape_points,&s1ip1);
				distance_between_points_3d_array_j(shape_array,ip1,jc,num_zstacks,&s2ip1);
				direction_vectors_i(shape_array,&e1xip1,&e1yip1,&e1zip1,ip1,jc,shape_points);
				direction_vectors_j(shape_array,&e2xip1,&e2yip1,&e2zip1,ip1,jc,num_zstacks);

			
				nxip1=(e1yip1*e2zip1-e2yip1*e1zip1);
				nyip1=(-e1xip1*e2zip1+e1zip1*e2xip1);
				nzip1=(e1xip1*e2yip1-e1yip1*e2xip1);
				nip1=sqrt(nxip1*nxip1+nyip1*nyip1+nzip1*nzip1);
				nxip1= nxip1/nip1;
				nyip1= nyip1/nip1;
				nzip1= nzip1/nip1;

				//find normal vector at point im1,j
				distance_between_points_3d_array_i(shape_array,im1,jc,shape_points,&s1im1);
				distance_between_points_3d_array_j(shape_array,im1,jc,num_zstacks,&s2im1);
				direction_vectors_i(shape_array,&e1xim1,&e1yim1,&e1zim1,im1,jc,shape_points);
				direction_vectors_j(shape_array,&e2xim1,&e2yim1,&e2zim1,im1,jc,num_zstacks);
				
				
				nxim1=(e1yim1*e2zim1-e2yim1*e1zim1);
				nyim1=(-e1xim1*e2zim1+e1zim1*e2xim1);
				nzim1=(e1xim1*e2yim1-e1yim1*e2xim1);
				nim1=sqrt(nxim1*nxim1+nyim1*nyim1+nzim1*nzim1);
				nxim1= nxim1/nim1;
				nyim1= nyim1/nim1;
				nzim1= nzim1/nim1;
				
				//find normal vector at point i,jp1
				distance_between_points_3d_array_i(shape_array,i,jp1,shape_points,&s1jp1);
				distance_between_points_3d_array_j(shape_array,i,jp1,num_zstacks,&s2jp1);
				direction_vectors_i(shape_array,&e1xjp1,&e1yjp1,&e1zjp1,i,jp1,shape_points);
				direction_vectors_j(shape_array,&e2xjp1,&e2yjp1,&e2zjp1,i,jp1,num_zstacks);

				
				nxjp1=(e1yjp1*e2zjp1-e2yjp1*e1zjp1);
				nyjp1=(-e1xjp1*e2zjp1+e1zjp1*e2xjp1);
				nzjp1=(e1xjp1*e2yjp1-e1yjp1*e2xjp1);

				njp1=sqrt(nxjp1*nxjp1+nyjp1*nyjp1+nzjp1*nzjp1);
				nxjp1= nxjp1/njp1;
				nyjp1= nyjp1/njp1;
				nzjp1= nzjp1/njp1;

				
				//find normal vector at point i,jm1
				distance_between_points_3d_array_i(shape_array,i,jm1,shape_points,&s1jm1);
				distance_between_points_3d_array_j(shape_array,i,jm1,num_zstacks,&s2jm1);
				direction_vectors_i(shape_array,&e1xjm1,&e1yjm1,&e1zjm1,i,jm1,shape_points);
				direction_vectors_j(shape_array,&e2xjm1,&e2yjm1,&e2zjm1,i,jm1,num_zstacks);

				
				nxjm1=(e1yjm1*e2zjm1-e2yjm1*e1zjm1);
				nyjm1=(-e1xjm1*e2zjm1+e1zjm1*e2xjm1);
				nzjm1=(e1xjm1*e2yjm1-e1yjm1*e2xjm1);
				njm1=sqrt(nxjm1*nxjm1+nyjm1*nyjm1+nzjm1*nzjm1);
				nxjm1= nxjm1/njm1;
				nyjm1= nyjm1/njm1;
				nzjm1= nzjm1/njm1;

				distance_between_points_3d_array_i(shape_array,i,j,shape_points,&s1);
				distance_between_points_3d_array_j(shape_array,i,j,num_zstacks,&s2);
				//s1 and s2 are the distances between points ip1/im1 and jp1/jm1 for the respective directions

				if(j==0)
				{//use a forwards difference method on s2 - this is already taken in to account in value of s2
					dnds1x=(nxip1-nxim1)/s1;
					dnds1y=(nyip1-nyim1)/s1;
					dnds1z=(nzip1-nzim1)/s1;
					dnds2x=(nxjp1-nx)/s2;
					dnds2y=(nyjp1-ny)/s2;
					dnds2z=(nzjp1-nz)/s2;
				}
				else if(j==(num_zstacks-1))
				{ 	//use a backwards difference method on s2 - this is already taken in to account in value of s2
					dnds1x=(nxip1-nxim1)/s1;
					dnds1y=(nyip1-nyim1)/s1;
					dnds1z=(nzip1-nzim1)/s1;
					dnds2x=(nx-nxjm1)/s2;
					dnds2y=(ny-nyjm1)/s2;
					dnds2z=(nz-nzjm1)/s2;
				}
				else 
				{
					dnds1x=(nxip1-nxim1)/s1;
					dnds1y=(nyip1-nyim1)/s1;
					dnds1z=(nzip1-nzim1)/s1;
					dnds2x=(nxjp1-nxjm1)/s2;
					dnds2y=(nyjp1-nyjm1)/s2;
					dnds2z=(nzjp1-nzjm1)/s2;
				}
				
				curvature_tensor[i][j][0]=dnds1x*e1x+dnds1y*e1y+dnds1z*e1z;//s1s1
				curvature_tensor[i][j][1]=dnds2x*e1x+dnds2y*e1y+dnds2z*e1z;//s2s1
				curvature_tensor[i][j][2]=dnds1x*e2x+dnds1y*e2y+dnds1z*e2z;//s1s2
				curvature_tensor[i][j][3]=dnds2x*e2x+dnds2y*e2y+dnds2z*e2z;//s2s2								
		
			}
			else
			{
				printf("no curvature mode selected :( \n"); 
				curvature_tensor[i][j][0]=0;
				curvature_tensor[i][j][1]=0;
				curvature_tensor[i][j][2]=0;
				curvature_tensor[i][j][3]=0;
			}
		}

	}
return;
}

void rotate_strain_to_cart(double*** pos_array,double*** strain_array,double us1s1,double us1s2,double us1n,double us2s2,double us2n,double unn,int i,int j,double theta,double gamma,int shape_points,int num_zstacks)
{	//for shell model, we rotate the strain back from (s1,n,s2) basis to cartesian
	int ip1,im1,jp1,jm1,jc;

	if(dimensions==2) gamma=0;

	two_nearest_neighbour_points_i(i,&ip1,&im1,shape_points);
	
	//printf("%f %f %f\n",pos_array[i][j][0],pos_array[i][j][1],theta);
	if(dimensions==3)
	{	
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	}

	double sg=sin(gamma);
	double st=sin(theta);
	double cg=cos(gamma);
	double ct=cos(theta);

	//this dx,dy,dz are the line along s1 which is rotated about by angle gamma. theta is a rotation about z axis
	double dx=pos_array[ip1][j][0]-pos_array[im1][j][0];
	double dy=pos_array[ip1][j][1]-pos_array[im1][j][1];
	double dz=pos_array[ip1][j][2]-pos_array[im1][j][2];

	if(mode2d=='c'|| dimensions==2) dz=0;

	double normalisation=sqrt(dx*dx+dy*dy+dz*dz);
	dx=dx/normalisation;
	dy=dy/normalisation;
	dz=dz/normalisation;
	//See maple sheet "rotation2.mw" on dropbox for the code generated below/ [0]=uxx [1]= uxy [2] = uyy [3] = uxz [4] = uyz [5]= uzz

	
	strain_array[i][j][0]=((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * ct + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * st) * (dx * dx + (dy * dy + dz * dz) * cg) + ((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * st - (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * ct) * (dx * dy * (1 - cg) - dz * sg) + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s2 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us2n + (dx * dz * (1 - cg) + dy * sg) * us2s2) * (dx * dz * (1 - cg) + dy * sg);
				

 	//uxy
	strain_array[i][j][1]=((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * ct + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * st) * (dx * dy * (1 - cg) + dz * sg) + ((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * st - (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * ct) * (dy * dy + (dx * dx + dz * dz) * cg) + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s2 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us2n + (dx * dz * (1 - cg) + dy * sg) * us2s2) * (dy * dz * (1 - cg) - dx * sg);

	//uyy
	
	strain_array[i][j][2]= ((((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s1 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us1n + (dy * dz * (1 - cg) - dx * sg) * us1s2) * ct + (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1n + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * unn + (dy * dz * (1 - cg) - dx * sg) * us2n) * st) * (dx * dy * (1 - cg) + dz * sg) + ((((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s1 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us1n + (dy * dz * (1 - cg) - dx * sg) * us1s2) * st - (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1n + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * unn + (dy * dz * (1 - cg) - dx * sg) * us2n) * ct) * (dy * dy + (dx * dx + dz * dz) * cg) + (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s2 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us2n + (dy * dz * (1 - cg) - dx * sg) * us2s2) * (dy * dz * (1 - cg) - dx * sg);

	if(mode2d=='c'&& dimensions==2)//completely 2d mode, i.e. no z direction strain terms
	{
		strain_array[i][j][3]=0;
		strain_array[i][j][4]=0;
		strain_array[i][j][5]=0;
	
	return;
	}
	else
	{	
//uxz
	
	strain_array[i][j][3]=((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * ct + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * st) * (dx * dz * (1 - cg) - dy * sg) + ((((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s1 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us1n + (dx * dz * (1 - cg) + dy * sg) * us1s2) * st - (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1n + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * unn + (dx * dz * (1 - cg) + dy * sg) * us2n) * ct) * (dy * dz * (1 - cg) + dx * sg) + (((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) - dz * sg) * st) * us1s2 + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) - dz * sg) * ct) * us2n + (dx * dz * (1 - cg) + dy * sg) * us2s2) * (dz * dz + (dx * dx + dy * dy) * cg);

	//uyz
	
	strain_array[i][j][4]=((((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s1 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us1n + (dy * dz * (1 - cg) - dx * sg) * us1s2) * ct + (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1n + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * unn + (dy * dz * (1 - cg) - dx * sg) * us2n) * st) * (dx * dz * (1 - cg) - dy * sg) + ((((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s1 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us1n + (dy * dz * (1 - cg) - dx * sg) * us1s2) * st - (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1n + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * unn + (dy * dz * (1 - cg) - dx * sg) * us2n) * ct) * (dy * dz * (1 - cg) + dx * sg) + (((dx * dy * (1 - cg) + dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * us1s2 + ((dx * dy * (1 - cg) + dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * us2n + (dy * dz * (1 - cg) - dx * sg) * us2s2) * (dz * dz + (dx * dx + dy * dy) * cg);

	//uzz
		
	strain_array[i][j][5]=((((dx * dz * (1 - cg) - dy * sg) * ct + (dy * dz * (1 - cg) + dx * sg) * st) * us1s1 + ((dx * dz * (1 - cg) - dy * sg) * st - (dy * dz * (1 - cg) + dx * sg) * ct) * us1n + (dz * dz + (dx * dx + dy * dy) * cg) * us1s2) * ct + (((dx * dz * (1 - cg) - dy * sg) * ct + (dy * dz * (1 - cg) + dx * sg) * st) * us1n + ((dx * dz * (1 - cg) - dy * sg) * st - (dy * dz * (1 - cg) + dx * sg) * ct) * unn + (dz * dz + (dx * dx + dy * dy) * cg) * us2n) * st) * (dx * dz * (1 - cg) - dy * sg) + ((((dx * dz * (1 - cg) - dy * sg) * ct + (dy * dz * (1 - cg) + dx * sg) * st) * us1s1 + ((dx * dz * (1 - cg) - dy * sg) * st - (dy * dz * (1 - cg) + dx * sg) * ct) * us1n + (dz * dz + (dx * dx + dy * dy) * cg) * us1s2) * st - (((dx * dz * (1 - cg) - dy * sg) * ct + (dy * dz * (1 - cg) + dx * sg) * st) * us1n + ((dx * dz * (1 - cg) - dy * sg) * st - (dy * dz * (1 - cg) + dx * sg) * ct) * unn + (dz * dz + (dx * dx + dy * dy) * cg) * us2n) * ct) * (dy * dz * (1 - cg) + dx * sg) + (((dx * dz * (1 - cg) - dy * sg) * ct + (dy * dz * (1 - cg) + dx * sg) * st) * us1s2 + ((dx * dz * (1 - cg) - dy * sg) * st - (dy * dz * (1 - cg) + dx * sg) * ct) * us2n + (dz * dz + (dx * dx + dy * dy) * cg) * us2s2) * (dz * dz + (dx * dx + dy * dy) * cg);

	}

	return;
}

void rotation_angles_3d(double*** pos_array,int i,int j,double* theta,double* phi,int num_xypoints,int num_zstacks)
{
	//given the point i,j on the array pos_array, returns the angles theta (between y axis and s1) and phi (between z axis and s2) to rotate strains and deformations by at those points.
	double dxi,dyi,dzi,dxj,dyj,dzj;
	double radius_jm1,radius_jp1;
	int ip1,im1,jp1,jm1,jc;

	two_nearest_neighbour_points_i(i,&ip1,&im1,num_xypoints);
	//printf("i=%d ip1=%d im1=%d\n",i,ip1,im1);
	dxi=pos_array[ip1][j][0]-pos_array[im1][j][0];
	dyi=pos_array[ip1][j][1]-pos_array[im1][j][1];
	dzi=pos_array[ip1][j][2]-pos_array[im1][j][2];
	*theta=atan2(dyi,dxi);
	
	if(dimensions==3 && num_zstacks>1)
	{
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);

		dxj=pos_array[i][jp1][0]-pos_array[i][jm1][0];
		dyj=pos_array[i][jp1][1]-pos_array[i][jm1][1];
		dzj=pos_array[i][jp1][2]-pos_array[i][jm1][2];
	
		//phi is the angle to rotate z on to s2.
		//check orientation by seeing whether "radius" in xy plane increases or decreases along s2, 
		radius_jp1=sqrt(pos_array[i][jp1][0]*pos_array[i][jp1][0]+pos_array[i][jp1][1]*pos_array[i][jp1][1]);
		radius_jm1=sqrt(pos_array[i][jm1][0]*pos_array[i][jm1][0]+pos_array[i][jm1][1]*pos_array[i][jm1][1]);
		
		*phi=atan2(sqrt(dxj*dxj+dyj*dyj),dzj);//M_PI/2.0-atan2(sqrt(dxj*dxj+dyj*dyj),dzj);
		//*phi=0;
		//printf("i=%d j=%d x=%f y=%f phi=%f dr=%f dz=%f\n",i,j,pos_array[i][j][0],pos_array[i][j][1],*phi,sqrt(dxj*dxj+dyj*dyj),dzj);
		
	}
	else *phi=0; // if this is ever not a surface with zero curvature in the out of plane direction for 2d data, this won't be appropriate!
	return;

}

void rotate_vector_3d(double* us1,double* un,double* us2,double theta, double gamma,int i, int j, int num_xypoints, double*** pos_array,double*** deformation_array,int num_zstacks)
{	//this function rotates a vector (ux,uy,uz) to the (us1,un,us2) basis where theta is the angle between 

	rotation_angles_3d(pos_array,i,j,&theta,&gamma,num_xypoints,num_zstacks);
//	if(dimensions==2 || num_zstacks==1) gamma=0;

	double ct=cos(theta);
	double st=sin(theta);
	double cg=cos(gamma);
	double sg=sin(gamma);
	double dx,dy,dz;//the line elements of the line we rotate around.
	double normalisation;
	int ip1,im1,jp1,jm1,jc;
	//direction is an integer showing which direciton to use

	two_nearest_neighbour_points_i(i,&ip1,&im1,num_xypoints);

	//note don't need the third dimension here, as the line rotated about is always in the xy plane (along s1)
	if(dimensions==3 && num_zstacks>1)
	{
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	}
	//	printf("i=%d ip1=%d im1=%d\n",i,ip1,im1);
	//(dx,dy,dz) is the vector rotated around by gamma.
	dx=pos_array[ip1][j][0]-pos_array[im1][j][0];
	dy=pos_array[ip1][j][1]-pos_array[im1][j][1];
	dz=pos_array[ip1][j][2]-pos_array[im1][j][2];
	normalisation=sqrt(dx*dx+dy*dy+dz*dz);
	dx=dx/normalisation;
	dy=dy/normalisation;
	dz=dz/normalisation;


	 *us1=((dx * dx + (dy * dy + dz * dz) * cg) * ct + (dx * dy * (1 - cg) + dz * sg) * st) * deformation_array[i][j][0] + ((dx * dx + (dy * dy + dz * dz) * cg) * st - (dx * dy * (1 - cg) + dz * sg) * ct) * deformation_array[i][j][1] + (dx * dz * (1 - cg) - dy * sg) * deformation_array[i][j][2];

	*un=((dx * dy * (1 - cg) - dz * sg) * ct + (dy * dy + (dx * dx + dz * dz) * cg) * st) * deformation_array[i][j][0] + ((dx * dy * (1 - cg) - dz * sg) * st - (dy * dy + (dx * dx + dz * dz) * cg) * ct) * deformation_array[i][j][1] + (dy * dz * (1 - cg) + dx * sg) * deformation_array[i][j][2];

	*us2=((dx * dz * (1 - cg) + dy * sg) * ct + (dy * dz * (1 - cg) - dx * sg) * st) * deformation_array[i][j][0] + ((dx * dz * (1 - cg) + dy * sg) * st - (dy * dz * (1 - cg) - dx * sg) * ct) * deformation_array[i][j][1] + (dz * dz + (dx * dx + dy * dy) * cg) * deformation_array[i][j][2];
return;

}

double traction_3d(double*** pos_array,double*** stress_array,int i,int j,int nrows,int num_zstacks, double* Fx, double* Fy,double* Fz, double* nx, double* ny,double* nz)
{
	double Nx,Ny,Nz=0,normalise;//components of normal vector
	int ip1,im1,jp1,jm1,jc;
	//given the point i,j on the array pos_array, returns the angles theta (between x axis and s1) and gamma (between z axis and s2) to rotate strains and deformations by at those points.
	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);

	if(dimensions==3)
	{
	 	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	}


	double s1,s2,n;

	double e1x,e1y,e1z,e2x,e2y,e2z;
	//elements of pos array are [0]= x coord, [1]=y coord [2]=z

	direction_vectors_i(pos_array,&e1x,&e1y,&e1z,i,j,nrows);
	if(dimensions==3)
	{
		direction_vectors_j(pos_array,&e2x,&e2y,&e2z,i,jm1,num_zstacks);	
	}
	
	//defined components of outward normal vector in appropriate dimensions
	if(dimensions==2)//in 2d case, or psuedo 3d case, 
	{
	*nx=e1y;
	*ny=-e1x;
	*nz=0;
	n=sqrt(*nx * *nx+*ny * *ny+*nz * *nz);
	}
	else if(dimensions==3)
	{
	*nx=e1y*e2z-e1z*e2y;
	*ny=-e1x*e2z+e1z*e2x;	
	*nz=e1x*e2y-e1y*e2x;
	n=sqrt(*nx * *nx+*ny * *ny+*nz * *nz);

	}

	//printf("%f %f %f %f %f %f\n",e1x,e1y,e1z,e2x,e2y,e2z);
	
	*nx= *nx/n;
	*ny= *ny/n;
	*nz= *nz/n;
	Nx=*nx;
	Ny=*ny;
	Nz=*nz;

	//0,1,2,3,4,5 elements are respectively xx,xy,yy,xz,yz,zz components in both stress and strain
	if(dimensions==3||(dimensions==2 && mode2d=='p'))	
	{
	*Fx= Nx*stress_array[i][j][0]+Ny*stress_array[i][j][1]+Nz*stress_array[i][j][3];
	*Fy= Nx*stress_array[i][j][1]+Ny*stress_array[i][j][2]+Nz*stress_array[i][j][4];
	*Fz= Nx*stress_array[i][j][3]+Ny*stress_array[i][j][4]+Nz*stress_array[i][j][5];

	}
	else if (dimensions==2 && mode2d=='c')
	{

	*Fx= Nx*stress_array[i][j][0]+Ny*stress_array[i][j][1];
	*Fy= Nx*stress_array[i][j][1]+Ny*stress_array[i][j][2];
	*Fz= 0;
	}
	return 0;
}

void initial_deformation_3d(double*** source_array,double*** target_array,double*** deformation_array,double*** christoffel_array,double***metric,double*** inverse_metric,double*** curvature_tensor,int* source_pointscount,int *target_pointcalc,int num_zstacks,int refstack, double entry_x,double exit_x)
{
	 //code that initialises the field.
	
	//each stack should already be remeshed to have the same number of z stacks and the same number of points on each zstack by mesh_3d_z and mesh_3d_xy respectively before running this function.
	//go through each possible deformation field and find the minimal energy initial deformation.
	//if you assume that each the order of points is kept, there are only num_xypoint initial possible deformation fields for each stack.

	int i,j,k,l,m;
	double energy_compare=0;
	double smallest_energy=0;
	double ***dummy_deformation;
	array_alloc_3d_dbl(&dummy_deformation,source_pointscount[refstack],num_zstacks,3);
	double ***dummy_strain;
	array_alloc_3d_dbl(&dummy_strain,source_pointscount[refstack],num_zstacks,6);
	double ***dummy_stress;
	array_alloc_3d_dbl(&dummy_stress,source_pointscount[refstack],num_zstacks,6);

	int* ishift;
	array_alloc_1d_int(&ishift,num_zstacks);

	for(j=0;j<num_zstacks;j++)
	{
		ishift[j]=0;
	}

	int* min_energy_ishift;//the ishift where the energy of the deformation is minimal for each stack.
	array_alloc_1d_int(&min_energy_ishift,num_zstacks);

	for(i=0;i<num_zstacks;i++)
		min_energy_ishift[i]=0;

	//first get a deformation field where the stacks are all assigned by going through all possible combinations of deformation fields between the pixels on the meshes and 
		
	int number_of_iterations=0;
	for(m=0;m<num_zstacks;m++)
	{	printf("finding initial deformation of stack %d\n",m);
		for(ishift[m]=0;ishift[m]<source_pointscount[refstack];ishift[m]++)
		{	
			for(j=0;j<num_zstacks;j++)
			{
				for(i=0;i<source_pointscount[refstack];i++)	
				{	
					if((i+ishift[m])<source_pointscount[refstack]) { k=i+ishift[m];}
					else {k=i+ishift[m]-source_pointscount[refstack];}
				
					dummy_deformation[i][j][0]=target_array[i][j][0]-source_array[k][j][0];
					dummy_deformation[i][j][1]=target_array[i][j][1]-source_array[k][j][1];
					dummy_deformation[i][j][2]=target_array[i][j][2]-source_array[k][j][2];
				}
			}

			energy_compare=0;
			for(j=0;j<num_zstacks;j++)
			{
				for(i=0;i<source_pointscount[refstack];i++)	
				{
				strain_calc_3d(christoffel_array,metric,inverse_metric,source_array,dummy_strain,dummy_deformation,curvature_tensor,i,j,source_pointscount[refstack],num_zstacks,entry_x,exit_x);		
				stress_calc_3d(source_array,dummy_strain,dummy_stress,i,j);	
				energy_compare+=free_energy_calc_3d(dummy_strain,dummy_stress,i,j);
				}
			}
//			printf("energy_compare=%f\n",energy_compare);
			if(ishift[m]==0 && m==0) 
			{
				smallest_energy=energy_compare; 
				//printf("smallest_energy=%f\n",energy_compare);
			}
			else if(energy_compare<smallest_energy) 
			{//if we found a new lower energy configuration, store that configuation in min_energy_ishift...	
				smallest_energy=energy_compare; 

				//printf("smaller energy combination of initial pixels found =%f\n",energy_compare);
				for(i=0;i<num_zstacks;i++) {min_energy_ishift[i]=ishift[i];} //printf("ishift[i]=%d\n",ishift[i]);}
			}
			
			number_of_iterations++;
		}
	}
	printf("iterations =%d num_zstacks=%d num_xy=%d\n",number_of_iterations,num_zstacks,source_pointscount[refstack]);

	//now we have the smallest energy configuration store in min_energy_ishift, set the initial deformation field to that!	
	for(j=0;j<num_zstacks;j++)
	{
		//num of pixels in source stack being considered is in source_pointscount, num in target known, is num_xypoints for all zstacks		
		for(i=0;i<source_pointscount[refstack];i++)	
		{	
			if((i+ishift[j])<source_pointscount[refstack]) { k=i+ishift[j];}
			else {k=i+ishift[j]-source_pointscount[refstack];}
	
			deformation_array[i][j][0]=target_array[i][j][0]-source_array[k][j][0];
			deformation_array[i][j][1]=target_array[i][j][1]-source_array[k][j][1];
			deformation_array[i][j][2]=target_array[i][j][2]-source_array[k][j][2];

		}
		printf("j= %d ishift[j]=%d\n",j,ishift[j]);
	}		
	
	printf("deformation initialised\n");
	
	return;
}



void deformation_perturb_s1(double*** shape_array,double*** deformation_array,int j,int num_xypoints,int num_zstacks)
{
	//only do the s1 deformation in this function.
	int i;
	i=rand()/(RAND_MAX+1.0)*num_xypoints;//select random point i on the boundary to perturb.

	//generate a random number from 0 to 1, if it's  >0.5 go forward along s1, else go backward.
	double rand_dir=(double)rand()/(double)(RAND_MAX);

	int ip1,im1;
	two_nearest_neighbour_points_i(i,&ip1,&im1,num_xypoints);

	double xchange=0,ychange=0,zchange=0;
	if(rand_dir>0.5){

			xchange=(shape_array[ip1][j][0]-shape_array[i][j][0])*delta;
			ychange=(shape_array[ip1][j][1]-shape_array[i][j][1])*delta;
		}
	else	{	
			xchange=(shape_array[im1][j][0]-shape_array[i][j][0])*delta;
			ychange=(shape_array[im1][j][1]-shape_array[i][j][1])*delta;
		}
	//printf("i=%d delta=%f xchange=%f ychange=%f\n",i,delta,xchange,ychange);
	zchange=0;//since this is supposed to have no component in z.
	shape_array[i][j][0]+=xchange;
	shape_array[i][j][1]+=ychange;
	shape_array[i][j][2]+=zchange;

	//moving the point also changes the deformation field!
	deformation_array[i][j][0]+=xchange;
	deformation_array[i][j][1]+=ychange;
	deformation_array[i][j][2]+=zchange;

	
	return;
}

void deformation_perturb_s2(double*** shape_array,double*** deformation_array,int num_xypoints,int num_zstacks)
{
	//only do the s2 deformation in this function.
	int i,j;
	//generate a random number from 0 to num_zstacks, if it's  >0.5 stack goes up along s2, else go down. Top/bottom stacks are kept fixed though.
	j=rand()/(RAND_MAX+1.0)*num_zstacks;//select random stack j to perturb.

	if(j>=(num_zstacks-1)||j<=0) 
	{
		return;
	}
	

	double rand_dir=(double)rand()/(double)(RAND_MAX);


	int jc,jp1,jm1;
	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
	for(i=0;i<num_xypoints;i++)
	{	double xchange=0,ychange=0,zchange=0;
		if(rand_dir>0.5)
		{	//delta is the fraction of the distance to shift zplanes by.
			xchange=(shape_array[i][jp1][0]-shape_array[i][j][0])*delta;
			ychange=(shape_array[i][jp1][1]-shape_array[i][j][1])*delta;
			zchange=(shape_array[i][jp1][2]-shape_array[i][j][2])*delta;
		}
		else
		{	
			xchange=(shape_array[i][jm1][0]-shape_array[i][j][0])*delta;
			ychange=(shape_array[i][jm1][1]-shape_array[i][j][1])*delta;
			zchange=(shape_array[i][jm1][2]-shape_array[i][j][2])*delta;
		}
		shape_array[i][j][0]+=xchange;
		shape_array[i][j][1]+=ychange;
		shape_array[i][j][2]+=zchange;

		deformation_array[i][j][0]+=xchange;
		deformation_array[i][j][1]+=ychange;
		deformation_array[i][j][2]+=zchange;
	}
	return;
}

void energy_min_s2(double*** target_array,double*** source_array,double*** strain_array,double*** deformation_array,double*** metric, double*** inverse_metric,double*** christoffel_array,double*** curvature_tensor,double*** stress_array,int initialised,int num_xypoints,int num_zstacks,double entry_x,double exit_x)
{
	printf("minimising energy in s2\n");
	//check for 3d data...
	if(dimensions == 2|| num_zstacks<3)
	{
		printf("Can't do 3d perturbation with only 2d data or <3 stacks!\n");
		return;
	}
	//since these perturbations should be quicker than s1 (far fewer zstacks than points on a given surface generally!), do entire minimisation algorithm in this function.

	int i,j,ip1,im1,jp1,jm1,k;
	int numchanges=1000,checktimes=0,rand_stack;
	double energytot=0;
	double energycomp=0;
	double kt,rand_comp,rand_num;
	//create dummy arrays to clone the source/deformation arrays to.
	double*** dummy_target;
	double*** dummy_deformation;

	array_alloc_3d_dbl(&dummy_target,num_xypoints,num_zstacks,3);
	array_alloc_3d_dbl(&dummy_deformation,num_xypoints,num_zstacks,3);

	double xchange=0,ychange=0,zchange=0;

	//first initialise kt.
	energytot=0;
	for(j=0;j<num_zstacks;j++)
	{
		for(i=0;i<num_xypoints;i++)
		{	
			dummy_target[i][j][0]=target_array[i][j][0];	
			dummy_target[i][j][1]=target_array[i][j][1];
			dummy_target[i][j][2]=target_array[i][j][2];
			dummy_deformation[i][j][0]=deformation_array[i][j][0];
			dummy_deformation[i][j][1]=deformation_array[i][j][1];	
			dummy_deformation[i][j][2]=deformation_array[i][j][2];	
		}
		
	}
	for(j=0;j<num_zstacks;j++)
	{
		for(i=0;i<num_xypoints;i++)
		{	
			energytot+=free_energy_calc_3d(strain_array,stress_array,i,j);
		}
		
	}
	//same logic as s1, initially keep approx 20% of potential changes when trying to minimise the energy, based on D. frekels book., i.e. exp(-Delta E/kT) ~0.2
	kt=energytot/num_zstacks*delta*delta*delta;
	printf("initial kt=%f\n",kt);
	srand(time(NULL));
// First check if the stack in question is the top or bottom stack. We are keeping upper and lower zstacks fixed, else results eventually converge on no extension in z case of all z stacks near or around same z values as in target image. This helps keep the z deformation component.

	
	while(numchanges>0 && checktimes<5)
	{	numchanges=0;
		for(k=0;k<num_zstacks*num_zstacks;k++)	
		{	

			//choose a random zstack to perturb

			rand_stack=rand()/(RAND_MAX+1.0)*(num_zstacks-1);
			j=(int)rand_stack;

			if(j!=(num_zstacks-1) && j!=0)	
			{	//if the chosen stack is not the top or bottom most stack, do a perturbation. Those 2 are kept fixed.
				jp1=j+1; 
				jm1= j-1;


				//generate a random number from 0 to 1, if it's  >0.5 go forward along s2 (i.e. up), else go back (i.e. down).
	
				double rand_dir=rand()/(RAND_MAX+1.0);
				for(i=0;i<num_xypoints;i++)//shift an entire zstack up if rand_dir>0.5, down if rand_dir<0.5
				{
					if(rand_dir>0.5)
					{
						xchange=(dummy_target[i][jp1][0]-dummy_target[i][j][0])*delta;
						ychange=(dummy_target[i][jp1][1]-dummy_target[i][j][1])*delta;
						zchange=(dummy_target[i][jp1][2]-dummy_target[i][j][2])*delta;
					}
					else
					{	
						xchange=(dummy_target[i][jm1][0]-dummy_target[i][j][0])*delta;
						ychange=(dummy_target[i][jm1][1]-dummy_target[i][j][1])*delta;
						zchange=(dummy_target[i][jm1][2]-dummy_target[i][j][2])*delta;
					}
		
					dummy_target[i][j][0]+=xchange;
					dummy_target[i][j][1]+=ychange;
					dummy_target[i][j][2]+=zchange;
	
					dummy_deformation[i][j][0]+=xchange;
					dummy_deformation[i][j][1]+=ychange;
					dummy_deformation[i][j][2]+=zchange;
				}	
				//now calculate the strain, stress and new energy for this deformation config
				energycomp=0;	
				for(j=0;j<num_zstacks;j++)
				{
					for(i=0;i<num_xypoints;i++)
					{	
						strain_calc_3d(christoffel_array,metric,inverse_metric,source_array,strain_array,dummy_deformation,curvature_tensor,i,j,num_xypoints,num_zstacks,entry_x,exit_x);
						stress_calc_3d(target_array,strain_array,stress_array,i,j);	
						energycomp+=free_energy_calc_3d(strain_array,stress_array,i,j);
					}
				}

				//and now check whether to keep the change.
				rand_comp=rand()/(RAND_MAX+1.0);
		
				if(exp(-(energycomp-energytot)/kt)>rand_num)
				{	//then keep the change!
					for(j=0;j<num_zstacks;j++)
					{
						for(i=0;i<num_xypoints;i++)
						{	
							target_array[i][j][0]=dummy_target[i][j][0];	
							target_array[i][j][1]=dummy_target[i][j][1];
							target_array[i][j][2]=dummy_target[i][j][2];
							deformation_array[i][j][0]=dummy_deformation[i][j][0];
							deformation_array[i][j][1]=dummy_deformation[i][j][1];	
							deformation_array[i][j][2]=dummy_deformation[i][j][2];
						}
				
					}	
					numchanges++;
					energytot=energycomp;
				}
				else
				{//otherwise, undo the change to dummy array
					printf("energy_change=%f\n",energycomp-energytot);
					for(j=0;j<num_zstacks;j++)
					{
						for(i=0;i<num_xypoints;i++)
						{	
							dummy_target[i][j][0]=target_array[i][j][0];	
							dummy_target[i][j][1]=target_array[i][j][1];
							dummy_target[i][j][2]=target_array[i][j][2];
							dummy_deformation[i][j][0]=deformation_array[i][j][0];
							dummy_deformation[i][j][1]=deformation_array[i][j][1];	
							dummy_deformation[i][j][2]=deformation_array[i][j][2];
						}			
					}
				}
			}
			printf("numchanges=%d\n",numchanges);		
			//reduce the effective temperature after 
			kt=kt*sim_anneal_scale;
		}
	}
return;
}

void energy_min_s1(double*** christoffel_array, double*** metric,double*** inverse_metric,double*** source_array,double*** target_array,double ***strain_array,double*** stress_array,double ***deformation_array,double*** curvature_tensor,int* target_pointcalc,int num_zstacks,double entry_x,double exit_x)
{	
	int i,j,k,l;
	//check whether a perturbation of point i,j, increases or decreases the energy, and reverse the deformation change if it doesn't and also doesn't match the increase energy rules for simulated annealing. 
	//calculate the strain, stress at all points on the target surface, and get the energy from there.
	double randnum;
	double theta,gamma;
	double new_freeenergy=0;
	double old_freeenergy=0;
	double deltaE;
	double kt=0.0010;
	int changes,lower_E_changes,higher_E_changes;
	int initialised_deformation=1;
	int n,num_perturbations;

	int energy_min_step=0;

	srand(time(NULL));
	for(l=0;l<num_zstacks;l++)
	{
		int checktimes=0;
		int checklimit=10;

		double*** lowest_energy_array;
		array_alloc_3d_dbl(&lowest_energy_array,target_pointcalc[l],num_zstacks,6);
		printf("minmising energy for stack %d, total numzstacks=%d\n",l,num_zstacks);
	
		double*** dummy_deformation;
		array_alloc_3d_dbl(&dummy_deformation,target_pointcalc[l],num_zstacks,3);
		double*** dummy_target;
		array_alloc_3d_dbl(&dummy_target,target_pointcalc[l],num_zstacks,3);


		double lowest_energy=pow(10,20);
		for(k=0;k<target_pointcalc[l];k++)//loop over all points on the given stack.
		{	//keep a copy of the lowest energy state found in lowest_energy
			lowest_energy_array[k][l][0]=target_array[k][l][0];	
			lowest_energy_array[k][l][1]=target_array[k][l][1];
			lowest_energy_array[k][l][2]=target_array[k][l][2];	

			lowest_energy_array[k][l][3]=deformation_array[k][l][0];
			lowest_energy_array[k][l][4]=deformation_array[k][l][1];
			lowest_energy_array[k][l][5]=deformation_array[k][l][2];	

			dummy_target[k][l][0]=target_array[k][l][0];	
			dummy_target[k][l][1]=target_array[k][l][1];
			dummy_target[k][l][2]=target_array[k][l][2];	
			dummy_deformation[k][l][0]=deformation_array[k][l][0];
			dummy_deformation[k][l][1]=deformation_array[k][l][1];
			dummy_deformation[k][l][2]=deformation_array[k][l][2];	
		
		}

		printf("initialised\n");
		//initialise the free energy for the current stack;
		old_freeenergy=0;
		for(k=0;k<target_pointcalc[l];k++)
		{
			rotation_angles_3d(source_array,k,l,&theta,&gamma,target_pointcalc[l],num_zstacks);			
			strain_calc_3d(christoffel_array,metric,inverse_metric,source_array,strain_array,dummy_deformation,curvature_tensor,k,l,target_pointcalc[l],num_zstacks,entry_x,exit_x);
			stress_calc_3d(source_array,strain_array,stress_array,k,l);		
			old_freeenergy+=free_energy_calc_3d(strain_array,stress_array,k,l);
			//printf("k=%d strain[i][j][0]=%f strain[i][j][1]=%f strain[i][j][2]=%f\n",k,strain_array[k][0][0],strain_array[k][0][1],strain_array[k][0][2]);
		}
			lowest_energy=old_freeenergy;
		printf("initial free energy=%f\n",old_freeenergy);
		//start kt at the mean value of energy between two points, decreased so initially have ~10% chance of going to higher energy (suggestion from understanding molecular simulations, frenkel).
		//warning: if kt is started at a value too high, causing many perturbations, liable to lose shape.

		kt=old_freeenergy/(target_pointcalc[l]*num_zstacks)*delta*delta*delta;
		if(num_zstacks>2) while(kt>5.0) {kt*=0.1;}//to reduce near infinite runtime caused by regions with errors in due to lack of averaged shapes, which result in very high starting kt and then loss of shape in 3d case :(.

		//kt=0.0;//for testing only!
		printf("initial kt=%f\n",kt);
		//now do the perturbations to minimise energy...
		if(num_zstacks>2) {num_perturbations=target_pointcalc[l];}
		else{num_perturbations=target_pointcalc[l]*target_pointcalc[l];}

		changes=1;
		while(checktimes<checklimit)
		{
			changes=0;
			lower_E_changes=0;
			higher_E_changes=0;
			double energy_prev_run=lowest_energy;
			for(n=0;n<num_perturbations;n++)
			{	
				//select random point and perturb it on s1
				//void deformation_perturb_s1(double*** shape_array,double*** deformation_array,int j,int num_xypoints,int num_zstacks)
				deformation_perturb_s1(dummy_target,dummy_deformation,l,target_pointcalc[l],num_zstacks);

				//calculate the new free energy with this configuration
				new_freeenergy=0;

				for(k=0;k<target_pointcalc[l];k++)
				{	
					rotation_angles_3d(source_array,k,l,&theta,&gamma,target_pointcalc[l],num_zstacks);									
					strain_calc_3d(christoffel_array,metric,inverse_metric,source_array,strain_array,dummy_deformation,curvature_tensor,k,l,target_pointcalc[l],num_zstacks,entry_x,exit_x);
					stress_calc_3d(source_array,strain_array,stress_array,k,l);						
					new_freeenergy+=free_energy_calc_3d(strain_array,stress_array,k,l);					
				}

				//find the change in free energy
				deltaE=new_freeenergy-old_freeenergy;
				//generate a random number between 0 and 1, and check if he new field should be kept		
				randnum=(double)rand()/(double)RAND_MAX;

				if(deltaE<0||exp(-deltaE/kt)>randnum)
				{	//if the energy decreases, keep the new deformation field and changes, otherwise 
					old_freeenergy=new_freeenergy;
					for(k=0;k<target_pointcalc[l];k++)
					{
						deformation_array[k][l][0]=dummy_deformation[k][l][0];
						deformation_array[k][l][1]=dummy_deformation[k][l][1];
						deformation_array[k][l][2]=dummy_deformation[k][l][2];	

						target_array[k][l][0]=dummy_target[k][l][0];	
						target_array[k][l][1]=dummy_target[k][l][1];
						target_array[k][l][2]=dummy_target[k][l][2];
					}
					changes++;
					if(deltaE<0) lower_E_changes++;
					else higher_E_changes++;
				}
				else 
				{	for(k=0;k<target_pointcalc[l];k++)
					{	//undo the change to the dummy arrays if not keeping it!
						dummy_deformation[k][l][0]=deformation_array[k][l][0];
						dummy_deformation[k][l][1]=deformation_array[k][l][1];
						dummy_deformation[k][l][2]=deformation_array[k][l][2];	
	
						dummy_target[k][l][0]=target_array[k][l][0];	
						dummy_target[k][l][1]=target_array[k][l][1];
						dummy_target[k][l][2]=target_array[k][l][2];
					}		
				}

				if(new_freeenergy<lowest_energy)
				{	//keep track of the absolutely lowest energy state found
					for(k=0;k<target_pointcalc[l];k++)//loop over all points on the given stack.
					{	//keep a copy of the lowest energy state found in lowest_energy array
						lowest_energy_array[k][l][0]=dummy_target[k][l][0];	
						lowest_energy_array[k][l][1]=dummy_target[k][l][1];
						lowest_energy_array[k][l][2]=dummy_target[k][l][2];	
						lowest_energy_array[k][l][3]=dummy_deformation[k][l][0];
						lowest_energy_array[k][l][4]=dummy_deformation[k][l][1];
						lowest_energy_array[k][l][5]=dummy_deformation[k][l][2];
					}
					lowest_energy=new_freeenergy;
				}	

		
				if(new_freeenergy>lowest_energy*energy_inc_limit)
				{//if the new energy from deformation gets too large, reset back to the lowest energy and continue the pertbuations from there.

					for(k=0;k<target_pointcalc[l];k++)
					{
						target_array[k][l][0]=lowest_energy_array[k][l][0];	
						target_array[k][l][1]=lowest_energy_array[k][l][1];
						target_array[k][l][2]=lowest_energy_array[k][l][2];	
						deformation_array[k][l][0]=lowest_energy_array[k][l][3];
						deformation_array[k][l][1]=lowest_energy_array[k][l][4];
						deformation_array[k][l][2]=lowest_energy_array[k][l][5];

						dummy_deformation[k][l][0]=deformation_array[k][l][0];
						dummy_deformation[k][l][1]=deformation_array[k][l][1];
						dummy_deformation[k][l][2]=deformation_array[k][l][2];	
	
						dummy_target[k][l][0]=target_array[k][l][0];	
						dummy_target[k][l][1]=target_array[k][l][1];
						dummy_target[k][l][2]=target_array[k][l][2];
					}
				}
			}
			if(energy_prev_run==lowest_energy || kt<0.00001)checktimes++;
				else checktimes=0;
			printf("changes=%d on stack %d for kt=%f checktimes=%d energy=%f,higher_E_changes=%d lower_E_changes=%d lowest_energy=%f\n",changes,l,kt,checktimes,old_freeenergy,higher_E_changes,lower_E_changes,lowest_energy);
			kt=kt*sim_anneal_scale;
			energy_min_step++;
			if(energy_min_step%1000==0)
			{//print out intermediate results to files
				printf("printing intermediate results\n");
				char intermediate_deformation[100];
				char intermediate_strain[100];
				char intermediate_stress[100];
				char intermediate_traction[100];

				sprintf(intermediate_deformation,"deformation_%d_stack=%d.txt",energy_min_step,l);
				sprintf(intermediate_strain,"strain_%d_stack=%d.txt",energy_min_step,l);
				sprintf(intermediate_stress,"stress_%d_stack=%d.txt",energy_min_step,l);
				sprintf(intermediate_traction,"traction_%d_stack=%d.txt",energy_min_step,l);
				
				FILE* intermediate_deformation_file;
				intermediate_deformation_file=fopen(intermediate_deformation,"w");

				FILE* intermediate_strain_file;
				intermediate_strain_file=fopen(intermediate_strain,"w");

				FILE* intermediate_stress_file;
				intermediate_stress_file=fopen(intermediate_stress,"w");

				FILE* intermediate_traction_file;
				intermediate_traction_file=fopen(intermediate_traction,"w");
				printf("files made,opened\n");
				double*** dummy_traction_array;
				array_alloc_3d_dbl(&dummy_traction_array,target_pointcalc[0],num_zstacks,3);
				printf("dummy trac memory allocated\n");
				for(k=0;k<target_pointcalc[l];k++)
				{

					strain_calc_3d(christoffel_array,metric,inverse_metric,source_array,strain_array,dummy_deformation,curvature_tensor,k,l,target_pointcalc[l],num_zstacks,entry_x,exit_x);
					stress_calc_3d(source_array,strain_array,stress_array,k,l);	
				}
				printf("stress, strain found for this config\n");
				traction_force(source_array,target_pointcalc,num_zstacks,stress_array,dummy_traction_array);
				printf("traction found for this configuration\n now printing to files\n");
				for(j=0;j<num_zstacks;j++)				
				{	for(k=0;k<target_pointcalc[j];k++)
					{		
					//	printf("printing to file\n");
					//	printf("printing to files for k=%d\n",k);
						fprintf(intermediate_deformation_file,"%f %f %f %f %f %f %f %f %f\n",source_array[k][j][0],source_array[k][j][1],source_array[k][j][2],target_array[k][j][0],target_array[k][j][1],target_array[k][j][2],deformation_array[k][j][0],deformation_array[k][j][1],deformation_array[k][j][2]);
						
						fprintf(intermediate_strain_file,"%f %f %f %f %f %f %f %f %f\n",source_array[k][j][0],source_array[k][j][1],source_array[k][j][2],strain_array[k][j][0],strain_array[k][j][1],strain_array[k][j][2],strain_array[k][j][3],strain_array[k][j][4],strain_array[k][j][5]);

						fprintf(intermediate_stress_file,"%f %f %f %f %f %f %f %f %f\n",source_array[k][j][0],source_array[k][j][1],source_array[k][j][2],stress_array[k][j][0],stress_array[k][j][1],stress_array[k][j][2],stress_array[k][j][3],stress_array[k][j][4],stress_array[k][j][5]);

						fprintf(intermediate_traction_file,"%f %f %f %f %f %f\n",source_array[k][j][0],source_array[k][j][1],source_array[k][j][2],dummy_traction_array[k][j][0],dummy_traction_array[k][j][1],dummy_traction_array[k][j][2]);
					}		
				}
				free_array_3d_dbl(&dummy_traction_array,target_pointcalc[0],num_zstacks,3);		
				fclose(intermediate_deformation_file);
				fclose(intermediate_strain_file);
				fclose(intermediate_stress_file);
				fclose(intermediate_traction_file);
				
			}
			//printf("kt=%f changes=%d\n",kt,changes,);
		}

		//energy minimisation is complete, now make the shape/deformation array for this stack be the lowest energy one found during the entire program run.
		for(k=0;k<target_pointcalc[l];k++)//loop over all points on the given stack.
		{	
			target_array[k][l][0]=lowest_energy_array[k][l][0];	
			target_array[k][l][1]=lowest_energy_array[k][l][1];
			target_array[k][l][2]=lowest_energy_array[k][l][2];	
			deformation_array[k][l][0]=lowest_energy_array[k][l][3];
			deformation_array[k][l][1]=lowest_energy_array[k][l][4];
			deformation_array[k][l][2]=lowest_energy_array[k][l][5];
		}
	}

	
	return;
}

int open_output_files(FILE** strainout,char* strainname, FILE** stressout,char* stressname,FILE** meshsource,char* meshsourcename,FILE** meshtarget,char* meshtargetname,FILE** christoffelout,char* christoffelname,FILE** deformationout,char* deformationname,FILE** tracout,char* tractionname)
{
	if((*strainout=fopen(strainname,"w"))==0)
	{
		printf("unable to open strain out file\n");
		return 0;
	}

	if((*stressout=fopen(stressname,"w"))==0)
	{
		printf("unable to open stress file\n");
		return 0;
	}

	if((*meshsource=fopen(meshsourcename,"w"))==0)
	{
		printf("unable to open mesh source out file\n");
		return 0;
	}

	if((*meshtarget=fopen(meshtargetname,"w"))==0)
	{
		printf("unable to open mesh target out file\n");
		return 0;
	}

	if((*christoffelout=fopen(christoffelname,"w"))==0)
	{
		printf("unable to open christoffel out file\n");
		return 0;
	}


	if((*deformationout=fopen(deformationname,"w"))==0)
	{
		printf("unable to open deformation out file\n");
		return 0;
	}

	if((*tracout=fopen(tractionname,"w"))==0)
	{
		printf("unable to open traction out file\n");
		return 0;
	}

	return 1;
}

void count_stacks(FILE* inputfile,int dimensions,int* stacks)
{rewind(inputfile);
	(*stacks)=0;
	double x,y,z,prev_z;
	if(dimensions==2)
	{	//2d data with data files only having x,y coordinates.
		(*stacks)=1;
	}
	else if(dimensions==3)
	{	//3d data file with x,y,z coordinates. note that z stacks should be ordered either from top to bottom or bottom to top, but must be ordered.

		prev_z=pow(10,10);//arb large initial setting so that z=/= prev_z on first element;
		if(z_coordinate==1)
		{
			while(fscanf(inputfile,"%lf %lf %lf",&z,&x,&y)>0)
			{	
				if(fabs(z-prev_z)>0.001)
				{
					//above condition met = new stack 
					(*stacks)++;
				}		
				prev_z=z;	
			}
		}
		else
		while(fscanf(inputfile,"%lf %lf %lf",&x,&y,&z)>0)
		{	
			if(fabs(z-prev_z)>0.001)
			{
				//above condition met = new stack 
				(*stacks)++;
			}		
			prev_z=z;	
		}
		
	}
	return;
}

void count_xypoints(FILE* inputfile,int dimensions,int* input_count,int stacks)
{	rewind(inputfile);
	double x,y,z,prev_z;
	int i,j;
	for(j=0;j<stacks;j++)
	{
		input_count[j]=0;
	}
	printf("input array initilised\n");
	if(dimensions==2)
	{	//2d data with data files only having x,y coordinates.
		while(fscanf(inputfile,"%lf %lf",&x,&y)>0)
		{				
			j=0;
			z=0;
			(input_count[j])++;
			i++;
		}
	}
	else	
	{	i=0; j=0;


		if(z_coordinate==1)
		{
			while(fscanf(inputfile,"%lf %lf %lf",&z,&x,&y)>0)
			{
				if(i==0 && j==0)
				{//first entry on a first row will trigger this rather than the following checks.
					i++;//i is just used as a check for the very first element here.					
					prev_z=z;
				}
				else if(fabs(z-prev_z)>=0.001)
				{//an entry on the next row.
					j++;				
				}	
			
				input_count[j]++;	
				prev_z=z;			
			}	
			
		}
		else 		
		{	while(fscanf(inputfile,"%lf %lf %lf",&x,&y,&z)>0)
			{
				if(i==0 && j==0)
				{//first entry on a first row will trigger this rather than the following checks.
					i++;//i is just used as a check for the very first element here.					
					prev_z=z;
				}
				else if(fabs(z-prev_z)>=0.001)
				{//an entry on the next row.
					j++;				
				}	
			
				input_count[j]++;	
				prev_z=z;			
			}	
		}

		
	}
	return;
}

void read_3d(FILE* inputfile,int dimensions,double*** input_array,int* input_count,int num_stacks)
{
	double x,y,z,prev_z;
	int i=0,j=0;
	rewind(inputfile);

	if(dimensions==2)
	{	//2d data with data files only having x,y coordinates.
		
		array_alloc_3d_dbl(&input_array,input_count[0],1,3);

		while(fscanf(inputfile,"%lf %lf",&x,&y)>0)
		{	z=0;
			input_array[i][j][0]=x;
			input_array[i][j][1]=y;
			input_array[i][j][2]=z;
		}
	}
	else if(dimensions==3)
	{	//3d data file with x,y,z coordinates. note that z stacks should be ordered either from top to bottom or bottom to top, but must be ordered.
		
		if(z_coordinate==1)
		for(j=0;j<num_stacks;j++)
		{
			for(i=0;i<(input_count[j]);i++)
			{		//messed up and had inconsistent labelling on the input files... hawas data files have the first coordinate as "z", while ivies had it in the sensible order x,y,
					fscanf(inputfile,"%lf %lf %lf",&input_array[i][j][2],&input_array[i][j][0],&input_array[i][j][1]);
			}
		}
		else 		
		for(j=0;j<num_stacks;j++)
		{
			for(i=0;i<(input_count[j]);i++)
			{	
				fscanf(inputfile,"%lf %lf %lf",&input_array[i][j][0],&input_array[i][j][1],&input_array[i][j][2]);
			}
		}
	}
	return;
}

double first_derivative_array_i(int i,int j,double*** shape_array,int nrows)
{
	int im1,ip1;

	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);
	double s1;

	distance_between_points_3d_array_i(shape_array,i,j,nrows,&s1);
	double dxds=(shape_array[ip1][j][0]-shape_array[im1][j][0])/s1;

	return dxds;
}

double second_derivative_array_i(int i,int j,double*** shape_array,int nrows,int nstacks)
{
	//second derivatives w.r.t. s by a central difference method
	int im1,ip1;
	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);

	double s1;
	distance_between_points_3d_array_i(shape_array,i,j,nrows,&s1);

	double d2xds2=(shape_array[ip1][j][0]-2*shape_array[i][j][0]+shape_array[im1][j][0])/(s1*s1);
	return d2xds2;
}

double first_derivative_array_j(int i,int j,double*** shape_array,int nrows,int nstacks)
{
	int jm1,jp1,jc;
	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,nstacks);

	double s1;

	distance_between_points_3d_array_i(shape_array,i,j,nrows,&s1);
	double dxds=(shape_array[i][jp1][0]-shape_array[i][jm1][0])/s1;

	return dxds;
}

double second_derivative_array_j(int i,int j,double*** shape_array,int nrows,int nstacks)
{	//second derivatives w.r.t. s by a central difference method
	int jm1,jp1,jc;
	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,nstacks);
	double s1;
	distance_between_points_3d_array_i(shape_array,i,j,nrows,&s1);

	double d2xds2=(shape_array[i][jp1][0]-2*shape_array[i][j][0]+shape_array[i][jm1][0])/(s1*s1);	
	return d2xds2;
}

void two_nearest_neighbour_points_i(int i,int* ip1,int* im1,int nrows)
{
	if(i==(nrows-1)){*ip1=0; *im1=i-1;}
	else if(i==0){*ip1=i+1; *im1=nrows-1;}
	else {*ip1=i+1; *im1= i-1;}
	return;
}

void four_nearest_neighbour_points_i(int i,int* ip1,int* im1,int* ip2,int* im2,int nrows)
{
	if(i==(nrows-1)){*ip1=0; *ip2=1; *im1=i-1; *im2=i-2;}
	else if(i==(nrows-2)){*ip1=nrows-1; *ip2=0; *im1=i-1; *im2=i-2;}
	else if(i==0){*ip1=i+1; *ip2=i+1; *im1=nrows-1; *im2=nrows-2;}
	else if(i==1){*ip1=i+1; *ip2=i+1; *im1=0; *im2=nrows-1;}
	else {*ip1=i+1; *ip2=i+2; *im1= i-1; *im2=i-2;}
	return;
}

void two_nearest_neighbour_points_j(int j,int* jc,int* jp1,int* jm1,int nstacks)
{
	if(nstacks<=1){*jp1=j;*jm1=j; return;} //exit immediately if only one stack!
	else if(j==(nstacks-1)){*jp1=j; *jm1=j-1;*jc=j;}//so that you use a backwards difference method rather than central difference on top layer here, since points are not cyclical in j.
	else if(j==0){*jp1=1; *jc=0; *jm1=0;}//so that you use a forward difference method rather than central difference on bottom here, since points are not cyclical in j.
	else {*jp1=j+1; *jm1= j-1; *jc=j;}
	return;
}

void four_nearest_neighbour_points_j(int j,int* jc,int* jp1,int* jm1,int* jp2,int* jm2,int nstacks)
{	//use this at any time where determining the indicies for 
	if(nstacks==1){*jp2=0; *jp1=0; *jm1=0;*jm2=0; *jc=0;}
	else if(nstacks==2){*jp2=nstacks; *jp1=nstacks; *jm1=0;*jm2=0; *jc=j;}
	else if(j==(nstacks-1)){*jp2=j; *jp1=j; *jm1=j-1;*jm2=j-2; *jc=j;}//so that you use a backwards difference method rather than central difference on top layer here, since points are not cyclical in j.
	else if(j==(nstacks-2)){*jp2=nstacks-1; *jp1=nstacks-1; *jm1=j-1; *jm2=j-2; *jc=j;}
	else if(j==0){*jp2=2; *jp1=1; *jc=0; *jm1=0; *jm2=0;}//so that you use a forward difference method rather than central difference on bottom here, since points are not cyclical in j.
	else if(j==1){*jp2=j+2; *jp1=j+1; *jm1=0; *jm2=0; *jc=1;}
	else {*jp2=j+2; *jp1=j+1; *jm1= j-1; *jm2=j-2; *jc=j;}

	return;
}

void distance_between_points_3d_array_i(double*** shape_array,int i,int j,int nrows,double* s1)
{
	int ip1,im1;
	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);
	
	*s1=sqrt( pow((shape_array[ip1][j][0]-shape_array[im1][j][0]),2)
		 +pow((shape_array[ip1][j][1]-shape_array[im1][j][1]),2)
		 +pow((shape_array[ip1][j][2]-shape_array[im1][j][2]),2));
	return;
}

void distance_between_points_3d_array_j(double*** shape_array,int i,int j,int nstacks,double* s2)
{
	int jp1,jm1,jc;
	
	if(nstacks>1)
	{
		two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,nstacks);
		*s2=sqrt( (shape_array[i][jp1][0]-shape_array[i][jm1][0])*(shape_array[i][jp1][0]-shape_array[i][jm1][0])
			 +(shape_array[i][jp1][1]-shape_array[i][jm1][1])*(shape_array[i][jp1][1]-shape_array[i][jm1][1])
			 +(shape_array[i][jp1][2]-shape_array[i][jm1][2])*(shape_array[i][jp1][2]-shape_array[i][jm1][2]));
	}
	else *s2=0;
	return;
}

void direction_vectors_i(double*** shape_array,double* e1x,double* e1y,double* e1z,int i,int j, int nrows)
{
	int ip1,im1;
	double s1;
	two_nearest_neighbour_points_i(i,&ip1,&im1,nrows);
	distance_between_points_3d_array_i(shape_array,i,j,nrows,&s1);

	if(s1!=0)
	{
		*e1x=(shape_array[ip1][j][0]-shape_array[im1][j][0])/s1;
		*e1y=(shape_array[ip1][j][1]-shape_array[im1][j][1])/s1;
		*e1z=(shape_array[ip1][j][2]-shape_array[im1][j][2])/s1; //nb this should always be zero, since z is held constant over varied i.
	}
	else 
	{		
		*e1x=0;
		*e1y=0;
		*e1z=0;
	}
	return;
}

void direction_vectors_j(double*** shape_array,double* e2x,double* e2y,double* e2z,int i,int j, int nstacks)
{
	int jp1,jm1,jc;
	two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,nstacks);

	double s2;
	distance_between_points_3d_array_j(shape_array,i,j,nstacks,&s2);

	if(s2!=0 && dimensions!=2 && nstacks>1)
	{
		*e2x=(shape_array[i][jp1][0]-shape_array[i][jm1][0])/s2;
		*e2y=(shape_array[i][jp1][1]-shape_array[i][jm1][1])/s2;
		*e2z=(shape_array[i][jp1][2]-shape_array[i][jm1][2])/s2; //nb this should always be zero, since z is held constant over varied i.
	}
	else 	
	{
		*e2x=0;
		*e2y=0;
		*e2z=0;
	}
	return;
}

void orientate_curve_before_remeshing(double*** shape_array, int num_zstacks,int* pointcount_array)
{	//this function orientates the curves making up shape array so that they are all orientated anti clockwise.
	//two versions of this function: this one and another immediately below, this one is for before curves are all remeshed to have same number of points only
	int i,j,k,smallest_index;
	double smallest_angle;
	double current_theta;
	for(j=0;j<num_zstacks;j++)
	{	
		double** dummy_array;
		array_alloc_2d_dbl(&dummy_array,pointcount_array[j],3);
		smallest_angle=4*M_PI;//just so this number is larger than any angle that will be calculated!
		for(i=0;i<pointcount_array[j];i++)
		{
			if(fabs(shape_array[i][j][1])<0.00001) shape_array[i][j][1]=+0;
			current_theta=atan2(shape_array[i][j][1],shape_array[i][j][0]);
			if(current_theta<smallest_angle)
			{
				smallest_angle=current_theta; 
				smallest_index=i;
			}
		}

		for(i=0;i<pointcount_array[j];i++)
		{
			if((i-smallest_index)>=0)
			{	
				dummy_array[i-smallest_index][0]=shape_array[i][j][0];
				dummy_array[i-smallest_index][1]=shape_array[i][j][1];
			}	
			else if((i-smallest_index)<0)
			{
				dummy_array[pointcount_array[j]-smallest_index+i][0]=shape_array[i][j][0];
				dummy_array[pointcount_array[j]-smallest_index+i][1]=shape_array[i][j][1];
			}
		}

		for(i=0;i<pointcount_array[j];i++)
		{			
				shape_array[i][j][0]=dummy_array[i][0];
				shape_array[i][j][1]=dummy_array[i][1];		
		}

		if(atan2(shape_array[1][j][1],shape_array[1][j][0])>0)
		{
			for(i=0;i<pointcount_array[j];i++)
			{
				dummy_array[i][0]=shape_array[i][j][0];
				dummy_array[i][1]=shape_array[i][j][1];
			}
			for(i=1;i<pointcount_array[j];i++)
			{
				shape_array[i][j][0]=dummy_array[pointcount_array[j]-i][0];
				shape_array[i][j][1]=dummy_array[pointcount_array[j]-i][1];
			}
		}
		free_array_2d_dbl(&dummy_array,pointcount_array[j]);
	}
	return;
}

void orientate_curve_after_remeshing(double*** shape_array, int num_zstacks,int shape_points)
{	//this function orientates the curves making up shape array so that they are all orientated anti clockwise.
	//two versions of this function: this one and another immediately above, this one is for AFTER curves are all remeshed to have same number of points ONLY, or in 2d case.
	int i,j,k,smallest_index;
	double smallest_angle;
	double current_theta;
	for(j=0;j<num_zstacks;j++)
	{	
		double** dummy_array;
		array_alloc_2d_dbl(&dummy_array,shape_points,3);
		smallest_angle=4*M_PI;//just so this number is larger than any angle that will be calculated!
		for(i=0;i<shape_points;i++)
		{
			if(fabs(shape_array[i][j][1])<0.00001) shape_array[i][j][1]=+0;
			current_theta=atan2(shape_array[i][j][1],shape_array[i][j][0]);
			if(current_theta<smallest_angle)
			{
				smallest_angle=current_theta; 
				smallest_index=i;
			}
		}

		for(i=0;i<shape_points;i++)
		{
			if((i-smallest_index)>=0)
			{	
				dummy_array[i-smallest_index][0]=shape_array[i][j][0];
				dummy_array[i-smallest_index][1]=shape_array[i][j][1];
			}	
			else if((i-smallest_index)<0)
			{
				dummy_array[shape_points-smallest_index+i][0]=shape_array[i][j][0];
				dummy_array[shape_points-smallest_index+i][1]=shape_array[i][j][1];
			}
		}

		for(i=0;i<shape_points;i++)
		{			
				shape_array[i][j][0]=dummy_array[i][0];
				shape_array[i][j][1]=dummy_array[i][1];		
		}
		//now have the list with smallest angle as the first element in the array. check list to see if need inverting now.
		//if second element has angle >0 then list is clockwise and needs reorientation
		//inverting list if needed
		if(atan2(shape_array[1][j][1],shape_array[1][j][0])>0)
		{
			for(i=0;i<shape_points;i++)
			{
				dummy_array[i][0]=shape_array[i][j][0];
				dummy_array[i][1]=shape_array[i][j][1];
			}
			for(i=1;i<shape_points;i++)
			{
				shape_array[i][j][0]=dummy_array[shape_points-i][0];
				shape_array[i][j][1]=dummy_array[shape_points-i][1];
			}
		}
		free_array_2d_dbl(&dummy_array,shape_points);
	}
	return;
}

void iterate_poissons_ratio(double*** source_shape,double*** target_shape,int* shape_rows,int num_zstacks,int refstack,double* output_pois,double*** stress_array,double*** strain_array,double*** traction_array)
{	//this poissons ratio function iterates the poisson ratio for a given deformation set. Alternate between this and the energy minimisation function to determine a new poissons ratio value.
	//only use after the remeshing algorithm so that number of stacks and rows are the same on both shapes!

	double prev_pois=10;//initialise to nonsense so that first loop can happen! 
	double sigma_tolerance=pow(10,-4);
	int i,j,k;
	int ip1,im1,jp1,jm1,jc;

	double deltas1,deltas2;
	FILE* poisson_file;
	poisson_file=fopen("poissons_ratio.txt","a");
	fprintf(poisson_file,"###############NEW ITERATION######\n");

	int iterations=0;
	int checktimes=0, checklimit=10;
	double* linelength_array;//contains the length of the curve along the targetshape between each midpoints of each segment. 
	array_alloc_1d_dbl(&linelength_array,shape_rows[refstack]);

	double** area_array;//contains the length of the curve along the source shape between each midpoints of each segment, used for weighting 3d data. 
	array_alloc_2d_dbl(&area_array,shape_rows[refstack],num_zstacks);//area is the area represented by each segment for weighting by the segment if num_zstacks>1
	
	double linelength_before=0;
	double linelength_after=0;
	double total_linelength=0;

	double area_before=0;
	double area_after=0;
	double area_change=0;

	if(num_zstacks==1)
	{ 	//2d data, and the linelength is used to weight each term of the poisson ratio 
		j=0;
		for (i=0;i<shape_rows[j];i++)
		{
			if(i==(shape_rows[j]-1)) {ip1=0; im1=i-1;}
			else if (i==0) {ip1 = i+1; im1=shape_rows[j]-1;}
			else { ip1=i+1; im1=i-1;}

			//length of segments between two neighbouring midpoints, for the mean pressure weighting towards the end of function.
			linelength_array[i]=1.0/2.0*sqrt((source_shape[ip1][j][0]-source_shape[i][j][0])*(source_shape[ip1][j][0]-source_shape[i][j][0])+(source_shape[ip1][j][1]-source_shape[i][j][1])*(source_shape[ip1][j][1]-source_shape[i][j][1]))+1.0/2.0*sqrt((source_shape[i][j][0]-source_shape[im1][j][0])*(source_shape[i][j][0]-source_shape[im1][j][0])+(source_shape[i][j][1]-source_shape[im1][j][1])*(source_shape[i][j][1]-source_shape[im1][j][1]));
			linelength_before+=linelength_array[i];
			area_before+= (source_shape[i][j][0] *source_shape[ip1][j][1]-source_shape[ip1][j][0]*source_shape[i][j][1]);
	  	
		}

		for (i=0;i<shape_rows[j];i++)
		{
			if(i==(shape_rows[j]-1)) {ip1=0; im1=i-1;}
			else if (i==0) {ip1 = i+1; im1=shape_rows[j]-1;}
			else { ip1=i+1; im1=i-1;}

			//length of segments between two neighbouring midpoints, for the mean pressure weighting towards the end of function.
			linelength_after+=1.0/2.0*sqrt((target_shape[ip1][j][0]-target_shape[i][j][0])*(target_shape[ip1][j][0]-target_shape[i][j][0])+(target_shape[ip1][j][1]-target_shape[i][j][1])*(target_shape[ip1][j][1]-target_shape[i][j][1]))+1.0/2.0*sqrt((target_shape[i][j][0]-target_shape[im1][j][0])*(target_shape[i][j][0]-target_shape[im1][j][0])+(target_shape[i][j][1]-target_shape[im1][j][1])*(target_shape[i][j][1]-target_shape[im1][j][1]));
			area_after+= (target_shape[i][j][0] *target_shape[ip1][j][1]-target_shape[ip1][j][0]*target_shape[i][j][1]);
		}
		area_change=area_after-area_before;
	}

	if(num_zstacks>1)
	{	//find the areas used to weight each segment
		for(j=0;j<num_zstacks;j++)
		{	
			for(i=0;i<shape_rows[j];i++)
			{
				two_nearest_neighbour_points_i(i,&ip1,&im1,shape_rows[j]);
				two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
				deltas1=sqrt(pow(source_shape[ip1][j][0]-source_shape[im1][j][0],2)+pow(source_shape[ip1][j][1]-source_shape[im1][j][1],2));
				deltas2=sqrt(pow(source_shape[i][jp1][0]-source_shape[i][jp1][0],2)+pow(source_shape[i][jp1][1]-source_shape[i][jm1][1],2)+pow(source_shape[i][jp1][2]-source_shape[i][jm1][2],2));
				area_array[i][j]=1.0/4.0*deltas1*deltas2;
				area_before+=area_array[i][j];
			}		
		}
	}


	double volume_before=0;
	double volume_after=0;
	double volume_change=0;
	double theta=0;
	double lij,lijp1,lip1j,lip1jp1;
	if(num_zstacks>1)
	{	//find the volumes of start and end shapes/
		for(j=0;j<num_zstacks;j++)
		{	
			for(i=0;i<shape_rows[j];i++)
			{
				two_nearest_neighbour_points_i(i,&ip1,&im1,shape_rows[j]);
				two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
				
				theta=atan2(source_shape[ip1][j][1]-source_shape[i][j][1],source_shape[ip1][j][0]-source_shape[i][j][0]);
				lij=sqrt(pow(source_shape[i][j][0],2)+pow(source_shape[i][j][1],2));
				lip1j=sqrt(pow(source_shape[ip1][j][0],2)+pow(source_shape[ip1][j][1],2));
				lijp1=sqrt(pow(source_shape[i][jp1][0],2)+pow(source_shape[i][jp1][1],2));	
				lip1jp1=sqrt(pow(source_shape[ip1][jp1][0],2)+pow(source_shape[ip1][jp1][1],2));	
				volume_before+=sin(theta)/6.0 * (fabs(source_shape[i][jp1][2]-source_shape[i][j][2])) *(lijp1*(lip1jp1+lip1j/2.0)+lij*(lip1jp1/2.0+lip1j));
			}	
			
		}

	}

	if(num_zstacks>1)
	{	//find the volumes of start and end shapes/
		for(j=0;j<num_zstacks;j++)
		{	
			for(i=0;i<shape_rows[j];i++)
			{
				two_nearest_neighbour_points_i(i,&ip1,&im1,shape_rows[j]);
				two_nearest_neighbour_points_j(j,&jc,&jp1,&jm1,num_zstacks);
				
				theta=atan2(target_shape[ip1][j][1]-target_shape[i][j][1],target_shape[ip1][j][0]-target_shape[i][j][0]);
				lij=sqrt(pow(target_shape[i][j][0],2)+pow(target_shape[i][j][1],2));
				lip1j=sqrt(pow(target_shape[ip1][j][0],2)+pow(target_shape[ip1][j][1],2));
				lijp1=sqrt(pow(target_shape[i][jp1][0],2)+pow(target_shape[i][jp1][1],2));	
				lip1jp1=sqrt(pow(target_shape[ip1][jp1][0],2)+pow(target_shape[ip1][jp1][1],2));	
				volume_after+=sin(theta)/6.0 * (fabs(target_shape[i][jp1][2]-target_shape[i][j][2])) *(lijp1*(lip1jp1+lip1j/2.0)+lij*(lip1jp1/2+lip1j));
			}	
			
		}

	}

	volume_change=volume_after-volume_before;
	double linelength_change=linelength_after-linelength_before;
	
	double volume=0;
	double*** volume_array;//used with 3d data	

	double Fx,Fy,Fz;
	double** pressure_array;
	array_alloc_2d_dbl(&pressure_array,shape_rows[refstack],num_zstacks);
	double e1x,e2x,e1y,e2y,e1z,e2z,nx,ny,nz;

	printf("finding poisson's ratio\n");

	while(fabs(*output_pois-prev_pois)>sigma_tolerance)//note prev_pois is the poisson ratio found from the previous energy minimisation 
	{	prev_pois=*output_pois;	
		
		while(checktimes<checklimit)
		{	iterations++;//to check/test how many times the entire code runs
			//ensure we have calculated the initial strain/stress for current deformation?

			for(j=0;j<num_zstacks;j++)
			{	
				for(i=0;i<shape_rows[j];i++)
				{
					pressure_array[i][j]=0;
					if(i==shape_rows[j]-1)
					{
						ip1=0; 
						im1=i-1;
					}
					else if(i==0)
					{
						ip1=i+1; 
						im1=shape_rows[j]-1;
					}
					else {
						ip1=i+1;
						im1= i-1;
					}
					
					direction_vectors_i(source_shape,&e1x,&e1y,&e1z,i,j,shape_rows[j]);
					if(num_zstacks>1) direction_vectors_j(source_shape,&e2x,&e2y,&e2z,i,j,num_zstacks);
					
					normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_zstacks);
					
					Fx= nx*stress_array[i][j][0]+ny*stress_array[i][j][1]+nz*stress_array[i][j][2];
					Fy= nx*stress_array[i][j][1]+ny*stress_array[i][j][2]+nz*stress_array[i][j][4];
					Fz= nx*stress_array[i][j][3]+ny*stress_array[i][j][4]+nz*stress_array[i][j][5];//should be evaluating as zero for the 2d case anyway...

					traction_array[i][j][0]=Fx;
					traction_array[i][j][1]=Fy;
					traction_array[i][j][2]=Fz;
					
					//pressure_array[i][j]=Fx*nx+Fy*ny+Fz*nz;
					pressure_array[i][j]=stress_array[i][j][0]+stress_array[i][j][2]+stress_array[i][j][5];
				}
			}	
			
			double meanp=0;
			double weighted_meanp=0;
			for(j=0;j<num_zstacks;j++)
			{	
				for(i=0;i<shape_rows[j];i++)
					{
						if(num_zstacks==1) weighted_meanp+=(pressure_array[i][j]*linelength_array[i]);	
						else if(num_zstacks>1) weighted_meanp+=(pressure_array[i][j]*area_array[i][j]);	
					}
			}

			if(num_zstacks==1) weighted_meanp=weighted_meanp/linelength_before;
			else if(num_zstacks>1) weighted_meanp=weighted_meanp/area_before;

			if(num_zstacks==1)
			{
				*output_pois=0.5-E*((area_change)/area_before)/(6.0*weighted_meanp); //assume the filled channel model
 				fprintf(poisson_file,"%f %f %f %f\n",*output_pois,area_change,area_before,weighted_meanp);
			}
			if(num_zstacks>1)
			{
				*output_pois=0.5-E*((volume_change)/volume_before)/(6.0*weighted_meanp); //assume the filled channel model
				fprintf(poisson_file,"%f %f %f %f\n",*output_pois,volume_change,volume_before,weighted_meanp);
			}
			

			if(fabs(sigma-*output_pois)<sigma_tolerance)
			{
				checktimes++;
			}
			else checktimes=0;
		
			if(*output_pois<=0.49 && *output_pois>=-1)
			{//valid output pois ratio...
				//printf("New poisson ratio=%f pressure =%f 2nd term=%f\n",output_pois,weighted_meanp,((area_change)/area_after)/(6.0*weighted_meanp));
				sigma=*output_pois;
				//and recalculate the stress for this shape with the new poisson ratio		
				for(j=0;j<num_zstacks;j++)
				{	for(i=0;i<shape_rows[j];i++)
					{	
						stress_calc_3d(source_shape,strain_array,stress_array,i,j);
					}			
				}
				

			}
			else if(*output_pois>0.49 && *output_pois<0.5)
			{	*output_pois=0.50;
				return;//stop here, and assume it is incompressible
			}
			else 
			{
				printf("Warning invalid ratio calculated :(");
				if(*output_pois>0.5) printf(" Larger than > 0.5\n"); 
				else printf("less than 1.0 \n");
			
				//if(initial_pois>=-1.0) initial_pois-=0.01;//try a lower starting value
				//else {printf("run out of values for initial_pois :(\n"); return 0;}
				return;
			}
			
		}
	
	}

	return;
}

//get the volume of a 2d shape with an assumption about the out of plane direction.
double volume_calc(double*** shape_array,int* shape_points,int num_stacks,char volume_mode,double entry_x,double exit_x)
{
	double volume;
	int i,j,k;
	double area=0.0,area_in=0.0,area_out=0.0;
	if(num_stacks==1)
	{
		if(volume_mode=='f')
		{
			//fills the channel/constriction entirely, and they both have a potentially varying "height", in and out of the constriction, so use the function which determines in/out too.

			double areacomx=0,areacomy=0;	
			double comx=0.0, comy=0.0;
			double xtmp=0.0, ytmp=0.0, ai=0.0;
			//order shape_array by smallest angle increasing,needs to be around com for this to work...;
			//make sure curve is orientated clockwise, and copy it .

			//first find the total area of the entire outline.
			area=0;
			for(j=0;j<num_stacks;j++)
			{	for (i=0;i<shape_points[j]; i++)	
				{	//2d outline so num_stacks=1;
					if(i==(shape_points[j]-1)) k=0;
					else k=i+1;
					ai=shape_array[i][j][0] *shape_array[k][j][1] - shape_array[k][j][0] * shape_array[i][j][1];
					area+= ai;
				}
			}
			area=area/2.0;
			//now find the index numbers where the nucleus switches from in the channel to out
			int first_pixel_in_con=-1,last_pixel_in_con=-1;
			int first_pixel_out_con=-1,last_pixel_out_con=-1;

			printf("finding entrance/exit pixels\n");
			for(j=0;j<num_stacks;j++)
			{	for (i=0;i<shape_points[j];i++)	
				{	//Check_entry is a function which finds whether a point is on the boundary between in/out of the constriction or not.
					if(check_entry(shape_array,i,j,shape_points,entry_x,exit_x,first_pixel_in_con,last_pixel_in_con)==1)
					{
						first_pixel_in_con=i;
					}
					else if(check_entry(shape_array,i,j,shape_points,entry_x,exit_x,first_pixel_in_con,last_pixel_in_con)==2)
					{
						last_pixel_in_con=i;
					}
					if(check_exit(shape_array,i,j,shape_points,entry_x,exit_x,first_pixel_out_con,last_pixel_out_con)==1)
					{
						first_pixel_out_con=i;		
					}
					else if(check_exit(shape_array,i,j,shape_points,entry_x,exit_x,first_pixel_out_con,last_pixel_out_con)==2)
					{
						last_pixel_out_con=i;		
					}					
				}
			}	
			//printf("entrance pixel=%d exit_pixel=%d %d %d\n",first_pixel_out_con,last_pixel_out_con,first_pixel_in_con,last_pixel_in_con);
			printf("pixels in :%d %d pixels out :%d %d\n",first_pixel_in_con,last_pixel_in_con,first_pixel_out_con,last_pixel_out_con);
			//initialise the areas in and outside of the constriction				
			area_in=0;
			area_out=0;
			//now find the area in the constriction by using the same method as before - except, we loop over the points from first_pixelion to last_pixel in.
			printf("finding area in constriction\n");
			area_in_out_con(shape_array,first_pixel_in_con,last_pixel_in_con,first_pixel_out_con,last_pixel_out_con,entry_x,exit_x,&area_in,&area_out,&area,shape_points);
			//find area between entry points
			printf("area found\n");
			printf("area_in=%f lzcon=%f area_out=%f lz=%f\n",area_in,Lzcon,area_out,Lz);
			volume=fabs(area_in)*Lzcon+fabs(area_out)*Lz;
			return volume;	
		}
		if(volume_mode=='s')
		{
			//small elipsoid volume, i.e. assume the shape of the nucleus is given by a ellipsoid with two axes the same length. In this case, the length shared twice is the x axis length (which is extended along the channel).	
			//first find the biggest change in x and y, half these and use those as the axes radii.
			double radii_a=0;//x radius, shared with z direction for small nucleus
			double radii_b=0;//y radius
			i=0;j=0;k=0;
			for(j=0;j<num_stacks;j++)
			{
				for(i=0;i<shape_points[j];i++)
				{	
					for(k=0;k<shape_points[j];k++)
					{	
						if((shape_array[i][j][0]-shape_array[k][j][0])>radii_a) radii_a=(shape_array[i][j][0]-shape_array[k][j][0]);
						if((shape_array[i][j][1]-shape_array[k][j][1])>radii_b) radii_b=(shape_array[i][j][1]-shape_array[k][j][1]);
					}
				}
			}
			radii_a=radii_a/2.0;
			radii_b=radii_b/2.0;
			printf("radii_a=%f radii_b=%f\n",radii_a,radii_b);
			volume=4.0/3.0 * M_PI * radii_a * radii_a *radii_b;
			return volume;		
		}
	}
	else if(num_stacks>1 && (top_surface=='b' || top_surface=='f') && (bottom_surface=='b' || bottom_surface=='f'))
	{	//3d volume with bounded or free top and bottom surfaces.
		//first find the volume between the stacks
		double aj,ajp1;
		double area_j=0,area_jp1=0;
		//check curve is orientated anticlockwise before doing anything, so that returned area/volume values are positive!
		orientate_curve_after_remeshing(shape_array,num_stacks,shape_points[0]);
		double d_theta,rij,rip1j,rijp1,rip1jp1,delta_z;


		volume=0;
		for(j=0;j<(num_stacks-1);j++)
		{	for (i=0;i<shape_points[j]; i++)	
			{	
				if(i==(shape_points[j]-1)) k=0;
				else k=i+1;
				
				int jp1=j+1;
				rij=sqrt(pow(shape_array[i][j][0]*shape_array[i][j][0]+shape_array[i][j][1]*shape_array[i][j][1],2));
				rip1j=sqrt(pow(shape_array[k][j][0]*shape_array[k][j][0]+shape_array[k][j][1]*shape_array[k][j][1],2));
				rijp1=sqrt(pow(shape_array[i][jp1][0]*shape_array[i][jp1][0]+shape_array[i][jp1][1]*shape_array[i][jp1][1],2));
				rip1jp1=sqrt(pow(shape_array[k][jp1][0]*shape_array[k][jp1][0]+shape_array[k][jp1][1]*shape_array[k][jp1][1],2));
				d_theta=fabs(atan2(shape_array[i][j][1],shape_array[i][j][0])-atan2(shape_array[k][j][1],shape_array[k][j][0]));
				delta_z=fabs(shape_array[i][jp1][2]-shape_array[i][j][2]);
				volume+=sin(d_theta)/6.0 * delta_z* (rijp1*(rip1jp1+rip1j/2.0)+rij*(rip1jp1/2.0+rip1j));
			}
		}
	}
	else return 0.0;
}

void area_in_out_con(double*** position_array, int first_pixel_in_con,int last_pixel_in_con, int first_pixel_out_con, int last_pixel_out_con,double entry_x,double exit_x,double* area_in,double* area_out, double* area,int* numpoints)
{	//this function should be 2d only, so no need to loop over j, just set j=0
	int i,j=0,k;
	int entered;
	double ai;
	*area_in=0;
	*area_out=0;
	j=0;
	for (i=0;i<numpoints[j]; i++)	
	{
		if(i==(numpoints[j]-1)) k=0;
		else k=i+1;
  		ai=position_array[i][j][0] *position_array[k][j][1] - position_array[k][j][0]* position_array[i][j][1];
		*area+=ai;
	}

	*area=*area/2.0;
	//printf("entry_x= %f exit_x=%f position_array[000]=%f\n",entry_x,exit_x,position_array[0][0][0]);
	if(first_pixel_in_con==-1 && last_pixel_in_con==-1 && first_pixel_out_con==-1 && last_pixel_out_con==-1 && (position_array[0][0][0]<entry_x || position_array[0][0][0]>exit_x))
	{//nucleus is not entering or exiting and is not between entry/exit, which is checked by having a check on a point on the curve
			
		*area_out=fabs(*area);
		*area_in=0;
		return;
	}
	else if(first_pixel_in_con==-1 && last_pixel_in_con==-1 && first_pixel_out_con==-1 && last_pixel_out_con==-1 && position_array[0][0][0]>entry_x &&position_array[0][0][0]<exit_x)
	{//nucleus is not entering or exiting and IS between entry/exit, which is checked by having a check on a point on the curve, so is entirely in constriction
		
		*area_in=fabs(*area);
		*area_out=0;
		entered=1;
	}
	else if(first_pixel_in_con!=-1 && last_pixel_in_con!=-1 && first_pixel_out_con==-1 && last_pixel_out_con==-1)
	{	
		//nucleus is entering but not exiting
		entered=1;
		//loop over the regions between first/last pixels in con 
		if(first_pixel_in_con>last_pixel_in_con)
		for (i=last_pixel_in_con;i<=first_pixel_in_con; i++)	
		{	
			if(i==(numpoints[j]-1)) k=0;
			else if(i==last_pixel_in_con) k=first_pixel_in_con;
			else k=i+1;

			ai=(position_array[k][j][0])*(position_array[i][j][1]) - (position_array[i][j][0]) * (position_array[k][j][1]);
	    		*area_in+= ai;
		}	
		else if(first_pixel_in_con<last_pixel_in_con)
		{			
			for (i=first_pixel_in_con;i<=last_pixel_in_con; i++)	
			{	
				if(i==(numpoints[j]-1)) k=0;
				else if(i==last_pixel_in_con) k=first_pixel_in_con;
				else k=i+1;

				ai=(position_array[k][j][0])*(position_array[i][j][1]) - (position_array[i][j][0]) * (position_array[k][j][1]);
	    			*area_in+= ai;
				//printf("%f %f %f %f\n",position_array[i][j][0],position_array[i][j][0]-position_array[last_pixel_in_con][j][0],position_array[i][j][1],ai);
			}

		}
		else{printf("warning first_pixel_in_con=last_pixel_in_con\n");}

		*area_out=fabs(*area)-fabs(*area_in);
	}
	else if(first_pixel_in_con==-1 && last_pixel_in_con==-1 && first_pixel_out_con!=-1 && last_pixel_out_con!=-1)
	{	//nucleus is exiting but not entering
	
		entered=2;
		if(first_pixel_out_con>last_pixel_out_con)
		for (i=last_pixel_out_con;i<=first_pixel_out_con; i++)	
		{	
			if(i==(numpoints[j]-1)) k=0;
			else if(i==last_pixel_in_con) k=first_pixel_in_con;
			else k=i+1;
			ai=(position_array[k][j][0])*(position_array[i][j][1]) - (position_array[i][j][0]) * (position_array[k][j][1]);
			*area_out+= ai;
		}	
		else if(first_pixel_out_con<last_pixel_out_con)
		for (i=first_pixel_out_con;i<=last_pixel_out_con; i++)	
		{	
			if(i==(numpoints[j]-1)) k=0;
			else if(i==last_pixel_in_con) k=first_pixel_in_con;
			else k=i+1;

			ai=(position_array[i][j][0]-position_array[last_pixel_out_con][j][0]) *position_array[k][j][1] - (position_array[k][j][0]-position_array[last_pixel_out_con][j][0]) * position_array[i][j][1];
			*area_out+= ai;
		}
		else{printf("warning first_pixel_out_con=last_pixel_out_con\n");}
		*area_in=fabs(*area)-fabs(*area_out);
		//	printf("area_in=%f area_out=%f total_area=%f\n",fabs(area_in),fabs(area_out),fabs(area));
	}
	else if(first_pixel_in_con!=-1 && last_pixel_in_con!=-1 && first_pixel_out_con!=-1 && last_pixel_out_con!=-1)
	{ //nucleus is entering at one end and exiting at the other end of the constriction in same frame
		entered=1;
		double area_1_4=0,area_2_3=0;
		if(first_pixel_in_con>last_pixel_in_con)
		{	for (i=last_pixel_in_con;i<=first_pixel_in_con; i++)	
			{	
				if(i==(numpoints[j]-1)) k=0;
				else if(i==last_pixel_in_con) k=first_pixel_in_con;
				else k=i+1;

			  	ai=(position_array[i][j][0]-position_array[last_pixel_in_con][j][0]) *position_array[k][j][1] - (position_array[k][j][0]-position_array[last_pixel_in_con][j][0]) * position_array[i][j][1];
				area_1_4+= ai;
			}	
		}
		else if(first_pixel_in_con<last_pixel_in_con)
		
		for (i=first_pixel_in_con;i<=last_pixel_in_con; i++)	
		{	
				if(i==(numpoints[j]-1)) k=0;
				else if(i==last_pixel_in_con) k=first_pixel_in_con;
				else k=i+1;
			ai=(position_array[i][j][0]-position_array[last_pixel_in_con][j][0]) *position_array[k][j][1] - (position_array[k][j][0]-position_array[last_pixel_in_con][j][0]) * position_array[i][j][1];
    			area_1_4+= ai;
		}
		else{printf("warning first_pixel_in_con=last_pixel_in_con\n");}

		if(first_pixel_out_con>last_pixel_out_con)
		{
			for (i=last_pixel_out_con;i<=first_pixel_out_con;i++)	
			{	
				if(i==(numpoints[j]-1)) k=0;
				else if(i==last_pixel_in_con) k=first_pixel_in_con;
				else k=i+1;
  				ai=(position_array[i][j][0]-position_array[last_pixel_out_con][j][0]) *position_array[k][j][1] - (position_array[k][j][0]-position_array[last_pixel_out_con][j][0]) * position_array[i][j][1];
	    			area_2_3+= ai;
			}	
		}
		else if(first_pixel_out_con<last_pixel_out_con)
		{
			for (i=first_pixel_out_con;i<=last_pixel_out_con;i++)	
			{	
				if(i==(numpoints[j]-1)) k=0;
				else if(i==last_pixel_in_con) k=first_pixel_in_con;
				else k=i+1;
  				ai=(position_array[i][j][0]-position_array[last_pixel_out_con][j][0]) *position_array[k][j][1] - (position_array[k][j][0]-position_array[last_pixel_out_con][j][0]) * position_array[i][j][1];
	    			area_2_3+= ai;
			}
		}
		else{printf("warning first_pixel_out_con=last_pixel_out_con\n");}

		*area_in=fabs(area_1_4-area_2_3);
		*area_out=fabs(*area)-fabs(*area_in);
	}
	
		*area_in=*area_in/2.0;
		*area_out=*area_out/2.0;
		
	
	return;
}

int check_entry(double*** shape,int i,int j,int* numpoints,int entry_x,int exit_x,int first_pixel_in_con,int last_pixel_in_con)
{
	//return 1 if first point on entry, return 2 if 2nd point, return 0 if point isn't entry or exit
		int k;
		if(i==(numpoints[j]-1)) k=0;
		else k=i+1;
		
		if(shape[i][j][0] < entry_x && shape[k][j][0]>entry_x && first_pixel_in_con==-1  )
		{	//the point is the first point on the channel edge, the cell is entering and the curve is oriented clockwise.
			return 1;
		}
		else if(shape[i][j][0]>entry_x && shape[k][j][0]<entry_x && first_pixel_in_con==-1)
		{
			//Then the point is the first point on the channel edge, cell is entering and curve is oriented anticlockwise.
			return 1;
		}
		else if( shape[i][j][0] < entry_x && shape[k][j][0]>entry_x && last_pixel_in_con==-1  )
		{
			//the point is the last point on the channel edge, the cell is entering and the curve is oriented clockwise.
			return 2;
		}
		else if(shape[i][j][0]>entry_x && shape[k][j][0]<entry_x && last_pixel_in_con==-1)
		{
			//Then the point is the last point on the channel edge, cell is entering and curve is oriented anticlockwise.
			return 2;
		}
	return 0;
}

int check_exit(double*** shape,int i,int j,int* numpoints,int entry_x,int exit_x,int first_pixel_out_con,int last_pixel_out_con)
{
	//return 1 if first point on entry, return 2 if 2nd point, return 0 if point isn't entry or exit
		int k;
		if(i==(numpoints[j]-1)) k=0;
		else k=i+1;
		
		if( shape[i][j][0] > exit_x && shape[k][j][0]<exit_x && first_pixel_out_con==-1)
		{
			//the point is the first point on the channel edge, the cell is exiting and the curve is oriented clockwise.
			return 1;
		}
		else if( shape[i][j][0] < exit_x && shape[k][j][0]>exit_x && first_pixel_out_con==-1)
		{
			//the point is the first point on the channel edge, the cell is exiting and the curve is oriented anticlockwise.
			return 1;
		}
		else if( shape[i][j][0] > exit_x && shape[k][j][0]<exit_x && last_pixel_out_con==-1)
		{
			//the point is the last point on the channel edge, the cell is exiting and the curve is oriented clockwise.
			return 2;
		}
		else if( shape[i][j][0] < exit_x && shape[k][j][0]>exit_x && last_pixel_out_con==-1)
		{
			//the point is the last point on the channel edge, the cell is exiting and the curve is oriented anticlockwise.
			return 2;
		}
	return 0;
}

int in_channel(double*** shape, int i,int j,int entry_x, int exit_x)
{
	if(shape[i][j][0]>entry_x && shape[i][j][0]<exit_x) return 1;
	else return 0;
}

void out_of_plane_strain_solid(double*** strain_array,int i,int j)
{
	if(volume_mode=='s') 
	{
		//symmetric deformation, defined by having the same strain in the along channel (x) and out of plane direction (z)
		strain_array[i][j][5]=strain_array[i][j][0];
		if(incompressible=='y' || fabs(sigma-0.5)<0.01) 
		{	printf("warning, incompressible when supposed to be biaxial strain\n");
			strain_array[i][j][5]=-strain_array[i][j][0]-strain_array[i][j][2];
		}
	}
	if(volume_mode=='f') 
	{
		//filled channel deformation, defined by having the same strain in the along channel (x) and out of plane direction (z)
		//if in the channel, strain is non-zero, if out of the constriction, strain is zero.
		strain_array[i][j][5]=(Lz-Lzcon);
		if(incompressible=='y' || fabs(sigma-0.5)<0.01) {strain_array[i][j][5]=-strain_array[i][j][0]-strain_array[i][j][2];}
	}

	return;
}

void out_of_plane_strain_shell(double*** shape,int i, int j,double entry_x,double exit_x,double* dus1ds2,double* dus2ds1,double* dunds2,double* dus2ds2, double* us2_i)
{
	if(volume_mode=='s')
	{	//in this volume mode, we assume that the shell is symetric in the out of plane direction
		dus1ds2=0; //since around the central line, the changes in deformation will be in the z direction purely, which is perpendicular to us1	
		//un and us2 change by a small amount due to the surface approximation not being entirely perpendicular to the y direction, of compression outside of the very central line
		/*dus2ds1=0;// because of the presumed symmetry of the shape, s2 is assumed 0 constant along the central line  and so the central difference approx to dus2ds1 is zero here			
		ds2=sqrt(pos_array[i][j][1]*pos_array[i][j][1]*pow(pow(cos(phip1),d)-pow(cos(phim1),d),2)+Lz*Lz*(pow(sin(phip1)-sin(phim1),2)));
		dun=0;
		dus2=0;
		dunds2=dun/ds2;
		dus2ds2=dus2/ds2;
		us2_i=0;//required because of symmetry around central line
		//printf("ds2=%f dun=%f dus2=%f dunds2=%f dusds2=%f\n",ds2,dun,dus2,dunds2,dus2ds2);
	*/
	}
	if(volume_mode=='f')
	{
		//in this volume mode, we assume that the shell is a flat surface which fills the channel in the out of plane direction. The channelchanges size to the constriction by going from height Lz to Lzcon.

		*dus1ds2=0; //since around the central line, the changes in deformation will be in the z direction purely, which is perpendicular to us1	
		//un and us2 change by a small amount due to the surface approximation not being entirely perpendicular to the y direction, of compression outside of the very central line

		//because of the symmetry of the shape, s2 is assumed 0 constant along the central line,except at the entrance  and so the central difference approx to dus2ds1 is zero
		*dus2ds1=0; 

		*dunds2=0;
		*dus2ds2=0;

		//and the deformation in the s2 direction is 0 outside of the constriction, Lz-Lcon inside of it.
		*us2_i=(Lz-Lzcon)*in_channel(shape,i,j,entry_x,exit_x);//required because of symmetry around central line
		//printf("ds2=%f dun=%f dus2=%f dunds2=%f dusds2=%f\n",ds2,dun,dus2,dunds2,dus2ds2);
	

	}
	return;
}


void traction_force(double*** shape_array,int* num_points,int num_stacks,double*** stress_array,double*** traction_array)
{
	int i,j;

	double e1x=0,e1y=0,e1z=0;
	double e2x=0,e2y=0,e2z=0;
	double nx=0 ,ny=0 ,nz=0;

	for(j=0;j<num_stacks;j++)
	{
		for(i=0;i<num_points[j];i++)
		{
			direction_vectors_i(shape_array,&e1x,&e1y,&e1z,i,j,num_points[j]);
			direction_vectors_j(shape_array,&e2x,&e2y,&e2z,i,j,num_stacks);
			normal_vector(e1x,e1y,e1z,e2x,e2y,e2z,&nx,&ny,&nz,dimensions,num_stacks);

			//printf("n=(%f,%f,%f), stress=(%f, %f ,%f, %f , %f , %f)\n",nx,ny,nz,stress_array[i][j][0],stress_array[i][j][1],stress_array[i][j][2],stress_array[i][j][3],stress_array[i][j][4],stress_array[i][j][5]);
			traction_array[i][j][0]= nx*stress_array[i][j][0]+ny*stress_array[i][j][1]+nz*stress_array[i][j][2];
			traction_array[i][j][1]= nx*stress_array[i][j][1]+ny*stress_array[i][j][2]+nz*stress_array[i][j][4];
			traction_array[i][j][2]= nx*stress_array[i][j][3]+ny*stress_array[i][j][4]+nz*stress_array[i][j][5];
		}
	}
	return;
}
